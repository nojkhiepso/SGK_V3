﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.Core.Contracts.Cover;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface ICoverService
    {
        SearchCoverResponse SearchUser(SearchCoverRequest request);

        bool CreateOrUpdate(CoverRequest request);

        bool DeleteBy(Guid id, Guid uId);

        bool DeleteBy(Guid[] id, Guid uId);

        Cover GetById(Guid id);

        CoverRecord GetRecordById(Guid id);

        int GetCoverMaxVersion(Guid id);

        bool OnAndOff(Guid id, Guid uId);

        bool TurnOff(Guid id, string status, Guid userId);
    }

    public class CoverService : BaseService, ICoverService
    {
        private readonly ICoverRepository _coverRepository;
        private readonly IAccessLogService _accessLogService;
        private readonly ICoverGroupService _coverGroupService;
        private readonly ICoverVersionGroupService _coverVersionGroupService;

        public CoverService(ICoverRepository coverRepository, IAccessLogService accessLogService, ICoverGroupService coverGroupService, ICoverVersionGroupService coverVersionGroupService)
        {
            _coverRepository = coverRepository;
            _accessLogService = accessLogService;
            _coverGroupService = coverGroupService;
            _coverVersionGroupService = coverVersionGroupService;
        }

        public SearchCoverResponse SearchUser(SearchCoverRequest request)
        {
            try
            {
                var response = new SearchCoverResponse()
                {
                    Paging = new PagingResponse(),
                    Status = new ResponseStatus()
                };

                var searchCache = new SearchCachePagging<CoverRecord>();
                var key = "Cover_" + request.Name + "_" + request.Code + "_" + request.Version + "_" + request.Paging.PageIndex;
                var cacheItem = CacheManager.Instance.GetCache<SearchCachePagging<CoverRecord>>(key);
                if (cacheItem != null)
                {
                    searchCache = cacheItem;
                }
                else
                {
                    int totalRecords;
                    var result =
                        _coverRepository.SearchPaging(request.Name, request.Code, request.Version, request.Paging.PageIndex,
                            request.Paging.PageSize, out totalRecords);

                    searchCache.Items = result.ToList();
                    searchCache.TotalRecords = totalRecords;

                    CacheManager.Instance.SetCache(key, searchCache);
                }

                response.Records = searchCache.Items;
                response.Paging.TotalRecords = searchCache.TotalRecords;
                response.Status.IsSuccessFull = true;

                return response;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool CreateOrUpdate(CoverRequest request)
        {
            CacheManager.Instance.RemoveCacheIfKeyContains("Cover_");
            try
            {
                if (request.Record == null)
                {
                    return false;
                }

                if (request.Header.Action == 1)//add
                {
                    var maxsortOrder = _coverRepository.GetOrderNo();
                    var entity = ToEntity(request, null);
                    entity.SortOrder = maxsortOrder + 1;

                    if (_coverRepository.Add(entity))
                    {
                        _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.Cover, entity, request.Header.UserId);
                        AddCoverGroupAndCoverVersion(request, entity);
                    }
                    return true;
                }
                else if (request.Header.Action == 3)// add new version
                {
                    var model = _coverRepository.GetById(request.Record.CoverId);
                    var entity = ToEntity(request, model);
                    entity.CoverId = Guid.NewGuid();

                    if (_coverRepository.Add(entity))
                    {
                        _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Cover, entity, request.Header.UserId);
                        AddCoverGroupAndCoverVersion(request, entity);
                    }

                    return true;
                }
                else if (request.Header.Action == 2)// update
                {
                    var model = _coverRepository.GetById(request.Record.CoverId);
                    var entity = ToEntity(request, model);

                    // delete

                    _coverVersionGroupService.DeleteBy(new Guid[] { request.Record.CoverId });
                    _coverGroupService.DeleteBy(request.Record.CoverId);

                    if (_coverRepository.Update(entity))
                    {
                        _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Cover, entity, request.Header.UserId);
                        AddCoverGroupAndCoverVersion(request, entity);
                    }

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        private void AddCoverGroupAndCoverVersion(CoverRequest request, Cover entity)
        {
            if (request.Record.Groups != null && request.Record.Groups.Length > 0)
            {
                // add to cover group
                var cg = request.Record.Groups.Select(x => new CoverGroup()
                {
                    GroupId = x,
                    CoverGroupId = Guid.NewGuid(),
                    CoverId = entity.CoverId
                }).ToList();

                _coverGroupService.AddRange(cg);
            }

            if (request.Record.CoverVersion != null && request.Record.CoverVersion.Any())
            {
                // add to cover version
                var cv = request.Record.CoverVersion.Select(x => new CoverVersionGroup()
                {
                    CoverVersionGroupId = Guid.NewGuid(),
                    CategoryId = x.CategoryId,
                    VersionId = x.VersionId,
                    CoverId = entity.CoverId,
                    CategoryName = x.CategoryName,
                    VersionNo = x.VersionNo
                }).ToList();

                _coverVersionGroupService.AddRange(cv);
            }
        }

        public bool DeleteBy(Guid id, Guid uId)
        {
            return DeleteItem(new[] { id }, uId);
        }

        public bool DeleteBy(Guid[] id, Guid uId)
        {
            return DeleteItem(id, uId);
        }

        private bool DeleteItem(Guid[] id, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Cover_");

                _coverVersionGroupService.DeleteBy(id);
                _coverGroupService.DeleteBy(id);

                if (_coverRepository.DeleteBy(id))
                {
                    _accessLogService.InsertAccessLog(Constant.Delete, Constant.ModuleName.Cover, new { id }, uId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public Cover GetById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<CoreData.Cover>("Cover_" + id + "_GetById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _coverRepository.GetById(id);
                    CacheManager.Instance.SetCache("Cover_" + id + "_GetById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public CoverRecord GetRecordById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<CoverRecord>("Cover_" + id + "_GetRecordById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _coverRepository.GetRecordById(id);
                    CacheManager.Instance.SetCache("Cover_" + id + "_GetRecordById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public int GetCoverMaxVersion(Guid id)
        {
            try
            {
                return (int)_coverRepository.GetVersionNo(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return 0;
            }
        }

        public bool OnAndOff(Guid id, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Cover_");

                if (_coverRepository.UpdateStatus(id))
                {
                    _accessLogService.InsertAccessLog(Constant.OnOff, Constant.ModuleName.Cover, new { id }, uId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool TurnOff(Guid id, string status, Guid userId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Cover_");


                if (_coverRepository.TurnOff(id, status))
                {
                    _accessLogService.InsertAccessLog(Constant.OnOff, Constant.ModuleName.Cover, new { id }, userId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public CoreData.Cover ToEntity(CoverRequest model, Cover entity)
        {
            if (entity == null)
            {
                var id = Guid.NewGuid();
                entity = new CoreData.Cover()
                {
                    CoverId = id,
                    IsActive = model.Record.IsActive,
                    Name = model.Record.Name,
                    Code = model.Record.Code,
                    Version = model.Record.Version,
                    CreateDate = model.Record.CreateDate,
                    CreateBy = model.Record.CreateBy,
                    ParentId = id
                };

            }
            else
            {
                entity.CoverId = model.Record.CoverId;
                entity.IsActive = model.Record.IsActive;
                entity.Name = model.Record.Name;
                entity.Code = model.Record.Code;
                entity.Version = model.Record.Version;
                entity.ParentId = model.Record.ParentId;

            }
            return entity;
        }
    }
}
