﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Module;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface IModuleService
    {
        IEnumerable<ModuleRecord> GetAllModule();

        bool CreateOrUpdate(ModuleRequest req);
        IEnumerable<ModuleRecord> GetModuleBy(Guid key);
    }

    public class ModuleService : BaseService, IModuleService
    {
        private readonly IModuleRepository _moduleRepository;
        private readonly IAccessLogService _accessLogService;

        public ModuleService(IModuleRepository moduleRepository, IAccessLogService accessLogService)
        {
            _moduleRepository = moduleRepository;
            _accessLogService = accessLogService;
        }

        public IEnumerable<ModuleRecord> GetAllModule()
        {
            try
            {
                return _moduleRepository.GetAllModule();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool CreateOrUpdate(ModuleRequest request)
        {
            try
            {
                if (request.Record == null)
                {
                    return false;
                }

                if (request.Header.Action == 1)//add
                {
                    var entity = ToEntity(request, null);
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.Module, entity, request.Header.UserId);

                    return _moduleRepository.Add(entity);
                }
                else
                {
                    var group = _moduleRepository.GetById(request.Record.ModuleId);
                    var entity = ToEntity(request, group);
                    _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Module, entity, request.Header.UserId);

                    return _moduleRepository.Update(entity);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public IEnumerable<ModuleRecord> GetModuleBy(Guid key)
        {
            try
            {
                return _moduleRepository.GetModuleBy(key);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public CoreData.Module ToEntity(ModuleRequest model, CoreData.Module entity)
        {
            if (entity == null)
            {
                entity = new CoreData.Module()
                {
                    ModuleId = Guid.NewGuid(),
                    IsActive = model.Record.IsActive,
                    Name = model.Record.Name,
                    Code = model.Record.Code
                };
            }
            else
            {
                entity.ModuleId = model.Record.ModuleId;
                entity.IsActive = model.Record.IsActive;
                entity.Name = model.Record.Name;
                entity.Code = model.Record.Code;
            }
            return entity;
        }
    }
}
