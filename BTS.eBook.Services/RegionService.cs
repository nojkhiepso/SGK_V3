﻿using System;
using System.Linq;
using BTS.eBook.Core;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.Core.Contracts.Region;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Data.Repositories;
using System.Collections.Generic;
using BTS.eBook.CoreData;

namespace BTS.eBook.Services
{
    public interface IRegionService
    {
        SearchRegionResponse SearchPaging(SearchRegionRequest request);

        bool CreateOrUpdate(RegionRequest request);

        bool Delete(Guid id, Guid uId);

        Region GetById(Guid id);

        RegionRecord GetRecordById(Guid id);

        IEnumerable<RegionRecord> GetAll();

    }

    public class RegionService : BaseService, IRegionService
    {
        private readonly IRegionRepository _regionRepository;
        private readonly IAccessLogService _accessLogService;

        public RegionService(IRegionRepository regionRepository, IAccessLogService accessLogService)
        {
            _regionRepository = regionRepository;
            _accessLogService = accessLogService;
        }

        public SearchRegionResponse SearchPaging(SearchRegionRequest request)
        {
            try
            {
                var response = new SearchRegionResponse()
                {
                    Paging = new PagingResponse(),
                    Status = new ResponseStatus()
                };

                var searchCache = new SearchCachePagging<RegionRecord>();
                var key = "Region_" + request.Name + "_" + request.Code + "_" + request.Paging.PageIndex + "_" + request.Paging.PageSize;
                var cacheItem = CacheManager.Instance.GetCache<SearchCachePagging<RegionRecord>>(key);
                if (cacheItem != null)
                {
                    searchCache = cacheItem;
                }
                else
                {
                    int totalRecords;
                    var result =
                        _regionRepository.SearchPaging(request.Name, request.Code, request.Paging.PageIndex,
                            request.Paging.PageSize, out totalRecords);

                    searchCache.Items = result.ToList();
                    searchCache.TotalRecords = totalRecords;

                    CacheManager.Instance.SetCache(key, searchCache);
                }

                response.Records = searchCache.Items;
                response.Paging.TotalRecords = searchCache.TotalRecords;
                response.Status.IsSuccessFull = true;

                return response;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }

        }

        public bool CreateOrUpdate(RegionRequest request)
        {
            CacheManager.Instance.RemoveCacheIfKeyContains("Region_");
            try
            {
                if (request.Record == null)
                {
                    return false;
                }
                if (request.Header.Action == 1)//add
                {
                    var entity = ToEntity(request, null);
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.Region, entity, request.Header.UserId);

                    return _regionRepository.Add(entity);
                }
                else
                {
                    var region = _regionRepository.GetById(request.Record.RegionId);
                    var entity = ToEntity(request, region);
                    _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Region, entity, request.Header.UserId);

                    return _regionRepository.Update(entity);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool Delete(Guid id, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Region_");
                if (_regionRepository.DeleteById(id))
                {
                    _accessLogService.InsertAccessLog(Constant.Delete, Constant.ModuleName.Region, new { id }, uId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public Region GetById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<Region>("Region_" + id + "_GetById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _regionRepository.GetById(id);
                    CacheManager.Instance.SetCache("Region_" + id + "_GetById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public RegionRecord GetRecordById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<RegionRecord>("Region_" + id + "_GetRecordById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _regionRepository.GetRecordById(id);
                    CacheManager.Instance.SetCache("Region_" + id + "_GetRecordById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public Region ToEntity(RegionRequest model, Region entity)
        {
            if (entity == null)
            {
                entity = new Region()
                {
                    RegionId = Guid.NewGuid(),
                    Name = model.Record.Name,
                    Code = model.Record.Code,
                    Body = model.Record.Body,
                    CreatedDate = model.Record.CreatedDate,
                    CreatedBy = model.Record.CreatedBy,
                    ModifiedDate = model.Record.ModifiedDate,
                    ModifiedBy = model.Record.ModifiedBy
                };

            }
            else
            {
                entity.RegionId = model.Record.RegionId;
                entity.Name = model.Record.Name;
                entity.Code = model.Record.Code;
                entity.ModifiedDate = model.Record.ModifiedDate;
                entity.ModifiedBy = model.Record.ModifiedBy;
            }
            return entity;
        }


        public IEnumerable<RegionRecord> GetAll()
        {
            try
            {
                return _regionRepository.GetAllRegion();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
    }
}
