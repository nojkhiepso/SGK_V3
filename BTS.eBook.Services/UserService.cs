﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.Core.Contracts.Client;
using BTS.eBook.Core.Contracts.Cover;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;
using log4net;
using log4net.Core;
using log4net.Repository.Hierarchy;
using Newtonsoft.Json;

namespace BTS.eBook.Services
{
    public interface IUserService
    {
        SearchUserResponse SearchUser(SearchUserRequest request);

        bool CreateOrUpdate(UserRequest request);

        bool ChangePassword(UserRequest request);

        bool DeleteBy(Guid id, Guid uId);

        bool DeleteBy(Guid[] id, Guid uId);

        User GetById(Guid id);

        UserRecord GetRecordById(Guid id);

        IEnumerable<UserRecord> GetAllUser();

        UserRecord GetUserByName(string request);

        User GetUser(string userName);

        bool UpdateUserById(Guid id);

        string[] GetAllUserFunction(Guid uId);

        IEnumerable<UserRecord> GetUserExcludeByGroup();

        bool ExitsEmail(string email);

        // client

        IEnumerable<UserVersionRecord> GetVersionByUser(Guid uId, List<OrderRecord> records, int versionNo);

        bool UpdateUniqueNo(string unique, Guid uId);

        bool TurnOff(Guid id, string status, Guid uId);

        bool AddMany(List<User> users);

        int GetCountUser();

        int GetCountUser(string key);

        bool UpdateUser(User user);

        IEnumerable<User> GetAllUserBy(string userName);
    }

    public class UserService : BaseService, IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAccessLogService _accessLogService;
        private readonly IRoleService _roleService;
        private readonly IGroupUserService _groupUserService;

        public UserService(IUserRepository userRepository, IAccessLogService accessLogService, IRoleService roleService, IGroupUserService groupUserService)
        {
            _userRepository = userRepository;
            _accessLogService = accessLogService;
            _roleService = roleService;
            _groupUserService = groupUserService;
        }

        public SearchUserResponse SearchUser(SearchUserRequest request)
        {
            try
            {
                var response = new SearchUserResponse()
                {
                    Paging = new PagingResponse(),
                    Status = new ResponseStatus()
                };

                var searchCache = new SearchCachePagging<UserRecord>();
                var key = "User_" + request.UserName + "_" + request.Email + "_" + request.Paging.PageIndex + "_" + request.Paging.PageSize;
                var cacheItem = CacheManager.Instance.GetCache<SearchCachePagging<UserRecord>>(key);
                if (cacheItem != null)
                {
                    searchCache = cacheItem;
                }
                else
                {
                    int totalRecords;
                    var result =
                        _userRepository.SearchUser(request.UserName, request.Email, request.Paging.PageIndex,
                            request.Paging.PageSize, out totalRecords);

                    searchCache.Items = result.ToList();
                    searchCache.TotalRecords = totalRecords;

                    CacheManager.Instance.SetCache(key, searchCache);
                }

                response.Records = searchCache.Items;
                response.Paging.TotalRecords = searchCache.TotalRecords;
                response.Status.IsSuccessFull = true;

                return response;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }

        }

        public bool CreateOrUpdate(UserRequest request)
        {
            CacheManager.Instance.RemoveCacheIfKeyContains("User_");
            try
            {
                if (request.Record == null)
                {
                    return false;
                }

                if (request.Header.Action == 1)//add
                {
                    var user = GetCacheUserByName(request.Record.UserName);
                    if (user != null) return false;

                    var entity = ToEntity(request, null);
                    if (_userRepository.Add(entity))
                    {
                        _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.User, entity, request.Header.UserId);
                        var userRoles = new List<UserRole>();
                        if (request.Record.Roles != null)
                        {
                            for (int i = 0; i < request.Record.Roles.Length; i++)
                            {
                                var roleId = request.Record.Roles[i];
                                userRoles.Add(new UserRole()
                                {
                                    RoleId = roleId,
                                    UserId = entity.UserId,
                                    UserRoleId = Guid.NewGuid()
                                });
                            }
                            _roleService.AddRange(userRoles, request.Header.UserId);
                        }
                    }
                    return true;
                }
                else
                {
                    // 
                    var result = _userRepository.GetById(request.Record.UserId);
                    var entity = ToEntity(request, result);

                    _roleService.DeleteRangeById(request.Record.UserId);

                    if (_userRepository.Update(entity))
                    {
                        _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.User, entity, request.Header.UserId);
                        var userRoles = new List<UserRole>();
                        if (request.Record.Roles != null)
                        {
                            for (int i = 0; i < request.Record.Roles.Length; i++)
                            {
                                var roleId = request.Record.Roles[i];
                                userRoles.Add(new UserRole()
                                {
                                    RoleId = roleId,
                                    UserId = request.Record.UserId,
                                    UserRoleId = Guid.NewGuid()
                                });
                            }
                            _roleService.AddRange(userRoles, request.Header.UserId);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool ChangePassword(UserRequest request)
        {
            try
            {
                var user = GetById(request.Record.UserId);
                if (user == null) return false;

                user.PasswordHash = request.Record.PasswordHash;
                if (_userRepository.Update(user))
                {
                    _accessLogService.InsertAccessLog(Constant.ChangePass, Constant.ModuleName.User, user, request.Header.UserId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool DeleteBy(Guid id, Guid uId)
        {
            return DeleteItem(new[] { id }, uId);
        }

        public bool DeleteBy(Guid[] id, Guid uId)
        {
            return DeleteItem(id, uId);
        }

        private bool DeleteItem(Guid[] id, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("User_");
                // delete userRole
                _roleService.DeleteRoleByUser(id);
                // delete groupuser
                _groupUserService.DeleteManyUserId(id);

                if (_userRepository.DeleteById(id))
                {
                    _accessLogService.InsertAccessLog(Constant.Delete, Constant.ModuleName.User, new { id }, uId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public User GetById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<User>("User_" + id + "_GetById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _userRepository.GetById(id);
                    CacheManager.Instance.SetCache("User_" + id + "_GetById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public UserRecord GetRecordById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<UserRecord>("User_" + id + "_GetRecordById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _userRepository.GetRecordById(id);
                    CacheManager.Instance.SetCache("User_" + id + "_GetRecordById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public string[] GetAllUserFunction(Guid uId)
        {
            try
            {
                string[] uf = { };
                var userFunction = _userRepository.GetAllUserFunction(uId);
                if (userFunction != null && userFunction.Any())
                {
                    uf = new string[userFunction.Count()];
                    var i = 0;
                    foreach (var u in userFunction)
                    {
                        uf[i] = u.FunctionName;
                        i++;
                    }
                }
                return uf;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<UserRecord> GetUserExcludeByGroup()
        {
            try
            {
                var userRoles = new List<UserRecord>();

                var users = _userRepository.GetAllUser();
                if (users != null && users.Any())
                {
                    userRoles.AddRange(from u in users
                                           //let exits = _roleService.CheckUserInRoleExist(u.UserId)
                                           //where !exits
                                       select new UserRecord()
                                       {
                                           UserId = u.UserId,
                                           FullName = u.FullName,
                                           UserName = u.UserName
                                       });
                }
                return userRoles;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;

            }
        }

        public bool ExitsEmail(string email)
        {
            try
            {
                return _userRepository.ExitsEmail(email);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public IEnumerable<UserRecord> GetAllUser()
        {
            try
            {
                return _userRepository.GetAllUser();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public UserRecord GetUserByName(string request)
        {
            try
            {
                return _userRepository.GetUserByUserName(request);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public User GetUser(string userName)
        {
            try
            {
                return _userRepository.GetUser(userName);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool UpdateUserById(Guid id)
        {
            try
            {
                var user = _userRepository.GetById(id);
                user.LastModifyTime = DateTime.Now;

                return _userRepository.Update(user);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        private UserRecord GetCacheUserByName(string userName)
        {
            try
            {
                UserRecord user;
                var cacheItem = CacheManager.Instance.GetCache<UserRecord>("User_" + userName);
                if (cacheItem != null)
                {
                    user = cacheItem;
                }
                else
                {
                    user = _userRepository.GetUserByUserName(userName);
                    CacheManager.Instance.SetCache("User_" + userName, user);
                }
                return user;
            }
            catch (Exception ex)
            {

                Logger.Error(ex);
                return null;
            }
        }

        public User ToEntity(UserRequest model, User entity)
        {
            if (entity == null)
            {
                entity = new User
                {
                    UserId = Guid.NewGuid(),
                    CreateTime = model.Record.CreateTime,
                    LastModifyTime = null,
                    PasswordHash = model.Record.PasswordHash,
                    UserName = model.Record.UserName,
                    FullName = model.Record.FullName,
                    Phone = model.Record.Phone,
                    Email = model.Record.Email,
                    IsLocked = false,
                    IsActive = model.Record.IsActive,
                    SortOrder = model.Record.SortOrder
                };

            }
            else
            {
                entity.UserId = model.Record.UserId;
                entity.LastModifyTime = null;
                entity.UserName = model.Record.UserName;
                entity.FullName = model.Record.FullName;
                entity.Phone = model.Record.Phone;
                entity.Email = model.Record.Email;
                entity.IsLocked = model.Record.IsLocked;
                entity.SortOrder = model.Record.SortOrder;
                entity.IsActive = model.Record.IsActive;
            }
            return entity;
        }

        // client
        public IEnumerable<UserVersionRecord> GetVersionByUser(Guid uId, List<OrderRecord> records, int versionNo)
        {
            try
            {
                return _userRepository.GetVersionByUser(uId, records, versionNo);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool UpdateUniqueNo(string unique, Guid uId)
        {
            try
            {
                return _userRepository.UpdateUniqueNo(unique, uId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool TurnOff(Guid id, string status, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("User_");


                if (_userRepository.TurnOff(id, status))
                {
                    _accessLogService.InsertAccessLog(Constant.OnOff, Constant.ModuleName.User, new { id }, uId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool AddMany(List<User> users)
        {
            try
            {
                return _userRepository.AddRange(users);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public int GetCountUser()
        {
            try
            {
                return _userRepository.GetCountUser();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return -1;
            }
        }

        public int GetCountUser(string key)
        {
            try
            {
                return _userRepository.GetCountUser(key);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return -1;
            }
        }

        public bool UpdateUser(User user)
        {
            try
            {
                return _userRepository.Update(user);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public IEnumerable<User> GetAllUserBy(string userName)
        {
            try
            {
                return _userRepository.GetAllUserBy(userName);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
    }
}
