﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories; 
namespace BTS.eBook.Services
{
    public interface IRoleService
    {

        Role GetRoleById(Guid id);

        RoleRecord GetRoleRecordById(Guid id);

        IEnumerable<RoleRecord> GetAllRoles();

        Guid[] GetUserRole();

        Guid[] GetUserRole(Guid uid);

        bool AddRange(List<UserRole> request, Guid uId);

        bool DeleteRange(List<UserRole> request, Guid uId);

        IEnumerable<UserRole> GetAllByUserId(Guid id);

        bool CheckRoleExist(string name);

        bool CheckUserInRoleExist(Guid uId);

        bool UpdateRole(GroupViewModelRequest model, List<RoleModuleRecord> list);

        IEnumerable<RoleModuleRecord> GetRoleModules(Guid roleId);

        bool DeleteRangeById(Guid userId);

        bool DeleteRoleById(Guid id, Guid uId);

        bool TurnOff(Guid id, string status, Guid userId);

        bool DeleteRoleByUser(Guid[] id);
    }

    public class RoleService : BaseService, IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IUserRoleRepository _userRoleRepository;
        private readonly IAccessLogService _accessLogService;
        private readonly IRoleModuleRepository _roleModuleRepository;

        public RoleService(IRoleRepository roleRepository, IUserRoleRepository userRoleRepository, IAccessLogService accessLogService, IRoleModuleRepository roleModuleRepository)
        {
            _roleRepository = roleRepository;
            _userRoleRepository = userRoleRepository;
            _accessLogService = accessLogService;
            _roleModuleRepository = roleModuleRepository;
        }

        public Role GetRoleById(Guid id)
        {
            try
            {
                return _roleRepository.GetRoleById(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public RoleRecord GetRoleRecordById(Guid id)
        {
            try
            {
                return _roleRepository.GetRoleRecordById(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<RoleRecord> GetAllRoles()
        {
            try
            {
                return _roleRepository.GetAllRole();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public Guid[] GetUserRole()
        {
            try
            {
                var userRoles = new Guid[] { };
                var ur = _userRoleRepository.GetAllUserRole();
                if (ur != null && ur.Any())
                {
                    userRoles = new Guid[ur.Count()];
                    var i = 0;
                    foreach (var r in ur)
                    {
                        userRoles[i] = r.RoleId;
                        i++;
                    }
                }
                return userRoles;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public Guid[] GetUserRole(Guid uid)
        {
            try
            {
                var userRoles = new Guid[] { };
                var ur = _userRoleRepository.GetAllUserRole(uid);
                if (ur != null && ur.Any())
                {
                    userRoles = new Guid[ur.Count()];
                    var i = 0;
                    foreach (var r in ur)
                    {
                        userRoles[i] = (Guid)r.RoleId;
                        i++;
                    }
                }
                return userRoles;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool AddRange(List<UserRole> request, Guid uId)
        {
            try
            {
                if (_userRoleRepository.AddRange(request))
                {
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.UserRole, request, uId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool DeleteRange(List<UserRole> request, Guid uId)
        {
            try
            {
                if (_userRoleRepository.DeleteRange(request))
                {
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.UserRole, request, uId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public IEnumerable<UserRole> GetAllByUserId(Guid id)
        {
            try
            {
                return _userRoleRepository.GetAllUserRole(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool CheckRoleExist(string name)
        {
            try
            {
                return _userRoleRepository.CheckRoleExist(name.Trim());
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool CheckUserInRoleExist(Guid uId)
        {
            try
            {
                return _userRoleRepository.CheckUserInRoleExist(uId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool UpdateRole(GroupViewModelRequest model, List<RoleModuleRecord> list)
        {
            var role = _roleRepository.GetById(model.Record.RoleRecord.RoleId);
            var entity = ToEntityRole(model.Record.RoleRecord, role);

            var result = model.Header.Action == 1
                ? _roleRepository.Add(entity)
                : _roleRepository.Update(entity);

            if (result)
            {
                if (model.Header.Action == 1)
                {
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.Role, entity, model.Header.UserId);
                }
                else
                {
                    _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Role, entity, model.Header.UserId);
                }

                if (_roleModuleRepository.DeleteRoleModule(model.Record.RoleRecord.RoleId))
                {
                    _accessLogService.InsertAccessLog(Constant.Delete, Constant.ModuleName.RoleModule, model.Record.RoleRecord.RoleId, model.Header.UserId);
                }

                if (list != null)
                {
                    foreach (var i in list)
                    {
                        if (i.Add || i.Edit || i.Delete || i.View || i.Publish)
                        {
                            RoleModule entityRoleModule = new RoleModule
                            {
                                RoleModuleId = Guid.NewGuid(),
                                RoleId = entity.RoleId,
                                ModuleId = i.ModuleId,
                                Add = i.Add,
                                Edit = i.Edit,
                                Delete = i.Delete,
                                Publish = i.Publish,
                                View = i.View
                            };
                            if (_roleModuleRepository.Add(entityRoleModule))
                            {
                                _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.RoleModule, entityRoleModule, model.Header.UserId);
                            }
                        }
                    }
                }
                return true;
            }
            return false;
        }

        public IEnumerable<RoleModuleRecord> GetRoleModules(Guid roleId)
        {
            try
            {
                return _userRoleRepository.GetRoleModules(roleId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool DeleteRangeById(Guid userId)
        {
            try
            {
                return _userRoleRepository.DeleteRangeById(new Guid[] { userId });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool DeleteRoleById(Guid id, Guid uId)
        {
            try
            {
                // delete userRole
                _userRoleRepository.DeleteRangeByRoleId(new Guid[] { id });
                // delete rolemodule
                _roleModuleRepository.DeleteRoleModule(id);

                if (_roleRepository.DeleteById(id))
                {
                    _accessLogService.InsertAccessLog(Constant.Delete, Constant.ModuleName.Role, new { id }, uId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return false;
        }

        public bool TurnOff(Guid id, string status, Guid userId)
        {
            try
            {

                if (_roleModuleRepository.TurnOff(id, status))
                {
                    _accessLogService.InsertAccessLog(Constant.OnOff, Constant.ModuleName.Role, new { id }, userId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool DeleteRoleByUser(Guid[] id)
        {
            try
            {
                return _userRoleRepository.DeleteRangeById(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public Role ToEntityRole(RoleRecord model, Role entity)
        {
            if (entity == null)
            {
                entity = new Role();
                entity.RoleId = Guid.NewGuid();
                entity.IsActive = model.IsActive;
                entity.Name = model.Name;
            }
            else
            {
                entity.RoleId = model.RoleId;
                entity.Name = model.Name;
                entity.IsActive = model.IsActive;
            }

            return entity;
        }
    }
}
