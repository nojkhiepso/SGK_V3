﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.Core.Contracts.Group;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;
using log4net;
using log4net.Core;
using log4net.Repository.Hierarchy;
using Newtonsoft.Json;
using Group = System.Text.RegularExpressions.Group;

namespace BTS.eBook.Services
{
    public interface IGroupService
    {
        SearchGroupResponse SearchUser(SearchGroupRequest request);

        bool CreateOrUpdate(GroupRequest request);

        bool DeleteBy(Guid id, Guid uId);

        bool DeleteBy(Guid[] id, Guid uId);

        CoreData.Group GetById(Guid id);

        GroupRecord GetRecordById(Guid id);

        IEnumerable<GroupRecord> GetAllGroupBy(int[] type);

        bool TurnOff(Guid id, string status, Guid userId);
    }

    public class GroupService : BaseService, IGroupService
    {
        private readonly IGroupRepository _groupRepository;
        private readonly IGroupUserService _groupUserService;
        private readonly IVersionGroupService _versionGroupService;
        private readonly IAccessLogService _accessLogService;
        private readonly ICoverGroupService _coverGroupService;
        private readonly ICoverService _coverService;
        private readonly ICoverVersionGroupService _coverVersionGroupService;
        public GroupService(IGroupRepository groupRepository, IGroupUserService groupUserService, IVersionGroupService versionGroupService, IAccessLogService accessLogService, ICoverGroupService coverGroupService, ICoverVersionGroupService coverVersionGroupService, ICoverService coverService)
        {
            _groupRepository = groupRepository;
            _groupUserService = groupUserService;
            _versionGroupService = versionGroupService;
            _accessLogService = accessLogService;
            _coverGroupService = coverGroupService;
            _coverVersionGroupService = coverVersionGroupService;
            _coverService = coverService;
        }

        public SearchGroupResponse SearchUser(SearchGroupRequest request)
        {
            try
            {
                var response = new SearchGroupResponse()
                {
                    Paging = new PagingResponse(),
                    Status = new ResponseStatus()
                };

                var searchCache = new SearchCachePagging<GroupRecord>();
                var key = "Group_" + request.Name + "_" + request.Code + "_" + request.Paging.PageIndex;
                var cacheItem = CacheManager.Instance.GetCache<SearchCachePagging<GroupRecord>>(key);
                if (cacheItem != null)
                {
                    searchCache = cacheItem;
                }
                else
                {
                    int totalRecords;
                    var result =
                        _groupRepository.SearchGroup(request.Name, request.Code, request.Paging.PageIndex,
                            request.Paging.PageSize, out totalRecords);

                    searchCache.Items = result.ToList();
                    searchCache.TotalRecords = totalRecords;

                    CacheManager.Instance.SetCache(key, searchCache);
                }

                response.Records = searchCache.Items;
                response.Paging.TotalRecords = searchCache.TotalRecords;
                response.Status.IsSuccessFull = true;

                return response;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }

        }

        public bool CreateOrUpdate(GroupRequest request)
        {
            CacheManager.Instance.RemoveCacheIfKeyContains("Group_");
            try
            {
                if (request.Record == null)
                {
                    return false;
                }

                if (request.Header.Action == 1)//add
                {
                    var entity = ToEntity(request, null);

                    if (_groupRepository.Add(entity))
                    {
                        _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.Group, entity, request.Header.UserId);
                        AddGroupUserAndGroupVersion(request, entity);
                        return true;
                    }

                    return false;
                }
                else
                {
                    var group = _groupRepository.GetById(request.Record.GroupId);
                    var entity = ToEntity(request, group);

                    _groupUserService.DeleteByGroupId(request.Record.GroupId);
                    _versionGroupService.DeleteByGroupId(request.Record.GroupId);

                    if (_groupRepository.Update(entity))
                    {
                        _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Group, entity, request.Header.UserId);
                        AddGroupUserAndGroupVersion(request, entity);
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        private void AddGroupUserAndGroupVersion(GroupRequest request, CoreData.Group entity)
        {
            if (request.Record.Users != null && request.Record.Users.Any())
            {
                var groupUsers = request.Record.Users.Select(t => new GroupUser()
                {
                    GroupId = entity.GroupId,
                    UserId = t,
                    GroupUserId = Guid.NewGuid()
                }).ToList();

                if (_groupUserService.AddMany(groupUsers, request.Header.UserId))
                {
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.GroupUser, groupUsers,
                        request.Header.UserId);
                }
            }

            if (request.Record.Versions != null && request.Record.Versions.Any())
            {
                var versions = request.Record.Versions.Select(x => new VersionGroup()
                {
                    VersionGroupId = Guid.NewGuid(),
                    GroupId = entity.GroupId,
                    VersionId = x,
                    CategoryId = request.Record.CategoryId
                }).ToList();

                if (_versionGroupService.AddMany(versions, request.Header.UserId))
                {
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.VersionGroup, versions,
                        request.Header.UserId);
                }
            }
        }

        public bool DeleteBy(Guid id, Guid uId)
        {
            return DeleteItem(new[] { id }, uId);
        }

        public bool DeleteBy(Guid[] id, Guid uId)
        {
            return DeleteItem(id, uId);
        }

        private bool DeleteItem(Guid[] id, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Group_");

                // delete GroupVersion
                _versionGroupService.DeleteByGroupId(id);
                _groupUserService.DeleteByGroupId(id);
                _coverGroupService.DeleteByGroup(id);

                var cg = _coverGroupService.GetCoverByGroup(id);
                if (cg != null && cg.Length > 0)
                {
                    // delete coverversiong
                    _coverVersionGroupService.DeleteBy(cg);
                    // delete  cover
                    _coverService.DeleteBy(cg, uId);

                    _coverGroupService.DeleteByGroup(id);
                }

                if (_groupRepository.DeleteById(id))
                {
                    _accessLogService.InsertAccessLog(Constant.Delete, Constant.ModuleName.Group, new { id }, uId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public CoreData.Group GetById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<CoreData.Group>("Group_" + id + "_GetById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _groupRepository.GetById(id);
                    CacheManager.Instance.SetCache("Group_" + id + "_GetById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public GroupRecord GetRecordById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<GroupRecord>("Group_" + id + "_GetRecordById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _groupRepository.GetRecordById(id);
                    CacheManager.Instance.SetCache("Group_" + id + "_GetRecordById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<GroupRecord> GetAllGroupBy(int[] type)
        {
            try
            {
                return _groupRepository.GetAllGroupBy(type);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool TurnOff(Guid id, string status, Guid userId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Group_");


                if (_groupRepository.TurnOff(id, status))
                {
                    _accessLogService.InsertAccessLog(Constant.OnOff, Constant.ModuleName.Group, new { id }, userId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public CoreData.Group ToEntity(GroupRequest model, CoreData.Group entity)
        {
            if (entity == null)
            {
                entity = new CoreData.Group()
                {
                    GroupId = Guid.NewGuid(),
                    IsActive = model.Record.IsActive,
                    Name = model.Record.Name,
                    Code = model.Record.Code,
                    CreatedBy = model.Record.CreatedBy,
                    CreatedDate = model.Record.CreatedDate,
                    ModifiedBy = model.Record.ModifiedBy,
                    ModifiedDate = model.Record.ModifiedDate,
                    GroupType = model.Record.GroupType,

                };

            }
            else
            {
                entity.GroupId = model.Record.GroupId;
                entity.IsActive = model.Record.IsActive;
                entity.Name = model.Record.Name;
                entity.Code = model.Record.Code;
                entity.ModifiedBy = model.Record.ModifiedBy;
                entity.ModifiedDate = model.Record.ModifiedDate;
                entity.GroupType = model.Record.GroupType;
            }
            return entity;
        }
    }
}
