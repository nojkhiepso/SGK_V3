﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace BTS.eBook.Services
{
    public class BaseService
    {
        protected static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class SearchCachePagging<T>
        {
            public SearchCachePagging()
            {
                Items = new List<T>();
            }

            public List<T> Items { get; set; }

            public int TotalRecords { get; set; }
        }
    }
}
