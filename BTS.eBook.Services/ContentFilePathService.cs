﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.ContentFilePath;
using BTS.eBook.Core.Helper;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface IContentFilePathService
    {
        bool CreateOrUpdate(Guid versionId, string key, Guid userId);

        IEnumerable<ContentFilePathRecord> GetFilesById(Guid id);

        bool DeleteBy(Guid vId, Guid[] cfId, Guid userId);

        bool DeleteBy(Guid id, Guid uId);

        bool DeleteByVersion(Guid[] id);
    }
    public class ContentFilePathService : BaseService, IContentFilePathService
    {
        private readonly IContentFilePathRepository _contentFilePathRepository;
        private readonly ICoreUploadService _coreUploadService;
        private readonly IAccessLogService _accessLogService;

        public ContentFilePathService(ICoreUploadService coreUploadService, IContentFilePathRepository contentFilePathRepository, IAccessLogService accessLogService)
        {
            _coreUploadService = coreUploadService;
            _contentFilePathRepository = contentFilePathRepository;
            _accessLogService = accessLogService;
        }

        public bool CreateOrUpdate(Guid versionId, string key, Guid userId)
        {
            try
            {
                var coreUploads = _coreUploadService.GetFileBy(key);
                if (coreUploads != null && coreUploads.Any())
                {
                    var files = new List<ContentFilePath>();
                    Guid[] id = { };
                    var i = 0;
                    foreach (var item in coreUploads)
                    {
                        id = new Guid[coreUploads.Count()];

                        files.Add(new ContentFilePath()
                        {
                            VersionId = versionId,
                            ContentilePathId = Guid.NewGuid(),
                            CreatedDate = DateTime.Now,
                            FileName = item.Name,
                            FileType = item.Code,
                            PathUrl = item.Path
                        });

                        id[i] = item.UploadsId;
                        i++;
                    }

                    // add
                    if (_contentFilePathRepository.AddRange(files))
                    {
                        _accessLogService.InsertAccessLog(Constant.UploadFile, Constant.ModuleName.Upload, files, userId);
                        // delete 
                        return _coreUploadService.DeleteManyById(id);
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public IEnumerable<ContentFilePathRecord> GetFilesById(Guid id)
        {
            try
            {
                return _contentFilePathRepository.GetFilesById(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return null;
        }

        public bool DeleteBy(Guid vId, Guid[] cfId, Guid userId)
        {
            try
            {
                if (_contentFilePathRepository.DeleteBy(vId, cfId))
                {
                    _accessLogService.InsertAccessLog(Constant.DeleteFile, Constant.ModuleName.Upload, new { cfId, vId }, userId);

                    return true;
                };
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return false;
        }

        public bool DeleteBy(Guid id, Guid uId)
        {
            try
            {
                if (_contentFilePathRepository.DeleteBy(id))
                {
                    _accessLogService.InsertAccessLog(Constant.DeleteFile, Constant.ModuleName.Upload, new { id }, uId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return false;
        }

        public bool DeleteByVersion(Guid[] id)
        {
            try
            {
                return _contentFilePathRepository.DeleteByVersion(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return false;
        }
    }
}
