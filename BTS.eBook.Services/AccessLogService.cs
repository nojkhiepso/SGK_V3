﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.Core.Contracts.AccessLog;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.Core.Helper;
using BTS.eBook.Data.Repositories;
using Newtonsoft.Json;

namespace BTS.eBook.Services
{
    public interface IAccessLogService
    {
        SearchAccessLogResponse SearchPaging(SearchAccessLogRequest request);

        void InsertAccessLog(string action, string functionName, object obj, Guid uId);

        IEnumerable<AccessLogRecord> GetAllAccessLogRecords(int limit);
    }

    public class AccessLogService : BaseService, IAccessLogService
    {
        private readonly IAccessLogRepository _accessLogRepository;

        public AccessLogService(IAccessLogRepository accessLogRepository)
        {
            _accessLogRepository = accessLogRepository;
        }

        public SearchAccessLogResponse SearchPaging(SearchAccessLogRequest request)
        {
            try
            {
                var response = new SearchAccessLogResponse()
                {
                    Paging = new PagingResponse(),
                    Status = new ResponseStatus()
                };

                var searchCache = new SearchCachePagging<AccessLogRecord>();
                var key = "AccessLog_" + request.AccessLogName + "_" + request.FunctionName + "_" + request.UserId + "_" + request.Paging.PageIndex;
                var cacheItem = CacheManager.Instance.GetCache<SearchCachePagging<AccessLogRecord>>(key);
                if (cacheItem != null)
                {
                    searchCache = cacheItem;
                }
                else
                {
                    int totalRecords;
                    var result =
                        _accessLogRepository.SearchPaging(request.AccessLogName, request.FunctionName, request.UserId, request.Paging.PageIndex,
                            request.Paging.PageSize, out totalRecords);

                    searchCache.Items = result.ToList();
                    searchCache.TotalRecords = totalRecords;

                    CacheManager.Instance.SetCache(key, searchCache);
                }

                response.Records = searchCache.Items;
                response.Paging.TotalRecords = searchCache.TotalRecords;
                response.Status.IsSuccessFull = true;

                return response;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public void InsertAccessLog(string action, string functionName, object obj, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("AccessLog_");
                _accessLogRepository.Insert(new AccessLogRequest()
                {
                    Record = new AccessLogRecord()
                    {
                        ActionName = action,
                        CreatedDate = DateTime.Now,
                        HistoryId = Guid.NewGuid(),
                        FunctionName = functionName,
                        UserId = uId,
                        LogInfo = obj.JsonSerialize()
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public IEnumerable<AccessLogRecord> GetAllAccessLogRecords(int limit)
        {
            try
            {
                return _accessLogRepository.GetAllAccessLogRecords(limit);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return null;
        }
    }
}
