﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.Core.Contracts.Version;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface IVersionService
    {
        SearchVersionResponse SearchPagging(SearchVersionRequest request);

        bool CreateOrUpdate(VersionRequest request);

        bool DeleteBy(Guid id, Guid uId);

        CoreData.Version GetById(Guid id);

        VersionRecord GetRecordById(Guid id);

        int GetVersionNo(Guid catId);

        IEnumerable<VersionRecord> GetVersions(Guid catId);

        bool TurnOff(Guid id, string status, Guid userId);

        Guid[] GetVersionByCat(Guid id);

        Guid[] GetVersionByCat(Guid[] id);

        bool DeleteBy(Guid[] id, Guid userId);

        bool UpdateBy(int status, Guid id, Guid uId);
    }
    public class VersionService : BaseService, IVersionService
    {
        private readonly IVersionRepository _versionRepository;
        private readonly IContentFilePathService _contentFilePathService;
        private readonly IAccessLogService _accessLogService;
        private readonly IVersionGroupService _versionGroupService;

        public VersionService(IVersionRepository versionRepository, IContentFilePathService contentFilePathService, IAccessLogService accessLogService, IVersionGroupService versionGroupService)
        {
            _versionRepository = versionRepository;
            _contentFilePathService = contentFilePathService;
            _accessLogService = accessLogService;
            _versionGroupService = versionGroupService;
        }

        public SearchVersionResponse SearchPagging(SearchVersionRequest request)
        {
            try
            {
                var response = new SearchVersionResponse()
                {
                    Paging = new PagingResponse(),
                    Status = new ResponseStatus()
                };

                var searchCache = new SearchCachePagging<VersionRecord>();
                var key = "Version_" + request.CategoryId + "_" + request.IsActive + "_" + request.Status + "_" + request.CurrentUserId + "_" + request.Paging.PageIndex;
                var cacheItem = CacheManager.Instance.GetCache<SearchCachePagging<VersionRecord>>(key);
                if (cacheItem != null)
                {
                    searchCache = cacheItem;
                }
                else
                {
                    int totalRecords;
                    var result =
                        _versionRepository.SearchPaging(request.CategoryId, request.IsActive, request.Status, request.CurrentUserId, request.Paging.PageIndex,
                            request.Paging.PageSize, out totalRecords);

                    searchCache.Items = result.ToList();
                    searchCache.TotalRecords = totalRecords;

                    CacheManager.Instance.SetCache(key, searchCache);
                }

                response.Records = searchCache.Items;
                response.Paging.TotalRecords = searchCache.TotalRecords;
                response.Status.IsSuccessFull = true;

                return response;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool CreateOrUpdate(VersionRequest request)
        {
            CacheManager.Instance.RemoveCacheIfKeyContains("Version_");
            try
            {
                if (request.Record == null)
                {
                    return false;
                }

                if (request.Header.Action == 1)//add
                {
                    var entity = ToEntity(request, null);

                    if (_versionRepository.Add(entity))
                    {
                        _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.Version, entity, request.Header.UserId);
                        _contentFilePathService.CreateOrUpdate(entity.VersionId, request.Record.UploadKey, request.Header.UserId);
                        return true;
                    }
                }
                else
                {
                    var group = _versionRepository.GetById(request.Record.VersionId);
                    var entity = ToEntity(request, group);

                    if (_versionRepository.Update(entity))
                    {
                        _contentFilePathService.CreateOrUpdate(entity.VersionId, request.Record.UploadKey, request.Header.UserId);
                        _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Version, entity, request.Header.UserId);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }


        public CoreData.Version GetById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<CoreData.Version>("Version_" + id + "_GetById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _versionRepository.GetById(id);
                    CacheManager.Instance.SetCache("Version_" + id + "_GetById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public VersionRecord GetRecordById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<VersionRecord>("Version_" + id + "_GetRecordById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _versionRepository.GetRecordById(id);
                    CacheManager.Instance.SetCache("Version_" + id + "_GetRecordById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public int GetVersionNo(Guid catId)
        {
            try
            {
                return (int)_versionRepository.GetVersionNo(catId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return 0;
            }
        }

        public IEnumerable<VersionRecord> GetVersions(Guid catId)
        {
            return _versionRepository.GetVersions(catId);
        }

        public bool TurnOff(Guid id, string status, Guid userId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Version_");


                if (_versionRepository.TurnOff(id, status))
                {
                    _accessLogService.InsertAccessLog(Constant.OnOff, Constant.ModuleName.Version, new { id }, userId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public Guid[] GetVersionByCat(Guid id)
        {
            try
            {
                return _versionRepository.GetVersionByCat(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public Guid[] GetVersionByCat(Guid[] id)
        {
            try
            {
                return _versionRepository.GetVersionByCat(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool DeleteBy(Guid id, Guid uId)
        {
            return DeleteItem(new[] { id }, uId);
        }

        public bool DeleteBy(Guid[] id, Guid userId)
        {
            return DeleteItem(id, userId);
        }

        public bool UpdateBy(int status, Guid id, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Version_");

                if (_versionRepository.UpdateBy(status, id))
                {
                    _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Version, new { status, id }, uId);

                    // delete
                    if (status == 3)
                    {
                        _versionGroupService.DeleteByVersion(new[] { id });
                        _accessLogService.InsertAccessLog(Constant.Delete, Constant.ModuleName.VersionGroup, new { msg = "Update status 2->3" }, uId);
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        private bool DeleteItem(Guid[] id, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Version_");
                // delete contenfile
                _contentFilePathService.DeleteByVersion(id);
                // delete versiongroup
                _versionGroupService.DeleteByVersion(id);

                if (_versionRepository.DeleteBy(id))
                {
                    _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Version, new { id }, uId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public CoreData.Version ToEntity(VersionRequest model, CoreData.Version entity)
        {
            if (entity == null)
            {
                entity = new CoreData.Version()
                {
                    VersionId = Guid.NewGuid(),
                    CategoryId = model.Record.CategoryId,
                    CreateDate = DateTime.Now,
                    IsActive = model.Record.IsActive,
                    PathUrl = model.Record.PathUrl,
                    VersionNo = model.Record.VersionNo,
                    Summary = model.Record.Summary,
                    CreatedBy = model.Record.CreatedBy,
                    ModifiedDate = model.Record.ModifiedDate,
                    ModifedBy = model.Record.ModifiedBy,
                    Status = model.Record.Status
                };
            }
            else
            {
                entity.VersionId = model.Record.VersionId;
                entity.CategoryId = model.Record.CategoryId;
                entity.IsActive = model.Record.IsActive;
                entity.PathUrl = model.Record.PathUrl;
                entity.VersionNo = model.Record.VersionNo;
                entity.Summary = model.Record.Summary;
                entity.ModifedBy = model.Record.ModifiedBy;
                entity.ModifiedDate = model.Record.ModifiedDate;
                entity.Status = model.Record.Status;
            }
            return entity;
        }
    }
}
