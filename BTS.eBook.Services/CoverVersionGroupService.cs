﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Version;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface ICoverVersionGroupService
    {
        bool Add(CoverVersionGroup cv);

        bool AddRange(List<CoverVersionGroup> cv);

        bool DeleteBy(Guid[] id);

        IEnumerable<VersionCoverModel> GetCoverGroupBy(Guid id);
    }
    public class CoverVersionGroupService : BaseService, ICoverVersionGroupService
    {
        private readonly ICoverVersionGroupRepository _coverVersionGroupRepository;
        private readonly IAccessLogService _accessLogService;

        public CoverVersionGroupService(ICoverVersionGroupRepository coverVersionGroupRepository, IAccessLogService accessLogService)
        {
            _coverVersionGroupRepository = coverVersionGroupRepository;
            _accessLogService = accessLogService;
        }

        public bool Add(CoverVersionGroup cv)
        {
            try
            {
                return _coverVersionGroupRepository.Add(cv);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool AddRange(List<CoverVersionGroup> cv)
        {
            try
            {
                return _coverVersionGroupRepository.AddRange(cv);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool DeleteBy(Guid[] id)
        {
            try
            {
                return _coverVersionGroupRepository.DeleteBy(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex); 
            }

            return false;
        }

        public IEnumerable<VersionCoverModel> GetCoverGroupBy(Guid id)
        {
            try
            {
                return _coverVersionGroupRepository.GetCoverGroupBy(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
    }
}
