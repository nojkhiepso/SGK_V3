﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.Core.Contracts.Category;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface ICategoryService
    {
        SearchCategoryResponse SearchUser(SearchCategoryRequest request);

        bool CreateOrUpdate(CategoryRequest request);

        bool DeleteBy(Guid id, Guid uId);

        bool DeleteBy(Guid[] id, Guid uId);

        Category GetById(Guid id);

        CategoryRecord GetRecordById(Guid id);

        IEnumerable<CategoryRecord> GetAllCategories();

        // dashboard
        IEnumerable<CategoryDashBoard> GetAllCategoryDashBoards(int limit);

        bool TurnOff(Guid id, string status, Guid uId);
    }
    public class CategoryService : BaseService, ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IAccessLogService _accessLogService;
        private readonly IVersionService _versionService;
        private readonly IContentFilePathService _contentFilePathService;
        private readonly IVersionGroupService _versionGroupService;

        public CategoryService(ICategoryRepository categoryRepository, IAccessLogService accessLogService, IVersionService versionService, IContentFilePathService contentFilePathService, IVersionGroupService versionGroupService)
        {
            _categoryRepository = categoryRepository;
            _accessLogService = accessLogService;
            _versionService = versionService;
            _contentFilePathService = contentFilePathService;
            _versionGroupService = versionGroupService;
        }

        public SearchCategoryResponse SearchUser(SearchCategoryRequest request)
        {
            try
            {
                var response = new SearchCategoryResponse()
                {
                    Paging = new PagingResponse(),
                    Status = new ResponseStatus()
                };

                var searchCache = new SearchCachePagging<CategoryRecord>();
                var key = "Category_" + request.Name + "_" + request.Code + "_" + request.Paging.PageIndex;
                var cacheItem = CacheManager.Instance.GetCache<SearchCachePagging<CategoryRecord>>(key);
                if (cacheItem != null)
                {
                    searchCache = cacheItem;
                }
                else
                {
                    int totalRecords;
                    var result =
                        _categoryRepository.SearchPaging(request.Name, request.Code, request.Paging.PageIndex,
                            request.Paging.PageSize, out totalRecords);

                    searchCache.Items = result.ToList();
                    searchCache.TotalRecords = totalRecords;

                    CacheManager.Instance.SetCache(key, searchCache);
                }

                response.Records = searchCache.Items;
                response.Paging.TotalRecords = searchCache.TotalRecords;
                response.Status.IsSuccessFull = true;

                return response;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool CreateOrUpdate(CategoryRequest request)
        {
            CacheManager.Instance.RemoveCacheIfKeyContains("Category_");
            try
            {
                if (request.Record == null)
                {
                    return false;
                }

                if (request.Header.Action == 1)//add
                {
                    var entity = ToEntity(request, null);
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.Category, entity, request.Header.UserId);

                    return _categoryRepository.Add(entity);
                }
                else
                {
                    var group = _categoryRepository.GetById(request.Record.CategoryId);
                    var entity = ToEntity(request, group);
                    _accessLogService.InsertAccessLog(Constant.Edit, Constant.ModuleName.Category, entity, request.Header.UserId);

                    return _categoryRepository.Update(entity);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool DeleteBy(Guid id, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Category_");

                var versions = _versionService.GetVersionByCat(id);
                if (versions != null && versions.Length > 0)
                {
                    // delete contentfile
                    _contentFilePathService.DeleteByVersion(versions);
                    // delete version
                    _versionGroupService.DeleteByVersion(versions);
                    // delete versiongroup
                    _versionService.DeleteBy(versions, uId);
                }

                if (_categoryRepository.DeleteById(id))
                {
                    _accessLogService.InsertAccessLog(Constant.Delete, Constant.ModuleName.Category, new { id }, uId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool DeleteBy(Guid[] id, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Category_");

                var versions = _versionService.GetVersionByCat(id);
                if (versions != null && versions.Length > 0)
                {
                    // delete contentfile
                    _contentFilePathService.DeleteByVersion(versions);
                    // delete version
                    _versionGroupService.DeleteByVersion(versions);
                    // delete versiongroup
                    _versionService.DeleteBy(versions, uId);
                }

                if (_categoryRepository.DeleteById(id))
                {
                    _accessLogService.InsertAccessLog(Constant.Delete, Constant.ModuleName.Category, new { id }, uId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public Category GetById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<CoreData.Category>("Category_" + id + "_GetById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _categoryRepository.GetById(id);
                    CacheManager.Instance.SetCache("Category_" + id + "_GetById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public CategoryRecord GetRecordById(Guid id)
        {
            try
            {
                var cacheItem = CacheManager.Instance.GetCache<CategoryRecord>("Category_" + id + "_GetRecordById");
                if (cacheItem != null)
                {
                    return cacheItem;
                }
                else
                {
                    var user = _categoryRepository.GetRecordById(id);
                    CacheManager.Instance.SetCache("Category_" + id + "_GetRecordById", user);

                    return user;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<CategoryRecord> GetAllCategories()
        {
            try
            {
                return _categoryRepository.GetAllCategories();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<CategoryDashBoard> GetAllCategoryDashBoards(int limit)
        {
            try
            {
                return _categoryRepository.GetAllCategoryDashBoards(limit);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public bool TurnOff(Guid id, string status, Guid uId)
        {
            try
            {
                CacheManager.Instance.RemoveCacheIfKeyContains("Category_");


                if (_categoryRepository.TurnOff(id, status))
                {
                    _accessLogService.InsertAccessLog(Constant.OnOff, Constant.ModuleName.Category, new { id }, uId);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public CoreData.Category ToEntity(CategoryRequest model, Category entity)
        {
            if (entity == null)
            {
                entity = new CoreData.Category()
                {
                    CategoryId = Guid.NewGuid(),
                    IsActive = model.Record.IsActive,
                    Name = model.Record.Name,
                    Code = model.Record.Code,
                    ImageUrl = model.Record.ImageUrl,
                    Body = model.Record.Body,
                    Summary = model.Record.Summary,
                    RegionId = model.Record.RegionId,
                    CreatedDate = model.Record.CreatedDate,
                    CreatedBy = model.Record.CreatedBy,
                    ModifiedDate = model.Record.ModifiedDate,
                    ModifiedBy = model.Record.ModifiedBy
                };

            }
            else
            {
                entity.CategoryId = model.Record.CategoryId;
                entity.IsActive = model.Record.IsActive;
                entity.Name = model.Record.Name;
                entity.Code = model.Record.Code;
                entity.ImageUrl = model.Record.ImageUrl;
                entity.Body = model.Record.Body;
                entity.Summary = model.Record.Summary;
                entity.RegionId = model.Record.RegionId;
                entity.ModifiedBy = model.Record.ModifiedBy;
            }
            return entity;
        }
    }
}
