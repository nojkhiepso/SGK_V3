﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface IVersionGroupService
    {
        bool AddMany(List<VersionGroup> request, Guid uId);

        bool DeleteByGroupId(Guid id);

        bool DeleteByGroupId(Guid[] id);

        VersionGroup GetByGroupId(Guid id);

        Guid[] GetVersionGroupBy(Guid id);

        bool DeleteByVersion(Guid[] id);
    }

    public class VersionGroupService : BaseService, IVersionGroupService
    {
        private readonly IVersionGroupRepository _versionGroupRepository;
        private readonly IAccessLogService _accessLogService;

        public VersionGroupService(IVersionGroupRepository versionGroupRepository, IAccessLogService accessLogService)
        {
            _versionGroupRepository = versionGroupRepository;
            _accessLogService = accessLogService;
        }

        public bool AddMany(List<VersionGroup> request, Guid uId)
        {
            try
            {
                if (_versionGroupRepository.AddRange(request))
                {
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.VersionGroup, request, uId);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return false;
        }

        public bool DeleteByGroupId(Guid id)
        {
            try
            {
                return _versionGroupRepository.DeleteByGroupId(id);
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return false;
        }

        public bool DeleteByGroupId(Guid[] id)
        {
            try
            {
                return _versionGroupRepository.DeleteByGroupId(id);
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return false;
        }

        public VersionGroup GetByGroupId(Guid id)
        {
            try
            {
                return _versionGroupRepository.GetByGroupId(id);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public Guid[] GetVersionGroupBy(Guid id)
        {
            try
            {
                return _versionGroupRepository.GetVersionGroupBy(id);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public bool DeleteByVersion(Guid[] id)
        {
            try
            {
                return _versionGroupRepository.DeleteByVersion(id);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return false;
            }
        }
    }
}
