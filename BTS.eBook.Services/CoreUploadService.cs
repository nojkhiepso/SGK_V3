﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.CoreUpload;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface ICoreUploadService
    {
        bool CreateUpload(CoreUploadRequest request);

        IEnumerable<CoreUploadRecord> GetFileBy(string key);

        bool DeleteManyById(Guid[] id);

        bool DeleteById(Guid[] id, string key);
    }

    public class CoreUploadService : BaseService, ICoreUploadService
    {
        private readonly ICoreUploadRepository _coreUploadRepository;

        public CoreUploadService(ICoreUploadRepository coreUploadRepository)
        {
            _coreUploadRepository = coreUploadRepository;
        }

        public bool CreateUpload(CoreUploadRequest request)
        {
            try
            {
                return _coreUploadRepository.Add(new CoreUpload()
                {
                    UploadKey = request.Record.UploadKey,
                    Name = request.Record.Name,
                    Path = request.Record.Path,
                    UploadsId = Guid.NewGuid(),
                    Code = request.Record.Code,
                    CreateDate = DateTime.Now
                });
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return false;
            }
        }

        public IEnumerable<CoreUploadRecord> GetFileBy(string key)
        {
            try
            {
                return _coreUploadRepository.GetFileBy(key);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public bool DeleteManyById(Guid[] id)
        {
            try
            {
                return _coreUploadRepository.DeleteManyById(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool DeleteById(Guid[] id, string key)
        {
            try
            {
                return _coreUploadRepository.DeleteById(id, key);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }
    }
}
