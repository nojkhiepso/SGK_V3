﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface IGroupUserService
    {
        bool CreateOrUpdate(GroupUser request, Guid uid);

        bool AddMany(List<GroupUser> request, Guid uid);

        bool DeleteManyUserId(Guid[] id);

        bool DeleteByGroupId(Guid id);

        bool DeleteByGroupId(Guid[] id);

        Guid[] GetUserByGroupId(Guid id);
    }

    public class GroupUserService : BaseService, IGroupUserService
    {
        private readonly IGroupUserRepository _groupUserRepository;
        private readonly IAccessLogService _accessLogService;

        public GroupUserService(IGroupUserRepository groupUserRepository, IAccessLogService accessLogService)
        {
            _groupUserRepository = groupUserRepository;
            this._accessLogService = accessLogService;
        }


        public bool CreateOrUpdate(GroupUser request, Guid uid)
        {
            try
            {
                if (_groupUserRepository.Add(request))
                {
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.GroupUser, request, uid);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return false;
        }

        public bool AddMany(List<GroupUser> request, Guid uid)
        {
            try
            {
                if (_groupUserRepository.AddRange(request))
                {
                    _accessLogService.InsertAccessLog(Constant.Addnew, Constant.ModuleName.GroupUser, request, uid);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return false;
        }

        public bool DeleteManyUserId(Guid[] id)
        {
            try
            {
                _groupUserRepository.DeleteManyUserId(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool DeleteByGroupId(Guid id)
        {
            try
            {
                return _groupUserRepository.DeleteByGroupId(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool DeleteByGroupId(Guid[] id)
        {
            try
            {
                return _groupUserRepository.DeleteByGroupId(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public Guid[] GetUserByGroupId(Guid id)
        {
            try
            {
                return _groupUserRepository.GetUsersByGroupId(id);

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }
    }
}
