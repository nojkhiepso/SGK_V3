﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;

namespace BTS.eBook.Services
{
    public interface ICoverGroupService
    {
        bool Add(CoverGroup cg);

        bool AddRange(List<CoverGroup> cg);

        bool DeleteBy(Guid coverId);

        bool DeleteBy(Guid[] coverId);

        bool DeleteByGroup(Guid[] id);

        Guid[] GetCoverGroupBy(Guid id);

        Guid[] GetCoverByGroup(Guid id);

        Guid[] GetCoverByGroup(Guid[] id);
    }

    public class CoverGroupService : BaseService, ICoverGroupService
    {
        private readonly ICoverGroupRepository _coverGroupRepository;
        private readonly IAccessLogService _accessLogService;

        public CoverGroupService(ICoverGroupRepository coverGroupRepository, IAccessLogService accessLogService)
        {
            _coverGroupRepository = coverGroupRepository;
            _accessLogService = accessLogService;
        }

        public bool Add(CoverGroup cg)
        {
            try
            {
                return _coverGroupRepository.Add(cg);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool AddRange(List<CoverGroup> cg)
        {
            try
            {
                return _coverGroupRepository.AddRange(cg);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return false;
            }
        }

        public bool DeleteBy(Guid coverId)
        {
            try
            {
                return _coverGroupRepository.DeleteBy(coverId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool DeleteBy(Guid[] coverId)
        {
            try
            {
                return _coverGroupRepository.DeleteBy(coverId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public bool DeleteByGroup(Guid[] id)
        {
            try
            {
                return _coverGroupRepository.DeleteByGroup(id);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return false;
        }

        public Guid[] GetCoverGroupBy(Guid id)
        {
            try
            {
                return _coverGroupRepository.GetCoverGroupBy(id);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public Guid[] GetCoverByGroup(Guid id)
        {
            try
            {
                return _coverGroupRepository.GetCoverGroupBy(id);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public Guid[] GetCoverByGroup(Guid[] id)
        {
            try
            {
                return _coverGroupRepository.GetCoverGroupBy(id);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }
    }
}
