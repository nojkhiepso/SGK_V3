﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.ContentFilePath;
using Dapper;
using System.IO;
using System.Web;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;

namespace BTS.eBook.Data.Repositories
{
    public interface IContentFilePathRepository : IRepository<ContentFilePath>
    {
        IEnumerable<ContentFilePathRecord> GetFilesById(Guid id);

        bool DeleteBy(Guid vId, Guid[] cfId);

        bool DeleteBy(Guid id);

        bool DeleteByVersion(Guid[] id);
    }

    public class ContentFilePathRepository : Repository<ContentFilePath>, IContentFilePathRepository
    {
        public ContentFilePathRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public IEnumerable<ContentFilePathRecord> GetFilesById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.Query<ContentFilePathRecord>("SELECT * FROM ContentFilePath WHERE VersionId=@id", new { id });
            }
        }

        public bool DeleteBy(Guid vId, Guid[] cfId)
        {
            var sql = "DELETE FROM [ContentFilePath] WHERE VersionId=@vId";
            if (cfId != null && cfId.Length > 0)
            {
                sql += " AND ContentilePathId  IN @cfId";
            }

            var sqlSelect = "SELECT * FROM ContentFilePath WHERE VersionId=@vId";
            if (cfId != null && cfId.Length > 0)
            {
                sqlSelect += " AND ContentilePathId  IN @cfId";
            }


            using (var cn = Connection)
            {
                var files = cn.Query<ContentFilePath>(sqlSelect, new { vId, cfId });

                var d = cn.Execute(sql, new { vId, cfId });
                if (d > 0)
                {
                    if (files != null && files.Any())
                    {
                        string root = HttpContext.Current.Server.MapPath("~/");
                        foreach (var item in files)
                        {
                            if (File.Exists(root + item.PathUrl))
                            {
                                File.Delete(root + item.PathUrl);
                            }
                        }
                        return true;
                    }
                }
                return false;
            }
        }

        public bool DeleteBy(Guid id)
        {
            using (var cn = Connection)
            {
                var d = cn.Execute("DELETE FROM [ContentFilePath] WHERE VersionId=@id", new { id });

                return d > 0;
            }
        }

        public bool DeleteByVersion(Guid[] id)
        {
            using (var cn = Connection)
            {
                var d = cn.Execute("DELETE FROM [ContentFilePath] WHERE VersionId IN @id", new { id });

                return d > 0;
            }
        }
    }
}
