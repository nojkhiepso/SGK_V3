﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Category;
using BTS.eBook.Core.Contracts.Group;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
        IEnumerable<CategoryRecord> SearchPaging(string name, string code, int pageIndex, int pageSize, out int totalRecords);

        Category GetById(Guid id);

        CategoryRecord GetRecordById(Guid id);

        bool DeleteById(Guid id);

        bool DeleteById(Guid[] id);

        IEnumerable<CategoryRecord> GetAllCategories();

        IEnumerable<CategoryDashBoard> GetAllCategoryDashBoards(int limit);

        bool TurnOff(Guid id, string status);
    }

    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public IEnumerable<CategoryRecord> SearchPaging(string name, string code, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var cn = Connection)
            {
                var para = new DynamicParameters();
                para.Add("@name", name);
                para.Add("@code", code);
                para.Add("@pageIndex", pageIndex - 1);
                para.Add("@pageSize", pageSize);
                para.Add("@totalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var result = cn.Query<CategoryRecord>("sp_Category_Search_Paging", para, commandType: CommandType.StoredProcedure);

                totalRecords = para.Get<int>("@totalRecords");

                return result;
            }
        }

        public Category GetById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<Category>("SELECT * FROM [Category] C WHERE C.CategoryId=@id", new { id });
            }
        }

        public CategoryRecord GetRecordById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<CategoryRecord>("SELECT * FROM [Category] C WHERE C.CategoryId=@id", new { id });
            }
        }

        public bool DeleteById(Guid id)
        {
            return DeleteItem(new [] { id });
        }

        public bool DeleteById(Guid[] id)
        {
            return DeleteItem(id);
        }

        private bool DeleteItem(Guid[] id)
        {
            using (var cn = Connection)
            {
                var d = cn.Execute("DELETE FROM [Category] WHERE CategoryId IN @id", new {id});

                return d > 0;
            }
        }

        public IEnumerable<CategoryRecord> GetAllCategories()
        {
            using (var cn = Connection)
            {
                return cn.Query<CategoryRecord>("SELECT * FROM Category WHERE IsActive='TRUE'");
            }
        }

        public IEnumerable<CategoryDashBoard> GetAllCategoryDashBoards(int limit)
        {
            var query = @"SELECT TOP(" + limit + @") C.CategoryId, C.Code, C.Name, C.ImageUrl,V.VersionNo FROM Category C 
                        INNER JOIN[Version] V
                           ON V.CategoryId = C.CategoryId

                        WHERE V.IsActive = 'TRUE' AND C.IsActive = 'TRUE'
                        ORDER BY V.VersionNo";
            using (var cn = Connection)
            {
                return cn.Query<CategoryDashBoard>(query);
            }
        }

        public bool TurnOff(Guid id, string status)
        {
            using (var cn = Connection)
            {
                var u = cn.Execute("UPDATE [Category] SET IsActive=@status WHERE CategoryId = @id", new { status, id });
                return u > 0;
            }
        }
    }
}
