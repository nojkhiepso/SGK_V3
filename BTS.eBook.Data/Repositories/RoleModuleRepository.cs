﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface IRoleModuleRepository : IRepository<RoleModule>
    {
        IEnumerable<RoleModule> GetAllRoleModuleById(Guid rid);

        bool DeleteRoleModule(Guid rid);

        bool TurnOff(Guid id, string status);
    }
    public class RoleModuleRepository : Repository<RoleModule>, IRoleModuleRepository
    {
        public RoleModuleRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public IEnumerable<RoleModule> GetAllRoleModuleById(Guid rid)
        {
            using (var cn = Connection)
            {
                return cn.Query<RoleModule>("SELECT * FROM RoleModule WHERE RoleId=@rid", new { rid });
            }
        }

        public bool DeleteRoleModule(Guid rid)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM RoleModule WHERE RoleId=@rid", new { rid });
                return delete > 0;
            }
        }

        public bool TurnOff(Guid id, string status)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("UPDATE [Role] SET IsActive=@status WHERE RoleId = @id", new { status, id });
                return delete > 0;
            }
        }
    }
}
