﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface ICoverGroupRepository : IRepository<CoverGroup>
    {
        bool DeleteBy(Guid coverId);

        bool DeleteBy(Guid[] coverId);

        Guid[] GetCoverGroupBy(Guid id);

        Guid[] GetCoverGroupBy(Guid[] id);

        Guid[] GetCoverByGroup(Guid id);

        bool DeleteByGroup(Guid[] id);
    }
    public class CoverGroupRepository : Repository<CoverGroup>, ICoverGroupRepository
    {
        public CoverGroupRepository(BTS_ebookEntities context) : base(context)
        {
        }


        public bool DeleteBy(Guid coverId)
        {
            return DeleteItem(new [] { coverId });
        }

        public bool DeleteBy(Guid[] coverId)
        {
            return DeleteItem(coverId);
        }

        private bool DeleteItem(Guid[] coverId)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [CoverGroup] WHERE CoverId IN @coverId", new {coverId});

                return delete > 0;
            }
        }

        public Guid[] GetCoverGroupBy(Guid id)
        {
            return CoverGroupBy(new Guid[] { id });
        }

        private Guid[] CoverGroupBy(Guid[] id)
        {
            Guid[] g = { };

            using (var cn = Connection)
            {
                var result = cn.Query<CoverGroup>("SELECT * FROM [CoverGroup] V WHERE V.CoverId IN @id", new { id });
                if (result != null)
                {
                    var i = 0;
                    g = new Guid[result.Count()];
                    foreach (var groupUser in result)
                    {
                        g[i] = (Guid)groupUser.GroupId;
                        i++;
                    }
                }
                return g;
            }
        }

        public Guid[] GetCoverGroupBy(Guid[] id)
        {
            return CoverGroupBy(id);
        }

        public Guid[] GetCoverByGroup(Guid id)
        {
            Guid[] cv = { };

            using (var cn = Connection)
            {
                var result = cn.Query<CoverGroup>("SELECT * FROM [CoverGroup] V WHERE V.GroupId=@id", new { id });
                if (result != null)
                {
                    var i = 0;
                    cv = new Guid[result.Count()];
                    foreach (var groupUser in result)
                    {
                        cv[i] = (Guid)groupUser.CoverId;
                        i++;
                    }
                }
                return cv;
            }
        }

        public bool DeleteByGroup(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [CoverGroup] WHERE GroupId IN @id", new { id });

                return delete > 0;
            }
        }
    }
}
