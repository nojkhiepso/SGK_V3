﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface IRoleRepository : IRepository<Role>
    {
        IEnumerable<RoleRecord> GetAllRole();

        Role GetRoleById(Guid id);

        RoleRecord GetRoleRecordById(Guid id);

        bool DeleteById(Guid id);
    }

    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(BTS_ebookEntities context) : base(context)
        {
        }


        public IEnumerable<RoleRecord> GetAllRole()
        {
            using (var cn = Connection)
            {
                return cn.Query<RoleRecord>("SELECT * FROM Role WHERE IsActive='TRUE'");
            }
        }

        public Role GetRoleById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<Role>("SELECT * FROM Role  WHERE RoleId=@id", new { id });
            }
        }

        public RoleRecord GetRoleRecordById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<RoleRecord>("SELECT * FROM Role WHERE RoleId=@id", new { id });
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [Role] WHERE RoleId = @id", new { id });

                return delete > 0;
            }
        }
    }
}
