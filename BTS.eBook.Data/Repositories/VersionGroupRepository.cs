﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface IVersionGroupRepository : IRepository<VersionGroup>
    {
        bool DeleteByGroupId(Guid id);

        bool DeleteByGroupId(Guid[] id);

        bool DeleteByVersion(Guid[] id);

        VersionGroup GetByGroupId(Guid id);

        Guid[] GetVersionGroupBy(Guid id);
    }

    public class VersionGroupRepository : Repository<VersionGroup>, IVersionGroupRepository
    {
        public VersionGroupRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public bool DeleteByGroupId(Guid id)
        {
            return DeleteItem(new[] { id });
        }

        public bool DeleteByGroupId(Guid[] id)
        {
            return DeleteItem(id);
        }

        private bool DeleteItem(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [VersionGroup] WHERE GroupId IN @id", new { id });

                return delete > 0;
            }
        }

        public bool DeleteByVersion(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [VersionGroup] WHERE VersionId IN @id", new { id });

                return delete > 0;
            }
        }

        public VersionGroup GetByGroupId(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<VersionGroup>("SELECT * FROM [VersionGroup] V WHERE V.GroupId=@id", new { id });
            }
        }

        public Guid[] GetVersionGroupBy(Guid id)
        {
            Guid[] users = { };

            using (var cn = Connection)
            {
                var result = cn.Query<VersionGroup>("SELECT * FROM [VersionGroup] V WHERE V.GroupId=@id", new { id });
                if (result != null)
                {
                    var i = 0;
                    users = new Guid[result.Count()];
                    foreach (var groupUser in result)
                    {
                        users[i] = (Guid)groupUser.VersionId;
                        i++;
                    }
                }
                return users;
            }
        }
    }
}
