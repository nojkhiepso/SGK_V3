﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.AccessLog;
using BTS.eBook.Core.Contracts.Category;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface IAccessLogRepository : IRepository<AccessLog>
    {
        bool Insert(AccessLogRequest request);

        IEnumerable<AccessLogRecord> GetAllAccessLogRecords(int limit);

        IEnumerable<AccessLogRecord> SearchPaging(string acName, string fName, Guid? uId, int pageIndex, int pageSize, out int totalRecords);
    }
    public class AccessLogRepository : Repository<AccessLog>, IAccessLogRepository
    {
        public AccessLogRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public bool Insert(AccessLogRequest request)
        {
            using (var cn = Connection)
            {
                var para = new DynamicParameters();
                para.Add("@historyId", request.Record.HistoryId);
                para.Add("@logInfo", request.Record.LogInfo);
                para.Add("@functionName", request.Record.FunctionName);
                para.Add("@createdDate", request.Record.CreatedDate);
                para.Add("@actionName", request.Record.ActionName);
                para.Add("@userId", request.Record?.UserId);

                var id = cn.QueryFirstOrDefault<int>("sp_AccessLog_Insert", para, commandType: CommandType.StoredProcedure);
                return id > 0;
            }
        }

        public IEnumerable<AccessLogRecord> GetAllAccessLogRecords(int limit)
        {
            using (var cn = Connection)
            {
                string sql = @"SELECT TOP(" + limit + ")* FROM AccessLog ORDER BY CreatedDate DESC";

                return cn.Query<AccessLogRecord>(sql);
            }
        }

        public IEnumerable<AccessLogRecord> SearchPaging(string acName, string fName, Guid? uId, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var cn = Connection)
            {
                var para = new DynamicParameters();
                para.Add("@actionName", acName);
                para.Add("@functionname", fName);
                para.Add("@userId", uId);
                para.Add("@pageIndex", pageIndex - 1);
                para.Add("@pageSize", pageSize);
                para.Add("@totalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var result = cn.Query<AccessLogRecord>("sp_AccessLog_Search_Paging", para, commandType: CommandType.StoredProcedure);

                totalRecords = para.Get<int>("@totalRecords");

                return result;
            }
        }
    }
}
