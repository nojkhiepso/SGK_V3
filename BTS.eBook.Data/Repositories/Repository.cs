﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace BTS.eBook.Data.Repositories
{
    public interface IRepository<T> where T : class
    {
        DbSet<T> DbSet { get; }

        T GetById(object id);

        T Attach(T entity);

        bool Add(T entity);

        bool AddRange(List<T> entities);

        IEnumerable<T> GetPaging(int page, int pageSize);

        IEnumerable<T> GetPaging(int page, int pageSize, Expression<Func<T, bool>> expression);

        int GetTotalRecord();

        bool Update(T entity);

        bool UpdateRange(List<T> entities);

        bool Delete(T entity);

        IEnumerable<TEntity> ExecuteSql<TEntity>(string sql) where TEntity : class;

        bool DeleteRange(List<T> entities);

        Task<List<T>> GetAll();

        Task<List<T>> FindAll(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes);

        Task<T> Find(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes);

        bool Exist(Expression<Func<T, bool>> predicate = null);

        int Count(Expression<Func<T, bool>> predicate = null);
    }

    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _context;
        private IDbSet<T> _entities;

        public Repository(BTS_ebookEntities context)
        {
            _context = context;
            _context.Configuration.ProxyCreationEnabled = true;
        }

        #region interface members

        public virtual DbSet<T> DbSet => Entities;

        public virtual T Create()
        {
            return Entities.Create();
        }

        public virtual T GetById(object id)
        {
            return Entities.Find(id);
        }

        public virtual T Attach(T entity)
        {
            return Entities.Attach(entity);
        }

        public bool Add(T entity)
        {
            Entities.Add(entity);
            return _context.SaveChanges() > 0;
        }

        public bool AddRange(List<T> entities)
        {
            Entities.AddRange(entities.AsEnumerable());
            return _context.SaveChanges() > 0;
        }

        public bool DeleteRange(List<T> entities)
        {
            Entities.RemoveRange(entities.AsEnumerable());
            return _context.SaveChanges() > 0;
        }

        public IEnumerable<TEntity> ExecuteSql<TEntity>(string sql) where TEntity : class
        {
            return _context.Database.SqlQuery<TEntity>(sql);
        }

        public bool Update(T entity)
        {
            if (entity == null)
                return false;

            SetEntityStateToModifiedIfApplicable(entity);

            return _context.SaveChanges() > 0;
        }

        public bool UpdateRange(List<T> entities)
        {
            if (entities == null || entities.Count == 0)
                return false;
            foreach (var item in entities)
            {
                SetEntityStateToModifiedIfApplicable(item);
            }
            return _context.SaveChanges() > 0;
        }

        public Task<List<T>> GetAll()
        {
            return DbSet.ToListAsync();
        }

        public Task<List<T>> FindAll(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes)
        {
            var query = DbSet.Where(expression);
            if (includes != null)
            {
                foreach (var include in includes)
                {
                    query.Include(include);
                }
            }
            return query.ToListAsync();
        }

        public Task<T> Find(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes)
        {
            var query = DbSet.Where(expression);
            if (includes != null)
            {
                foreach (var include in includes)
                {
                    query.Include(include);
                }
            }
            return query.FirstOrDefaultAsync();
        }

        private void SetEntityStateToModifiedIfApplicable(T entity)
        {
            var entry = _context.Entry(entity);
            entry.State = EntityState.Modified;
        }

        public bool Delete(T entity)
        {
            DbSet.Remove(entity);
            return _context.SaveChanges() > 0;
        }

        public virtual IEnumerable<T> GetPaging(int page, int pageSize)
        {
            page = (page <= 0) ? 1 : page;
            return DbSet.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public int GetTotalRecord()
        {
            return DbSet.Count();
        }

        public IEnumerable<T> GetPaging(int page, int pageSize, Expression<Func<T, bool>> expression)
        {
            page = (page <= 0) ? 1 : page;
            return DbSet.Where(expression).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public bool Exist(Expression<Func<T, bool>> predicate = null)
        {
            return (predicate == null) ? Entities.Any() : Entities.Any(predicate);
        }

        public int Count(Expression<Func<T, bool>> predicate = null)
        {
            return (predicate == null)
                ? Entities.Count()
                : Entities.Count(predicate);
        }

        private DbSet<T> Entities
        {
            get
            {
                if (_entities == null)
                {
                    _entities = _context.Set<T>();
                }
                return _entities as DbSet<T>;
            }
        }

        public virtual void PrepareUpdate(T entity)
        {
            DbSet.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public int SaveChanges()
        {
            int result;
            try
            {
                result = _context.SaveChanges();
            }
            catch (Exception e)
            {
                ReloadEntities();
                throw new Exception("SaveChanges error", e);
            }
            return result;
        }

        public void ReloadEntities()
        {
            foreach (var entry in _context.ChangeTracker.Entries())
            {
                entry.Reload();
            }
        }

        public IDbConnection Connection
        {
            get
            {
                var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["DapperSql"].ConnectionString);
                cn.Open();

                return cn;
            }
        }

        #endregion
    }
}