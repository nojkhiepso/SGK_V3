﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Module;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface IModuleRepository : IRepository<Module>
    {
        IEnumerable<ModuleRecord> GetAllModule();

        CoreData.Module GetById(Guid id);

        ModuleRecord GetRecordById(Guid id);

        IEnumerable<ModuleRecord> GetModuleBy(Guid key);
    }

    public class ModuleRepository : Repository<Module>, IModuleRepository
    {
        public ModuleRepository(BTS_ebookEntities context) : base(context)
        {
        }


        public IEnumerable<ModuleRecord> GetAllModule()
        {
            using (var cn = Connection)
            {
                return cn.Query<ModuleRecord>("SELECT * FROM Module WHERE IsActive='TRUE'");
            }
        }

        public CoreData.Module GetById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<Module>("SELECT * FROM [Module] C WHERE C.ModuleId=@id", new { id });
            }
        }

        public ModuleRecord GetRecordById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<ModuleRecord>("SELECT * FROM [Module] C WHERE C.ModuleId=@id", new { id });
            }
        }

        public IEnumerable<ModuleRecord> GetModuleBy(Guid key)
        {
            using (var cn = Connection)
            {
                return cn.Query<ModuleRecord>("SELECT * FROM [Module] C WHERE C.ModuleId=@key", new { key });
            }
        }
    }
}
