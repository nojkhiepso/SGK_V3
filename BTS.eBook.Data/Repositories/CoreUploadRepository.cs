﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.CoreUpload;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface ICoreUploadRepository : IRepository<CoreUpload>
    {
        IEnumerable<CoreUploadRecord> GetFileBy(string key);

        bool DeleteManyById(Guid[] id);

        bool DeleteById(Guid[] id, string key);
    }

    public class CoreUploadRepository : Repository<CoreUpload>, ICoreUploadRepository
    {
        public CoreUploadRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public IEnumerable<CoreUploadRecord> GetFileBy(string key)
        {
            using (var cn = Connection)
            {
                return cn.Query<CoreUploadRecord>("SELECT * FROM CoreUploads C WHERE C.UploadKey=@key ORDER BY C.CreateDate DESC", new { key });
            }
        }

        public bool DeleteManyById(Guid[] id)
        {
            using (var cn = Connection)
            {
                var d = cn.Execute("DELETE FROM [CoreUploads] WHERE UploadsId IN @id", new { id });

                return d > 0;
            }
        }

        public bool DeleteById(Guid[] id, string key)
        {
            using (var cn = Connection)
            {
                var d = cn.Execute("DELETE FROM [CoreUploads] WHERE UploadsId IN @id AND UploadKey=@key", new { id, key });

                return d > 0;
            }
        }
    }
}
