﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Category;
using BTS.eBook.Core.Contracts.Cover;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface ICoverRepository : IRepository<Cover>
    {
        IEnumerable<CoverRecord> SearchPaging(string name, string code, int? version, int pageIndex, int pageSize, out int totalRecords);

        Cover GetById(Guid id);

        CoverRecord GetRecordById(Guid id);

        bool DeleteById(Guid id);

        bool DeleteBy(Guid[] id);

        bool UpdateStatus(Guid id);

        int? GetVersionNo(Guid id);

        int? GetOrderNo();

        bool TurnOff(Guid id, string status);
    }

    public class CoverRepository : Repository<Cover>, ICoverRepository
    {
        public CoverRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public IEnumerable<CoverRecord> SearchPaging(string name, string code, int? version, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var cn = Connection)
            {
                var para = new DynamicParameters();
                para.Add("@name", name);
                para.Add("@code", code);
                para.Add("@version", version);
                para.Add("@pageIndex", pageIndex - 1);
                para.Add("@pageSize", pageSize);
                para.Add("@totalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var result = cn.Query<CoverRecord>("sp_Cover_Search_Paging", para, commandType: CommandType.StoredProcedure);

                totalRecords = para.Get<int>("@totalRecords");

                return result;
            }
        }

        public Cover GetById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<Cover>("SELECT * FROM [Cover] C WHERE C.CoverId=@id", new { id });
            }
        }

        public CoverRecord GetRecordById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<CoverRecord>("SELECT * FROM [Cover] C WHERE C.CoverId=@id", new { id });
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [Cover] WHERE CoverId = @id", new { id });
                return delete > 0;
            }
        }

        public bool DeleteBy(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [Cover] WHERE CoverId IN @id", new { id });
                return delete > 0;
            }
        }

        public bool UpdateStatus(Guid id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("UPDATE [Cover] SET IsActive='FALSE' WHERE CoverId = @id", new { id });
                return delete > 0;
            }
        }

        public int? GetVersionNo(Guid id)
        {
            using (var cn = Connection)
            {
                var vNo = cn.QueryFirstOrDefault<int?>("SELECT MAX(C.[Version]) as VersionNo FROM [Cover] C WHERE C.ParentId=@id", new { id });
                return vNo ?? 0;
            }
        }

        public int? GetOrderNo()
        {
            using (var cn = Connection)
            {
                var vNo = cn.QueryFirstOrDefault<int?>("SELECT MAX(C.[SortOrder]) as SortOrder FROM [Cover] C");
                return vNo ?? 0;
            }
        }

        public bool TurnOff(Guid id, string status)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("UPDATE [Cover] SET IsActive=@status WHERE CoverId = @id", new { status, id });
                return delete > 0;
            }
        }
    }
}
