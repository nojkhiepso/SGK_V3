﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Group;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface IGroupRepository : IRepository<Group>
    {
        IEnumerable<GroupRecord> SearchGroup(string name, string code, int pageIndex, int pageSize, out int totalRecords);

        Group GetById(Guid id);

        GroupRecord GetRecordById(Guid id);

        bool DeleteById(Guid id);

        bool DeleteById(Guid[] id);

        IEnumerable<GroupRecord> GetAllGroupBy(int[] type);

        bool TurnOff(Guid id, string status);
    }

    public class GroupRepository : Repository<Group>, IGroupRepository
    {
        public GroupRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public IEnumerable<GroupRecord> SearchGroup(string name, string code, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var cn = Connection)
            {
                var para = new DynamicParameters();
                para.Add("@name", name);
                para.Add("@code", code);
                para.Add("@pageIndex", pageIndex - 1);
                para.Add("@pageSize", pageSize);
                para.Add("@totalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var result = cn.Query<GroupRecord>("sp_Group_Search_Paging", para, commandType: CommandType.StoredProcedure);

                totalRecords = para.Get<int>("@totalRecords");

                return result;
            }
        }

        public Group GetById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<Group>("SELECT * FROM [GROUP] G WHERE GroupId=@id", new { id });
            }
        }

        public GroupRecord GetRecordById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<GroupRecord>("SELECT * FROM [GROUP] G WHERE GroupId=@id", new { id });
            }
        }

        public bool DeleteById(Guid id)
        {
            return DeleteItem(new[] { id });
        }

        public bool DeleteById(Guid[] id)
        {
            return DeleteItem(id);
        }

        private bool DeleteItem(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [Group] WHERE GroupId IN @id", new { id });
                return delete > 0;
            }
        }

        public IEnumerable<GroupRecord> GetAllGroupBy(int[] type)
        {
            using (var cn = Connection)
            {
                return cn.Query<GroupRecord>("SELECT * FROM [Group] WHERE IsActive='TRUE' AND GroupType IN @type", new { type });
            }
        }

        public bool TurnOff(Guid id, string status)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("UPDATE [Group] SET IsActive=@status WHERE GroupId = @id", new { status, id });
                return delete > 0;
            }
        }
    }
}
