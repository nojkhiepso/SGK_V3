﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface IGroupUserRepository : IRepository<GroupUser>
    {
        bool DeleteManyUserId(Guid[] id);

        bool DeleteByGroupId(Guid id);

        bool DeleteByGroupId(Guid[] id);

        Guid[] GetUsersByGroupId(Guid id);
    }
    public class GroupUserRepository : Repository<GroupUser>, IGroupUserRepository
    {
        public GroupUserRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public bool DeleteManyUserId(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [GroupUser] WHERE UserId IN @id", new { id });

                return delete > 0;
            }
        }
        public bool DeleteByGroupId(Guid id)
        {
            return DeleteItem(new[] { id });
        }

        public bool DeleteByGroupId(Guid[] id)
        {
            return DeleteItem(id);
        }

        private bool DeleteItem(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [GroupUser] WHERE GroupId IN @id", new { id });

                return delete > 0;
            }
        }

        public Guid[] GetUsersByGroupId(Guid id)
        {
            Guid[] users = { };

            using (var cn = Connection)
            {
                var result = cn.Query<GroupUser>("SELECT * FROM GroupUser WHERE GroupId=@id", new { id });
                if (result != null)
                {
                    var i = 0;
                    users = new Guid[result.Count()];
                    foreach (var groupUser in result)
                    {
                        users[i] = (Guid)groupUser.UserId;
                        i++;
                    }
                }
                return users;
            }
        }
    }
}
