﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Client;
using BTS.eBook.Core.Contracts.Cover;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;
using Microsoft.Ajax.Utilities;

namespace BTS.eBook.Data.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        IEnumerable<UserRecord> SearchUser(string userName, string email, int pageIndex, int pageSize, out int totalRecords);

        User GetById(Guid id);

        UserRecord GetRecordById(Guid id);

        UserRecord GetUserByUserName(string userName);

        User GetUser(string userName);

        bool DeleteById(Guid id);

        bool DeleteById(Guid[] id);

        IEnumerable<UserRecord> GetAllUser();

        IEnumerable<UserFunctionRecord> GetAllUserFunction(Guid uId);

        bool ExitsEmail(string email);

        // client
        IEnumerable<UserVersionRecord> GetVersionByUser(Guid uId, List<OrderRecord> records, int versionNo);

        bool UpdateUniqueNo(string unique, Guid uId);

        bool TurnOff(Guid id, string status);

        int GetCountUser();

        int GetCountUser(string key);

        IEnumerable<User> GetAllUserBy(string userName);
    }

    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public IEnumerable<UserRecord> SearchUser(string userName, string email, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var cn = Connection)
            {
                var para = new DynamicParameters();
                para.Add("@userName", userName);
                para.Add("@email", email);
                para.Add("@pageIndex", pageIndex - 1);
                para.Add("@pageSize", pageSize);
                para.Add("@totalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var result = cn.Query<UserRecord>("sp_User_Search_Paging", para, commandType: CommandType.StoredProcedure);

                totalRecords = para.Get<int>("@totalRecords");

                return result;
            }
        }

        public User GetById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<User>("SELECT * FROM [User] U WHERE U.UserId=@id", new { id });
            }
        }

        public UserRecord GetRecordById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<UserRecord>("SELECT * FROM [User] U WHERE U.UserId=@id", new { id });
            }
        }

        public UserRecord GetUserByUserName(string userName)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<UserRecord>("SELECT * FROM [User] U WHERE  U.IsActive='TRUE' AND U.IsLocked ='FALSE' AND U.UserName=@userName", new { userName });
            }
        }

        public User GetUser(string userName)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<User>("SELECT * FROM [User] U WHERE  U.IsActive='TRUE' AND U.IsLocked ='FALSE' AND U.UserName=@userName", new { userName });
            }
        }

        public bool DeleteById(Guid id)
        {
            return DeleteItem(new[] { id });
        }

        public bool DeleteById(Guid[] id)
        {
            return DeleteItem(id);
        }

        private bool DeleteItem(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [User] WHERE UserId IN @id", new { id });
                return delete > 0;
            }
        }

        public IEnumerable<UserRecord> GetAllUser()
        {
            using (var cn = Connection)
            {
                return cn.Query<UserRecord>("SELECT * FROM [User] WHERE IsActive='TRUE' AND IsLocked ='FALSE' ORDER BY CreateTime DESC");
            }
        }

        public IEnumerable<UserFunctionRecord> GetAllUserFunction(Guid uId)
        {
            using (var cn = Connection)
            {
                return cn.Query<UserFunctionRecord>("SELECT * FROM [UserFunction] WHERE UserId=@userId", new { uId });
            }
        }

        public bool ExitsEmail(string email)
        {
            using (var cn = Connection)
            {
                var r = cn.Query<Role>("SELECT TOP(1) * FROM [User]  WHERE Email=@email", new { email });
                return r != null && r.Any();
            }
        }

        public IEnumerable<UserVersionRecord> GetVersionByUser(Guid uId, List<OrderRecord> records, int versionNo)
        {
            var rc = new List<OrderRecord>();
            if (records != null && records.Any())
            {
                rc = records.DistinctBy(x => new { x.Id, x.Version }).ToList();
            }

            if (versionNo >= 0 && !rc.Any()) // v2,v3
            {
                using (var cn = Connection)
                {
                    var para = new DynamicParameters();
                    para.Add("@userId", uId);
                    para.Add("@versionNo", versionNo);

                    var rs = cn.Query<UserVersionRecord>("sp_User_GetVersionBy", para, commandType: CommandType.StoredProcedure);
                    cn.Close();
                    return rs;
                }

            }
            else
            {
                using (var cn = Connection)
                {
                    var uvr = new List<UserVersionRecord>();
                    foreach (var or in rc)
                    {
                        int ino;
                        int.TryParse(or.Version, out ino);

                        var para = new DynamicParameters();
                        para.Add("@userId", uId);
                        para.Add("@parentId", or.Id);
                        para.Add("@versionNo", ino);

                        var rs = cn.Query<UserVersionRecord>("sp_User_GetVersionBy", para, commandType: CommandType.StoredProcedure);

                        if (rs != null && rs.Any())
                        {
                            uvr.AddRange(rs);
                        }
                    }
                    cn.Close();
                    return uvr;
                }
            }
        }

        public bool UpdateUniqueNo(string unique, Guid uId)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("UPDATE [User] SET DeviceCode=@unique WHERE UserId = @uId", new { unique, uId });
                return delete > 0;
            }
        }

        public bool TurnOff(Guid id, string status)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("UPDATE [User] SET IsActive=@status WHERE UserId = @id", new { status, id });
                return delete > 0;
            }
        }

        public int GetCountUser()
        {
            using (var cn = Connection)
            {
                var c = cn.QueryFirstOrDefault<int?>("SELECT COUNT(*) FROM [User] U");
                return c ?? 0;
            }
        }

        public int GetCountUser(string key)
        {
            using (var cn = Connection)
            {
                string term = "%" + key + "%";

                var c = cn.QueryFirstOrDefault<int?>("SELECT COUNT(*) FROM [User] U WHERE U.IsActive='TRUE' AND  U.UserName LIKE @term", new { term });
                return c ?? 0;
            }
        }

        public IEnumerable<User> GetAllUserBy(string userName)
        {
            using (var cn = Connection)
            {
                string term = "%" + userName + "%";
                return cn.Query<User>("SELECT * FROM [User] U WHERE U.IsActive='TRUE' AND U.UserName LIKE @term ORDER BY U.CreateTime ASC", new { term });
            }
        }
    }
}
