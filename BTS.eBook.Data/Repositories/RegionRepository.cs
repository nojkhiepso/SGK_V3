﻿using BTS.eBook.Core.Contracts.Region;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Data.Repositories
{

    public interface IRegionRepository : IRepository<Region>
    {
        IEnumerable<RegionRecord> SearchPaging(string name, string code, int pageIndex, int pageSize, out int totalRecords);

        Region GetById(Guid id);

        RegionRecord GetRecordById(Guid id);

        bool DeleteById(Guid id);

        IEnumerable<RegionRecord> GetAllRegion();

    }

    public class RegionRepository : Repository<Region>, IRegionRepository
    {
        public RegionRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public IEnumerable<RegionRecord> SearchPaging(string name, string code, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var cn = Connection)
            {
                var para = new DynamicParameters();
                para.Add("@name", name);
                para.Add("@code", code);
                para.Add("@pageIndex", pageIndex - 1);
                para.Add("@pageSize", pageSize);
                para.Add("@totalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var result = cn.Query<RegionRecord>("sp_Region_Search_Paging", para, commandType: CommandType.StoredProcedure);

                totalRecords = para.Get<int>("@totalRecords");

                return result;
            }
        }

        public Region GetById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<Region>("SELECT * FROM [Region] C WHERE C.RegionId=@id", new { id });
            }
        }

        public RegionRecord GetRecordById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<RegionRecord>("SELECT * FROM [Region] C WHERE C.RegionId=@id", new { id });
            }
        }

        public bool DeleteById(Guid id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE [Region] WHERE RegionId = @id", new { id });
                return delete > 0;
            }
        }

        public IEnumerable<RegionRecord> GetAllRegion()
        {
            using (var cn = Connection)
            {
                return cn.Query<RegionRecord>("SELECT * FROM Region ");
            }
        }

    }
}
