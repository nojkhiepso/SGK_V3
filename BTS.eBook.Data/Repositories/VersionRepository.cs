﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Version;
using BTS.eBook.Core.Helper;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface IVersionRepository : IRepository<CoreData.Version>
    {
        IEnumerable<VersionRecord> SearchPaging(Guid? categoryId, bool? isActive, int? status, Guid? currentUserId, int pageIndex, int pageSize, out int totalRecords);

        CoreData.Version GetById(Guid id);

        VersionRecord GetRecordById(Guid id);

        bool DeleteById(Guid id);

        int? GetVersionNo(Guid catId);

        IEnumerable<VersionRecord> GetVersions(Guid catId);

        bool TurnOff(Guid id, string status);

        Guid[] GetVersionByCat(Guid id);

        Guid[] GetVersionByCat(Guid[] id);

        bool DeleteBy(Guid[] id);

        bool UpdateBy(int status, Guid id);
    }

    public class VersionRepository : Repository<CoreData.Version>, IVersionRepository
    {
        public VersionRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public IEnumerable<VersionRecord> SearchPaging(Guid? categoryId, bool? isActive, int? status, Guid? currentUserId, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var cn = Connection)
            {
                var para = new DynamicParameters();
                para.Add("@categoryId", categoryId);
                para.Add("@isActive", isActive);
                para.Add("@currentUserId", currentUserId);
                para.Add("@status", status);
                para.Add("@pageIndex", pageIndex - 1);
                para.Add("@pageSize", pageSize);
                para.Add("@totalRecords", dbType: DbType.Int32, direction: ParameterDirection.Output);

                var result = cn.Query<VersionRecord>("sp_Version_Search_Paging", para, commandType: CommandType.StoredProcedure);

                totalRecords = para.Get<int>("@totalRecords");

                return result;
            }
        }

        public CoreData.Version GetById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<CoreData.Version>("SELECT * FROM [Version] C WHERE C.VersionId=@id", new { id });
            }
        }

        public VersionRecord GetRecordById(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.QueryFirstOrDefault<VersionRecord>("SELECT * FROM [Version] C WHERE C.VersionId=@id", new { id });
            }
        }

        public int? GetVersionNo(Guid catId)
        {
            using (var cn = Connection)
            {
                var vNo = cn.QueryFirstOrDefault<int?>("SELECT MAX(V.VersionNo) as VersionNo FROM [Version] V WHERE V.CategoryId=@catId", new { catId });
                return vNo ?? 0;
            }
        }

        public IEnumerable<VersionRecord> GetVersions(Guid catId)
        {
            using (var cn = Connection)
            {
                return cn.Query<VersionRecord>("SELECT * FROM [Version] V WHERE V.IsActive='TRUE' AND V.CategoryId=@catId", new { catId });
            }
        }

        public bool TurnOff(Guid id, string status)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("UPDATE [Version] SET IsActive=@status WHERE VersionId = @id", new { status, id });
                return delete > 0;
            }
        }

        public Guid[] GetVersionByCat(Guid id)
        {
            using (var cn = Connection)
            {
                return VersionByCat(new[] { id }, cn);
            }
        }

        private static Guid[] VersionByCat(Guid[] id, IDbConnection cn)
        {
            Guid[] vs = { };
            var v = cn.Query<CoreData.Version>("SELECT * FROM [Version] V WHERE V.CategoryId IN @id", new { id });
            if (v != null && v.Any())
            {
                vs = new Guid[v.Count()];
                var i = 0;
                foreach (var version in v)
                {
                    vs[i] = version.VersionId;
                    i++;
                }
            }
            return vs;
        }

        public Guid[] GetVersionByCat(Guid[] id)
        {
            using (var cn = Connection)
            {
                return VersionByCat(id, cn);
            }
        }


        public bool DeleteById(Guid id)
        {
            return DeleteItem(new[] { id });
        }

        public bool DeleteBy(Guid[] id)
        {
            return DeleteItem(id);
        }

        public bool UpdateBy(int status, Guid id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("UPDATE [Version] SET Status=@status WHERE VersionId = @id", new { status, id });
                return delete > 0;
            }
        }

        private bool DeleteItem(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [Version] WHERE VersionId IN @id", new { id });

                return delete > 0;
            }
        }
    }
}
