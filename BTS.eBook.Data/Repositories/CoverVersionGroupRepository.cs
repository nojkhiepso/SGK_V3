﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Version;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface ICoverVersionGroupRepository : IRepository<CoverVersionGroup>
    {
        bool DeleteBy(Guid[] id);

        IEnumerable<VersionCoverModel> GetCoverGroupBy(Guid id);
    }
    public class CoverVersionGroupRepository : Repository<CoverVersionGroup>, ICoverVersionGroupRepository
    {
        public CoverVersionGroupRepository(BTS_ebookEntities context) : base(context)
        {
        }

        public bool DeleteBy(Guid[] id)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [CoverVersionGroup] WHERE CoverId  IN @id", new { id });

                return delete > 0;
            }
        }

        public IEnumerable<VersionCoverModel> GetCoverGroupBy(Guid id)
        {
            using (var cn = Connection)
            {
                var sql = @"SELECT CV.VersionId,CV.CategoryId,CE.Name as CategoryName,V.VersionNo FROM Cover C
                            INNER JOIN CoverVersionGroup CV
	                            ON C.CoverId = CV.CoverId
                            INNER JOIN Category CE
	                            ON CE.CategoryId=CV.CategoryId
                            INNER JOIN [Version] V
	                            ON V.VersionId=CV.VersionId	
                            WHERE C.CoverId=@id";

                return cn.Query<VersionCoverModel>(sql, new { id });
            }
        }
    }
}
