﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;
using Dapper;

namespace BTS.eBook.Data.Repositories
{
    public interface IUserRoleRepository : IRepository<UserRole>
    {
        IEnumerable<UserRoleRecord> GetAllUserRole();

        IEnumerable<UserRole> GetAllUserRole(Guid id);

        bool CheckRoleExist(string name);

        IEnumerable<RoleModuleRecord> GetRoleModules(Guid roleId);

        bool DeleteRangeById(Guid[] userId);

        bool DeleteRangeByRoleId(Guid[] roleId);

        bool CheckUserInRoleExist(Guid uId);
    }

    public class UserRoleRepository : Repository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(BTS_ebookEntities context) : base(context)
        {
        }


        public IEnumerable<UserRoleRecord> GetAllUserRole()
        {
            using (var cn = Connection)
            {
                return cn.Query<UserRoleRecord>("SELECT * FROM UserRole");
            }
        }

        public IEnumerable<UserRole> GetAllUserRole(Guid id)
        {
            using (var cn = Connection)
            {
                return cn.Query<UserRole>("SELECT * FROM UserRole WHERE UserId=@id", new { id });
            }
        }

        public bool CheckRoleExist(string name)
        {
            using (var cn = Connection)
            {
                var r = cn.Query<Role>("SELECT TOP(1) * FROM [ROLE]   WHERE Name=@name", new { name });
                return r != null && r.Any();
            }
        }

        public IEnumerable<RoleModuleRecord> GetRoleModules(Guid roleId)
        {
            using (var cn = Connection)
            {
                var sql = @"SELECT RM.*,M.Name, M.Code FROM RoleModule RM
                             INNER JOIN Module M
                              ON M.ModuleId=RM.ModuleId
                            WHERE RM.RoleId=@roleId";

                return cn.Query<RoleModuleRecord>(sql, new { roleId });
            }
        }

        public bool DeleteRangeById(Guid[] userId)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [UserRole] WHERE UserId IN @userId", new { userId });
                return delete > 0;
            }
        }

        public bool DeleteRangeByRoleId(Guid[] roleId)
        {
            using (var cn = Connection)
            {
                var delete = cn.Execute("DELETE FROM [UserRole] WHERE RoleId IN @roleId", new { roleId });
                return delete > 0;
            }
        }

        public bool CheckUserInRoleExist(Guid uId)
        {
            using (var cn = Connection)
            {
                var r = cn.Query<Role>("SELECT TOP(1) * FROM [UserRole]  WHERE UserId=@uId", new { uId });
                return r != null && r.Any();
            }
        }
    }
}
