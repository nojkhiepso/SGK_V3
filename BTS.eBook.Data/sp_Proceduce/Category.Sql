﻿ 
/****** Object:  StoredProcedure [dbo].[sp_Category_Search_Paging]    Script Date: 01/06/2017 22:48:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Category_Search_Paging]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Category_Search_Paging]
GO
 
/****** Object:  StoredProcedure [dbo].[sp_Category_Search_Paging]    Script Date: 01/06/2017 22:48:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- exec sp_Category_Search_Paging '','',0,10,1

CREATE PROCEDURE [dbo].[sp_Category_Search_Paging]
	@name nvarchar(255)=NULL,
	@code nvarchar(255)=NULL,
	@pageIndex int=NULL,
	@pageSize int=NULL,
	@totalRecords int output
	
AS 
	
	SELECT
		@totalRecords = COUNT(A.CategoryId)
		
	FROM 
			Category A WITH (NOLOCK) 
	WHERE 
			(@name = '' OR @name IS NULL OR A.Name LIKE '%' + @name +'%')
			AND (@code ='' OR @code IS NULL OR A.Code LIKE '%'+@code+'%')
			
			
	DECLARE @StartRowIndex int
	SET @StartRowIndex = (@pageIndex* @pageSize) + 1;
	
	WITH [sp_Category_Search_Paging] AS
	(
		  SELECT 
				ROW_NUMBER() OVER (ORDER BY A.CategoryId DESC) AS [STT], 
				A.*
		  FROM 
				Category A WITH (NOLOCK) 
		  WHERE 
				(@name = '' OR @name IS NULL OR 	A.Name LIKE '%' + @name +'%')
				AND (@code ='' OR @code IS NULL OR A.Code LIKE '%'+@code+'%')
	)
			
	SELECT 
		*
	FROM 
		[sp_Category_Search_Paging] PSFS WITH (NOLOCK) 
	WHERE 
		PSFS.[STT] BETWEEN @StartRowIndex AND @StartRowIndex + @pageSize - 1 
	ORDER BY PSFS.[STT] 


GO


