﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BTS.eBook.Core.Helper;
using Newtonsoft.Json.Linq;

namespace BTS.eBook.Core.Mvc
{
    public class AjaxResult : ActionResult
    {
        private readonly IList<ActionBase> _actions;

        public AjaxResult()
        {
            _actions = new List<ActionBase>();
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var sb = new StringBuilder();

            foreach (var action in _actions)
            {
                action.ExecuteResult(context, sb);
            }

            var isAjaxRequest = context.HttpContext.Request.IsAjaxRequest();
            if (isAjaxRequest)
            {
                var result = new ContentResult {Content = sb.ToString(), ContentType = "application/x-javascript"};
                result.ExecuteResult(context);
            }
            else
            {
                sb.Insert(0, "<!DOCTYPE html><html><head><script type=\"text/javascript\">");
                sb.Append("</script></head></html>");

                var result = new ContentResult {Content = sb.ToString(), ContentType = "text/html"};
                result.ExecuteResult(context);
            }
        }

        public AjaxResult CloseModalDialog(string returnValue = null)
        {
            _actions.Add(new CloseModalDialogAction(returnValue));
            return this;
        }

        public AjaxResult Reload(bool parentTarget = false)
        {
            return Redirect(null, parentTarget);
        }

        public AjaxResult Redirect(string redirectUrl, bool parentTarget = false)
        {
            _actions.Add(new RedirectAction(redirectUrl, parentTarget));
            return this;
        }

        public AjaxResult Alert(string message)
        {
            _actions.Add(new AlertAction(message));
            return this;
        }

        public ActionResult HttpStatus(HttpStatusCode statusCode)
        {
            _actions.Add(new HttpStatusAction(statusCode));
            return this;
        }

        #region Ajax Actions

        public abstract class ActionBase
        {
            public abstract void ExecuteResult(ControllerContext context, StringBuilder scriptBuilder);
        }

        private class RedirectAction : ActionBase
        {
            private readonly string redirectUrl;
            private readonly bool parentTarget;

            public RedirectAction(string redirectUrl, bool parentTarget)
            {
                this.redirectUrl = redirectUrl;
                this.parentTarget = parentTarget;
            }

            public override void ExecuteResult(ControllerContext context, StringBuilder scriptBuilder)
            {
                if (string.IsNullOrEmpty(redirectUrl))
                {
                    scriptBuilder.AppendLine(parentTarget
                       ? "window.parent.location.reload();"
                       : "window.location.reload();");
                }
                else
                {
                    scriptBuilder.AppendLine(parentTarget
                        ? string.Format("window.parent.location = {0};", redirectUrl.JsEncode())
                        : string.Format("window.location = {0};", redirectUrl.JsEncode()));
                }
            }
        }

        private class AlertAction : ActionBase
        {
            private readonly string message;

            public AlertAction(string message)
            {
                this.message = message;
            }

            public override void ExecuteResult(ControllerContext context, StringBuilder scriptBuilder)
            {
                scriptBuilder.AppendLine(string.Format("alert({0});", message.JsEncode()));
            }
        }
         

        private class CloseModalDialogAction : ActionBase
        {
            private readonly string returnValue;

            public CloseModalDialogAction(string returnValue)
            {
                this.returnValue = returnValue;
            }

            public override void ExecuteResult(ControllerContext context, StringBuilder scriptBuilder)
            {
                if (returnValue != null)
                {
                    throw new NotImplementedException();
                }
                scriptBuilder.AppendLine("parent.jQuery.fancybox.close();");
            }
        }
         
        private class ExecuteScriptAction : ActionBase
        {
            private readonly string script;

            public ExecuteScriptAction(string script)
            {
                this.script = script;
            }

            public override void ExecuteResult(ControllerContext context, StringBuilder scriptBuilder)
            {
                scriptBuilder.Append(script);
            }
        }

        private class OpenContentInNewWindowAction : ActionBase
        {
            private readonly string content;

            public OpenContentInNewWindowAction(string content)
            {
                this.content = content;
            }

            public override void ExecuteResult(ControllerContext context, StringBuilder scriptBuilder)
            {
                scriptBuilder.AppendLine("parent.jQuery.fancybox.close();");
            }
        }

        private class HttpStatusAction : ActionBase
        {
            private readonly HttpStatusCode statusCode;

            public HttpStatusAction(HttpStatusCode statusCode)
            {
                this.statusCode = statusCode;
            }

            public override void ExecuteResult(ControllerContext context, StringBuilder scriptBuilder)
            {
                context.HttpContext.Response.StatusCode = (int) statusCode;
            }
        }

        #endregion Ajax Actions
    }
}