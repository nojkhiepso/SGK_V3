﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
namespace BTS.eBook.Core.Mvc
{
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session[Constant.SESSION_LOGIN_USER] == null)
            {
                filterContext.Result = new RedirectToRouteResult("u-Login", new RouteValueDictionary());
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
