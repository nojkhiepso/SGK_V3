﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Group
{
    public class SearchGroupResponse : ISearchResponse<GroupRecord>
    {
        public ResponseStatus Status { get; set; }

        public PagingResponse Paging { get; set; }

        public IList<GroupRecord> Records { get; set; }
    }
}
