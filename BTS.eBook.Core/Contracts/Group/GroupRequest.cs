﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Group
{
    public class GroupRequest : ICreationRequest<GroupRecord>
    {
        public RequestHeader Header { get; set; }

        public GroupRecord Record { get; set; }
    }
}
