﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BTS.eBook.Core.Contracts.Group
{
    public class GroupRecord
    {
        public System.Guid GroupId { get; set; }

        public int STT { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public bool IsActive { get; set; }

        public int GroupType { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public Guid CreatedBy { get; set; }

        public Guid ModifiedBy { get; set; }

        // ---
        public Guid[] Users { get; set; }

        public Guid CategoryId { get; set; }

        public Guid[] Versions { get; set; }
        
        // Aprroval
        public Guid[] Ids { get; set; }

        public Guid VersionId { get; set; }

        [AllowHtml]
        public string Body { get; set; }
    }
}
