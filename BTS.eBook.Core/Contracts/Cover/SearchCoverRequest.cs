﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Cover
{
    public class SearchCoverRequest : ISearchRequest
    {
        public PagingRequest Paging { get; set; }

        public RequestHeader Header { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public int? Version { get; set; }
    }
}
