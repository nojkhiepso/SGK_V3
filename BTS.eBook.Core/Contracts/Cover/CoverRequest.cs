﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.CoreUpload;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Cover
{
    public class CoverRequest : ICreationRequest<CoverRecord>
    {
        public RequestHeader Header { get; set; }

        public CoverRecord Record { get; set; }
    }
}
