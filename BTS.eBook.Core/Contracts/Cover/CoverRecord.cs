﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Version;

namespace BTS.eBook.Core.Contracts.Cover
{
    public class CoverRecord
    {
        public Guid CoverId { get; set; }

        public Guid? ParentId { get; set; }

        public string Name { get; set; }

        public int STT { get; set; }

        public int SortOrder { get; set; }

        public string Code { get; set; }

        public int Version { get; set; }

        public bool IsActive { get; set; }

        public DateTime? CreateDate { get; set; }

        public Guid? CreateBy { get; set; }

        // save data
        public List<VersionCoverModel> CoverVersion { get; set; }

        public Guid[] Groups { get; set; }
    }
}
