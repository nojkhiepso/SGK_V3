﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.Cover
{
    public class OrderRecord
    {
        [DisplayName("id")]
        public Guid Id { get; set; }

        [DisplayName("version")]
        public string Version { get; set; }
    }
}
