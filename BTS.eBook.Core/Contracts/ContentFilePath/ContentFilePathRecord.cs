﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.ContentFilePath
{
    public class ContentFilePathRecord
    {
        public System.Guid ContentilePathId { get; set; }

        public Guid VersionId { get; set; }

        public string FileType { get; set; }

        public string FileName { get; set; }

        public string PathUrl { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
