﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.CoreUpload
{
    public class CoreUploadRecord
    {
        public System.Guid UploadsId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Path { get; set; }

        public string UploadKey { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
