﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.User
{
    public class RoleModuleRecord
    {
        public Guid RoleModuleId { get; set; }

        public Guid RoleId { get; set; }

        public Guid ModuleId { get; set; }

        public bool Add { get; set; }

        public bool Edit { get; set; }

        public bool Delete { get; set; }

        public bool View { get; set; }

        public bool Publish { get; set; }


        // module
        public string Name { get; set; }

        public string Code { get; set; }


    }
}
