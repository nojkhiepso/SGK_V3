﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.User
{
    public class UserRoleRecord
    {
        public Guid UserRoleId { get; set; }

        public Guid RoleId { get; set; }

        public Guid UserId { get; set; }
    }
}
