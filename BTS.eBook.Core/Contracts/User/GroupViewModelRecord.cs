﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Module;

namespace BTS.eBook.Core.Contracts.User
{
    public class GroupViewModelRecord
    {
        public RoleRecord RoleRecord { get; set; }

        public List<ModuleRecord> ListModule { get; set; }
    }
}
