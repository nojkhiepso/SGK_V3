﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.User
{
    public class UserRecord
    {
        public System.Guid UserId { get; set; }

        public int  STT { get; set; }

        public string UserName { get; set; }

        public string PasswordHash { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime? LastModifyTime { get; set; }

        public bool IsLocked { get; set; }

        public string AvatarUrl { get; set; }

        public bool IsActive { get; set; }

        public int UserType { get; set; }

        public string DeviceCode { get; set; }

        public int SortOrder { get; set; }

        public Guid[] Roles { get; set; }
    }
}
