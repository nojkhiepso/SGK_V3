﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.User
{
    public class SearchUserRequest : ISearchRequest
    {
        public PagingRequest Paging { get; set; }

        public RequestHeader Header { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }
    }
}
