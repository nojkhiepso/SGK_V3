﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.User
{
    public class UserFunctionRecord
    {
        public Guid UserFunctionId { get; set; }

        public Guid UserId { get; set; }

        public string FunctionName { get; set; }
    }
}
