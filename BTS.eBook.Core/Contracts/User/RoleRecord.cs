﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.User
{
    public class RoleRecord
    {
        public Guid RoleId { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public int Type { get; set; }

    }
}
