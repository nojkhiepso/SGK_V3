﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.Client
{
    public class UserVersionRecord
    {
        public string PathUrl { get; set; }

        public string CategoryName { get; set; }

        public int VersionCatNo { get; set; }

        public Guid CoverId { get; set; }

        public Guid ParentId { get; set; }

        public int CoverVersion { get; set; }

    }
}
