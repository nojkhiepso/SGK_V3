﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.AccessLog
{
    public class SearchAccessLogResponse : ISearchResponse<AccessLogRecord>
    {
        public ResponseStatus Status { get; set; }

        public PagingResponse Paging { get; set; }

        public IList<AccessLogRecord> Records { get; set; }
    }
}
