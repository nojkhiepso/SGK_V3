﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.AccessLog
{
    public class AccessLogRecord
    {
        public System.Guid HistoryId { get; set; }

        public int  STT { get; set; }

        public string ActionName { get; set; }

        public string FunctionName { get; set; }

        public string FullName { get; set; }

        public string LogInfo { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid? UserId { get; set; }
    }
}
