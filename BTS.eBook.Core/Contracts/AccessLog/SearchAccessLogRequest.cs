﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.AccessLog
{
    public class SearchAccessLogRequest:ISearchRequest
    {
        public PagingRequest Paging { get; set; }

        public RequestHeader Header { get; set; }

        public string AccessLogName { get; set; }

        public string FunctionName { get; set; }

        public Guid? UserId { get; set; }
    }
}
