﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Version
{
    public class SearchVersionRequest : ISearchRequest
    {
        public PagingRequest Paging { get; set; }

        public RequestHeader Header { get; set; }

        public Guid? CategoryId { get; set; }

        public bool? IsActive { get; set; }

        public int? Status { get; set; }

        public Guid? CurrentUserId { get; set; }
    }
}
