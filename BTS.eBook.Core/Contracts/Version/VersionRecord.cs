﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Category;

namespace BTS.eBook.Core.Contracts.Version
{
    public class VersionRecord
    {
        public System.Guid VersionId { get; set; }

        public System.Guid CategoryId { get; set; }

        public int STT { get; set; }

        public string PathUrl { get; set; }

        public string CategoryName { get; set; }

        public string Summary { get; set; }

        public int VersionNo { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public bool IsActive { get; set; } 

        public string UploadKey { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public Guid CreatedBy { get; set; }

        public Guid ModifiedBy { get; set; }
    }


    public class VersionCoverModel
    {
        public System.Guid VersionId { get; set; }

        public System.Guid CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int VersionNo { get; set; }

    }
}
