﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Version
{
    public class SearchVersionResponse : ISearchResponse<VersionRecord>
    {
        public ResponseStatus Status { get; set; }

        public PagingResponse Paging { get; set; }

        public IList<VersionRecord> Records { get; set; }
    }
}
