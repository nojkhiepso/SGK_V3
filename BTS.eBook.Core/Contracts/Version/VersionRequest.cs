﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Version
{
    public class VersionRequest : ICreationRequest<VersionRecord>
    {
        public RequestHeader Header { get; set; }

        public VersionRecord Record { get; set; }
    }
}
