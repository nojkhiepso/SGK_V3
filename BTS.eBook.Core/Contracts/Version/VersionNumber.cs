﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.Version
{
    public class VersionNumber
    {
        public int VersionNo { get; set; }
    }
}
