﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.User;

namespace BTS.eBook.Core.Contracts.Module
{
    public class ModuleRecord
    {
        public Guid ModuleId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public bool IsActive { get; set; }

        public RoleModuleRecord RoleModuleRecord { get; set; }
    }
}
