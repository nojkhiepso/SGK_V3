﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Module
{
    public class ModuleRequest : ICreationRequest<ModuleRecord>
    {
        public RequestHeader Header { get; set; }

        public ModuleRecord Record { get; set; }
    }
}
