﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.Category
{
    public class CategoryRecord
    {
        public System.Guid CategoryId { get; set; }

        public System.Guid RegionId { get; set; }

        public int STT { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string ImageUrl { get; set; }

        public string Summary { get; set; }

        public string Body { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public Guid CreatedBy { get; set; }

        public Guid ModifiedBy { get; set; }
    }

    public class CategoryDashBoard
    {
        public System.Guid CategoryId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string ImageUrl { get; set; }

        public int VersionNo { get; set; }
    }


}
