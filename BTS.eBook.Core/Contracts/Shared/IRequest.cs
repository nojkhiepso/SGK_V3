﻿using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Shared
{
    public interface IRequest
    {
        RequestHeader Header { get; set; }
    }
}