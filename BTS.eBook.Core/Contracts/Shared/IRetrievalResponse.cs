﻿namespace BTS.eBook.Core.Contracts.Shared
{
    public interface IRetrievalResponse<T> : IResponse
    {
        T Record { get; set; }
    }
}