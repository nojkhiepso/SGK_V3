﻿using System.Collections.Generic;
using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Shared
{
    public interface ISearchResponse<T> : IResponse
    {
        PagingResponse Paging { get; set; }

        IList<T> Records { get; set; }
    }
}