﻿using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Shared
{
    public interface IResponse
    {
        ResponseStatus Status { get; set; }
    }
}