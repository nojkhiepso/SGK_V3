﻿using BTS.eBook.Core.Contracts.Shared.Records;

namespace BTS.eBook.Core.Contracts.Shared
{
    public interface ISearchRequest
    {
        PagingRequest Paging { get; set; }

        RequestHeader Header { get; set; }
    }
}