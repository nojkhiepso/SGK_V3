﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.Shared.Records
{
    public class ResponseStatus
    {
        public bool IsSuccessFull { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorCode { get; set; }
    }
}
