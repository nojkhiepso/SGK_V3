﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Contracts.Shared.Records
{
    public class RequestHeader
    {
        public string CallerName { get; set; }

        public int Action { get; set; }

        public Guid UserId { get; set; }
    }
}
