﻿namespace BTS.eBook.Core.Contracts.Shared.Records
{
    public class PagingResponse
    {
        public int TotalRecords { get; set; }
    }
}