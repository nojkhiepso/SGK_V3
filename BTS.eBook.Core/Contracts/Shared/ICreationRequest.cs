﻿namespace BTS.eBook.Core.Contracts.Shared
{
    public interface ICreationRequest<T> : IRequest
    {
        T Record { get; set; }
    }
}