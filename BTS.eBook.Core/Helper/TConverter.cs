﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Helper
{
    // Sealed, because this is meant to be the only implementation.
    public sealed class Converter<TFrom, TTo> : Converter
    {
        Func<TFrom, TTo> converter; // Converter is strongly typed.

        public Converter(Func<TFrom, TTo> converter)
            : base(typeof(TFrom), typeof(TTo)) // Can't send null types to the base.
        {
            if (converter == null)
                throw new ArgumentNullException("converter", "Converter must not be null.");
            this.converter = converter;
        }

        public override object Convert(object obj)
        {
            if (!(obj is TFrom))
            {
                var msg = string.Format("Object is not of the type {0}.", this.From.FullName);
                throw new ArgumentException(msg, "obj");
            }
            // Can throw exception, it's ok.
            return this.converter.Invoke((TFrom)obj);
        }
    }
}
