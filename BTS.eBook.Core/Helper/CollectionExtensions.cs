﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Helper
{
    public static class CollectionExtensions
    {
        public static T[] Append<T>(this T[] source, T[] destination)
        {
            var newArray = new T[source.Length + destination.Length];
            Array.Copy(source, newArray, source.Length);
            Array.Copy(destination, 0, newArray, source.Length, destination.Length);
            return newArray;
        }
    }
}
