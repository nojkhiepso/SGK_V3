﻿using System;
using System.Globalization;

namespace BTS.eBook.Core.Helper
{
    public static class DateTimeExtensions
    {
        public static string ToString(this DateTime? dt, string format)
        {
            if (dt.HasValue && dt.Value > DateTime.MinValue)
            {
                return dt.Value.ToString(format, CultureInfo.CurrentCulture);
            }
            return null;
        }

        public static string ToString(this DateTime? dt, string format, CultureInfo cultureInfo)
        {
            if (dt.HasValue && dt.Value > DateTime.MinValue)
            {
                return dt.Value.ToString(format, cultureInfo);
            }
            return null;
        }
    }
}
