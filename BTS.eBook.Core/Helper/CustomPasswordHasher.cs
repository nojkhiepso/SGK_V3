﻿using BTS.eBook.Core.Mvc;
using Microsoft.AspNet.Identity;

namespace BTS.eBook.Core.Helper
{
    public class CustomPasswordHasher : PasswordHasher
    {
        public override string HashPassword(string password)
        {
            return CryptographyHelper.HashPassword(password);
        }

        public override PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            var result = CryptographyHelper.ValidatePassword(providedPassword, hashedPassword);
            if (result)
                return PasswordVerificationResult.Success;

            return PasswordVerificationResult.Failed;
        }
    }
}