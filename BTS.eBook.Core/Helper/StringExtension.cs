﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace BTS.eBook.Core.Helper
{
    public static class StringExtension
    {
        private const string RegexArabicAndHebrew = @"[\u0600-\u06FF,\u0590-\u05FF]+";

        public static string JsEncode(this string value, bool appendQuotes = true)
        {
            if (value == null)
            {
                return string.Empty;
            }

            var sb = new StringBuilder();

            if (appendQuotes)
            {
                sb.Append("\"");
            }

            foreach (char c in value)
            {
                switch (c)
                {
                    case '\"': sb.Append("\\\""); break;
                    case '\\': sb.Append("\\\\"); break;
                    case '\b': sb.Append("\\b"); break;
                    case '\f': sb.Append("\\f"); break;
                    case '\n': sb.Append("\\n"); break;
                    case '\r': sb.Append("\\r"); break;
                    case '\t': sb.Append("\\t"); break;
                    default:
                        var i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else { sb.Append(c); }
                        break;
                }
            }

            if (appendQuotes)
            {
                sb.Append("\"");
            }

            return sb.ToString();
        }

        public static string ToSlugUrl(string value)
        {
            if (string.IsNullOrEmpty(value)) value = DateTime.Now.ToString("dd/MM/yyyy");
            var stringFormKd = value.Normalize(NormalizationForm.FormKD);
            var stringBuilder = new StringBuilder();

            foreach (var character in stringFormKd)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(character);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(character);
                }
            }

            // Replace some characters
            stringBuilder.Replace(",", "-").Replace(".", "-").Replace("&", "-").Replace("?", "-");

            var slug = stringBuilder.ToString().Normalize(NormalizationForm.FormKC);

            //First to lower case
            slug = slug.ToLowerInvariant();

            if (!IsRightToLeft(slug))
            {
                //Remove all accents
                var bytes = Encoding.GetEncoding("Cyrillic").GetBytes(slug);
                slug = Encoding.ASCII.GetString(bytes);

                //Remove invalid chars
                slug = Regex.Replace(slug, @"[^a-z0-9\s-_]", string.Empty, RegexOptions.Compiled);
            }

            //Replace spaces
            slug = Regex.Replace(slug, @"\s", "-", RegexOptions.Compiled);

            //Trim dashes from end
            slug = slug.Trim('-', '_');

            //Replace double occurences of - or _
            slug = Regex.Replace(slug, @"([-_]){2,}", "$1", RegexOptions.Compiled);

            return slug;
        }

        public static bool IsRightToLeft(string value)
        {
            if (Regex.IsMatch(value, RegexArabicAndHebrew, RegexOptions.IgnoreCase))
            {
                return true;
            }
            return false;
        }
    }
}