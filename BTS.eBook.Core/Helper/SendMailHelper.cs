﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mail;
using MailMessage = System.Net.Mail.MailMessage;
using MailPriority = System.Net.Mail.MailPriority;

namespace BTS.eBook.Core.Helper
{
    public class SendMailHelper
    {
        public static void SendMail(int type, string toAddress, string subject, string body)
        {
            MailMessage emailMessage = new MailMessage();
            emailMessage.From = new MailAddress("contact.bts.one@gmail.com", "BTS Support");
            emailMessage.To.Add(new MailAddress(toAddress));
            emailMessage.Subject = subject;
            emailMessage.Body = body;
            emailMessage.IsBodyHtml = true;
            emailMessage.Priority = MailPriority.Normal;

            // SMTP
            SmtpClient mailClient = new SmtpClient()
            {
                Host = type == 1 ? "smtp.gmail.com" : "smtp.live.com",
                //Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                Credentials = new System.Net.NetworkCredential("contact.bts.one@gmail.com", "contact@bts.vn"),
                Timeout = 20000
            };
            // Send mail
            mailClient.Send(emailMessage);
        }
    }
}
