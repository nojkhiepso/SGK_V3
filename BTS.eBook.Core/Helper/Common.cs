﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace BTS.eBook.Core.Helper
{
    public class CommonHelper
    {
        public static string NormalizeTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                return string.Empty;
            }

            var result = Regex.Replace(title, @"\s+", " ").Trim();

            for (int i = 33; i < 48; i++)
            {
                result = result.Replace(((char)i).ToString(), "");
            }

            for (int i = 58; i < 65; i++)
            {
                result = result.Replace(((char)i).ToString(), "");
            }

            for (int i = 91; i < 97; i++)
            {
                result = result.Replace(((char)i).ToString(), "");
            }

            for (int i = 123; i < 127; i++)
            {
                result = result.Replace(((char)i).ToString(), "");
            }

            result = result.Replace(((char)8220).ToString(), ""); //Replate char " open
            result = result.Replace(((char)8221).ToString(), ""); //Replate char " close
            result = result.Trim().Replace(' ', '-');
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = result.Normalize(System.Text.NormalizationForm.FormD);
            result = regex.Replace(strFormD, string.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
            result = Regex.Replace(result, @"[-]+", "-").Trim();
            result = result.ToLower();
            if (result.Length > 100)
            {
                result = result.Substring(0, 100);
            }
            return result;
        }

        public static void Upload(HttpPostedFileBase file, ref string value, ref string lblMessage, string size, int userId)
        {
            int flag = 0;
            if (file != null && file.ContentLength > 0)
            {
                if (string.IsNullOrEmpty(file.FileName))
                {
                    lblMessage = "Chọn 1 ảnh upload";
                    flag = 1;
                }
                if (file.ContentLength > 5000000)
                {
                    lblMessage = "Chọn dung lượng file nhỏ hơn 5Mb";
                    flag = 1;
                }
                Regex imageFilenameRegex = new Regex(Constant.RegexImageUpload);
                if (!imageFilenameRegex.IsMatch(file.FileName.ToLower()))
                {
                    lblMessage = "Dạng ảnh upload phải là .(jpg|jpeg|png|gif)";
                    flag = 1;
                }

                string hdfExtentionFile = Path.GetExtension(file.FileName);
                string _fileName = Path.GetFileName(file.FileName);

                if (!string.IsNullOrEmpty(_fileName) && flag != 1)
                {
                    string fileNameWithoutExtention = string.Format("{0}_{1}", DateTime.Now.ToString("yyyyMMdd-hhmmss"),
                                                                    Path.GetFileNameWithoutExtension(file.FileName));
                    Stream streamSave = file.InputStream;
                    string phy = HttpContext.Current.Server.MapPath("~");
                    string[] saSizeList;
                    if (ConfigurationManager.AppSettings[size] != null)
                    {
                        string str = ConfigurationManager.AppSettings[size].Replace(" ", "");
                        saSizeList = str.Split(',');
                    }
                    else
                    {
                        string str = "C200x150";
                        saSizeList = str.Split(',');
                    }
                    string firstFileName = string.Empty;
                    ImageHelper.InsertFileImage(streamSave, phy, "/Uploads/", userId, DateTime.Now, saSizeList, ref value,
                                                ref firstFileName, fileNameWithoutExtention, hdfExtentionFile);
                }
            }
            else
            {
                lblMessage = "Chọn 1 ảnh upload";
            }
        }


        public static string GetMd5Hash(string input)
        {
            using (MD5 md = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash. 
                byte[] data = md.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes 
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string. 
                return sBuilder.ToString();
            }
        }

        public static bool VerifyMd5Hash(string input)
        {
            string hash = GetMd5Hash(input);

            // Hash the input.
            string hashOfInput = GetMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            return false;
        }

        public static bool VerifyMd5Hash(string input, string hashPass)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hashPass))
            {
                return true;
            }
            return false;
        }


        public static bool GetSession(string sessionName, ref string sessionValueRef)
        {
            bool result = false;
            try
            {
                if (System.Web.HttpContext.Current.Session[sessionName] != null)
                {
                    sessionValueRef = Convert.ToString(System.Web.HttpContext.Current.Session[sessionName]);
                    result = true;
                }
            }
            catch (Exception value)
            {
                return false;
            }
            return result;
        }

        public static bool SetSession(string sessionName, object sessionValue)
        {
            bool result = false;
            if (!string.IsNullOrEmpty(sessionName))
            {
                System.Web.HttpContext.Current.Session.Add(sessionName, sessionValue);
                if (System.Web.HttpContext.Current.Session[sessionName] != null)
                {
                    result = true;
                }
            }
            return result;
        }

        public static string GetTokenKey(int sizeToken)
        {
            if (sizeToken < 1 || sizeToken > 50)
            {
                sizeToken = 8;
            }
            return Random("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").Substring(0, sizeToken);
        }

        public static string Random(string valueRan)
        {
            string text = string.Empty;
            try
            {
                string[] array = new string[valueRan.Length];
                for (int i = 0; i < valueRan.Length; i++)
                {
                    array[i] = char.ToString(valueRan[i]);
                }
                Random random = new Random();
                List<KeyValuePair<int, string>> list = new List<KeyValuePair<int, string>>();
                string[] array2 = array;
                for (int j = 0; j < array2.Length; j++)
                {
                    string value = array2[j];
                    list.Add(new KeyValuePair<int, string>(random.Next(), value));
                }
                IOrderedEnumerable<KeyValuePair<int, string>> orderedEnumerable = from item in list
                                                                                  orderby item.Key
                                                                                  select item;
                string[] array3 = new string[array.Length];
                int num = 0;
                foreach (KeyValuePair<int, string> current in orderedEnumerable)
                {
                    array3[num] = current.Value;
                    num++;
                }
                if (array3.Length > 0)
                {
                    for (int i = 0; i < array3.Length; i++)
                    {
                        text += array3[i];
                    }
                    text = text.Trim();
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return text;
        }


        public static string GenerateCode(int xNo)
        {
            string rNo;
            switch (xNo.ToString().Length)
            {
                case 1:
                    rNo = ("0000" + xNo);
                    break;
                case 2:
                    rNo = ("000" + xNo);
                    break;
                case 3:
                    rNo = ("00" + xNo);
                    break;
                case 4:
                    rNo = ("0" + xNo);
                    break;
                default:
                    rNo = xNo.ToString();
                    break;
            }

            return rNo;
        }

        public static string ConvertArrayToString(int[] array)
        {
            var builder = new StringBuilder();
            foreach (var value in array)
            {
                builder.AppendFormat("'{0}'", value);
                builder.Append(',');
            }
            return builder.ToString().TrimEnd(',');
        }

    }
}
