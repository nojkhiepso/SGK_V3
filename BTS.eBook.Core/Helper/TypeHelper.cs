﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Helper
{
    /// <summary>
    /// Type helper
    /// </summary>
    /// <Modified>
    /// Name     Date         Comments
    /// Bill  11/3/2015   created
    /// </Modified>
    public static class TypeHelper
    {
        public static T GetDefaultValue<T>(object obj)
        {
            T ret = default(T);
            try
            {
                if (obj == null)
                {
                    ret = (T)(object)"NULL";
                }
                else if (obj.GetType() == typeof(Guid))
                {
                    obj.TryCast<T>(out ret);
                }

                else if (obj != null)
                {
                    ret = ToType<T>(obj);
                }
            }
            catch { }
            return ret;
        }

        /// <summary>
        /// http://codereview.stackexchange.com/questions/17982/improve-my-trycastt-method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        /// <Modified>
        /// Name     Date         Comments
        /// Bill  11/3/2015   created
        /// </Modified>
        public static bool TryCast<T>(this object value, out T result)
        {
            if (value is T)
            {
                result = (T)value;
                return true;
            }

            var converter = new Converter<object, T>(s => (T)s);
            if (converter != null)
            {
                result = (T)converter.Convert(value);
                return true;
            }

            result = default(T);
            return false;
        }

        /// <summary>
        /// To the type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        /// <Modified>
        /// Name     Date         Comments
        /// Bill  11/3/2015   created
        /// </Modified>
        public static T ToType<T>(this object instance)
        {
            return (T)Convert.ChangeType(instance, typeof(T));
        }
    }
}
