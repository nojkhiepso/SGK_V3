﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.Helper
{
    public abstract class Converter
    {
        private readonly Type from; // Type of the instance to convert.
        private readonly Type to;   // Type that the instance will be converted to.

        // Internal, because we'll provide the only implementation...
        // ...that's also why we don't check if the arguments are null.
        internal Converter(Type from, Type to)
        {
            this.from = from;
            this.to = to;
        }

        public Type From { get { return this.from; } }
        public Type To { get { return this.to; } }

        public abstract object Convert(object obj);
    }
}
