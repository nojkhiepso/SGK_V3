using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace BTS.eBook.Core.Helper
{
    public class ImageHelper
    {
        #region Private Menber
        private String[] saSizeList;
        private static String sPhyDirFull = String.Empty;
        private static String sVirDirFull = String.Empty;
        private static String sFileName = String.Empty;
        static String sFileNameResult = String.Empty;
        static String UrlImage = String.Empty;
        //Duong dan mac dinh tao file anh
        private static ImageFormat ifFileExt = ImageFormat.Jpeg;
        private static String sFileExt = string.Empty;
        #endregion

        #region Public Property
        /// <summary>
        /// File Extension
        /// </summary>
        public static ImageFormat FileExtension
        {
            set
            {
                // ifFileExt = value;

                if (ifFileExt == ImageFormat.Jpeg)
                    sFileExt = ".jpg";
                else if (ifFileExt == ImageFormat.Gif)
                    sFileExt = ".gif";
                else if (ifFileExt == ImageFormat.Bmp)
                    sFileExt = ".bmp";
                else if (ifFileExt == ImageFormat.Png)
                    sFileExt = ".png";
            }
            get { return ifFileExt; }
        }

        #endregion
        /// <summary>
        /// Save file image folder
        /// </summary>
        /// <param name="stmImage"></param>
        /// <param name="sPhyHome"></param>
        /// <param name="sVirHome"></param>
        /// <param name="UserId"></param>
        /// <param name="dtCreated"></param>
        /// <param name="saSize"></param>
        /// <param name="UrlI"></param>
        /// <returns></returns>
        public static String SaveFileImage(Stream stmImage, String sPhyHome, String sVirHome, long UserId, DateTime dtCreated, String[] saSize, ref string UrlI)
        {
            if (stmImage == null || saSize == null || saSize.Length == 0)
                return String.Empty;
            try
            {
                sPhyDirFull = sPhyHome + GetDirectory(sVirHome, UserId, dtCreated, false);
                sVirDirFull = GetDirectory(sVirHome, UserId, dtCreated, true);
                sFileName = Guid.NewGuid().ToString();

                //Luu va tra lai duong dan anh goc
                SaveFileImage(stmImage, "raw");
                //Tao danh sach file anh theo kich thuoc
                foreach (String sSize in saSize)
                {
                    //CommonLib.Common.Info.Instance.WriteToLog(sSize);
                    SaveFileImage(stmImage, sSize);
                }

                return UrlI = sFileNameResult;

            }
            catch (Exception ex)
            {
                //CommonLib.Common.Info.Instance.WriteToLog(ex.ToString());
                return String.Empty;
            }

        }
        /// <summary>
        /// Save file image folder
        /// </summary>
        /// <param name="stmImage"></param>
        /// <param name="sPhyHome"></param>
        /// <param name="sVirHome"></param>
        /// <param name="UserId"></param>
        /// <param name="dtCreated"></param>
        /// <param name="saSize"></param>
        /// <param name="UrlI"></param>
        /// <param name="firstFileName"></param>
        /// <param name="extentionFile"></param>
        /// <returns></returns>
        public static String InsertFileImage(Stream stmImage, String sPhyHome, String sVirHome, long UserId, DateTime dtCreated, string[] saSize, ref string UrlI, ref string firstFileName, string fileNameWithoutExtention, string extentionFile)
        {
            if (stmImage == null || saSize == null || saSize.Length == 0)
                return String.Empty;
            try
            {
                #region Get extention FIle

                sFileExt = ".jpg";
                if (!string.IsNullOrEmpty(extentionFile))
                {

                    //Remove . character to compare
                    if ((Convert.ToString(ImageFormat.Jpeg).ToLower().Equals(extentionFile.Substring(1).ToLower())))
                    {
                        sFileExt = ".jpg";
                        ifFileExt = ImageFormat.Jpeg;
                    }
                    if ((Convert.ToString(ImageFormat.Jpeg).ToLower().Equals(extentionFile.Substring(1).ToLower())))
                    {
                        sFileExt = ".jpeg";
                        ifFileExt = ImageFormat.Jpeg;
                    }

                    else if (Convert.ToString(ImageFormat.Gif).ToLower().Equals(extentionFile.Substring(1).ToLower()))
                    {
                        sFileExt = ".gif";
                        ifFileExt = ImageFormat.Gif;
                    }
                    else if (Convert.ToString(ImageFormat.Bmp).ToLower().Equals(extentionFile.Substring(1).ToLower()))
                    {
                        sFileExt = ".bmp";
                        ifFileExt = ImageFormat.Bmp;
                    }
                    else if (Convert.ToString(ImageFormat.Png).ToLower().Equals(extentionFile.Substring(1).ToLower()))
                    {
                        sFileExt = ".png";
                        ifFileExt = ImageFormat.Png;
                    }
                }

                #endregion

                sPhyDirFull = sPhyHome + GetDirectory(sVirHome, UserId, dtCreated, false);
                sVirDirFull = GetDirectory(sVirHome, UserId, dtCreated, true);
                if (!string.IsNullOrEmpty(fileNameWithoutExtention))
                {
                    firstFileName = sFileName = fileNameWithoutExtention;
                }
                else
                {
                    firstFileName = sFileName = Guid.NewGuid().ToString();
                }

                foreach (string size in saSize)
                {
                    //CommonLib.Common.Info.Instance.WriteToLog(sSize);
                    SaveFileImage(stmImage, size);
                }
                //Luu va tra lai duong dan anh goc
                SaveFileImage(stmImage, "raw");
                return UrlI = sFileNameResult;
            }
            catch (Exception ex)
            {
                //CommonLib.Common.Info.Instance.WriteToLog(ex.ToString());
                return String.Empty;
            }

        }
        /// <summary>
        /// Image URL 
        /// </summary>
        /// <param name="objImage"></param>
        /// <param name="ImageSize"></param>
        /// <returns></returns>
        public static String ProcessImage(Object objImage, String ImageSize)
        {
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(objImage)))
                {
                    string sImage = objImage.ToString();
                    string img = string.Format("{0}_{1}{2}", sImage.Substring(0, sImage.IndexOf('.')), ImageSize, sImage.Substring(sImage.IndexOf('.')));
                    if (File.Exists(HttpContext.Current.Server.MapPath(img)))
                    {
                        return img;
                    }
                    return "/Content/images/no_photo_available_large.png";
                }
                else
                {
                    return "/Content/images/no_photo_available_large.png";
                } 
            }
            catch
            {

                return "/Content/images/no_photo_available_large.png";
            }
        }
        /// <summary>
        /// Save file image folder
        /// </summary>
        /// <param name="stmImage"></param>
        /// <param name="sPhyHome"></param>
        /// <param name="sVirHome"></param>
        /// <param name="UserId"></param>
        /// <param name="dtCreated"></param>
        /// <param name="sSize"></param>
        /// <param name="UrlImage"></param>
        /// <returns></returns>
        public static String SaveFileImage(Stream stmImage, String sPhyHome, String sVirHome, long UserId, DateTime dtCreated, String sSize, ref string UrlImage)
        {
            if (stmImage == null || String.IsNullOrEmpty(sSize))
                return String.Empty;
            try
            {
                sPhyDirFull = sPhyHome + GetDirectory(sVirHome, UserId, dtCreated, false);
                sVirDirFull = GetDirectory(sVirHome, UserId, dtCreated, true);
                sFileName = Guid.NewGuid().ToString();
                SaveFileImage(stmImage, sSize);
                //Luu va tra lai duong dan anh goc
                SaveFileImage(stmImage, "raw");
                return UrlImage = sFileNameResult;

            }
            catch (Exception ex)
            {
                //CommonLib.Common.Info.Instance.WriteToLog(ex.ToString());
                return String.Empty;
            }
        }
        /// <summary>
        /// Crop image
        /// </summary>
        /// <param name="source"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        private static Bitmap CropImage(Bitmap source, Rectangle section)
        {

            // An empty bitmap which will hold the cropped image
            Bitmap bmp = new Bitmap(section.Width, section.Height);

            Graphics g = Graphics.FromImage(bmp);

            // Draw the given area (section) of the source image
            // at location 0,0 on the empty bitmap (bmp)
            g.DrawImage(source, 0, 0, section, GraphicsUnit.Pixel);

            return bmp;
        }
        /// <summary>
        /// Get Size From Crop Size
        /// </summary>
        /// <param name="sSize"></param>
        /// <param name="iWidth"></param>
        /// <param name="iHeight"></param>
        private static void GetSizeFromCropSize(String sSize, ref int iWidth, ref int iHeight)
        {
            iWidth = Convert.ToInt32(sSize.Substring(1, sSize.LastIndexOf("x") - 1));
            iHeight = Convert.ToInt32(sSize.Substring(sSize.LastIndexOf("x") + 1));
        }
        /// <summary>
        /// Create Crop Image
        /// </summary>
        /// <param name="originalBMP"></param>
        /// <param name="filePath"></param>
        /// <param name="sSize"></param>
        /// <returns></returns>
        private static String CreateCropImage(Bitmap originalBMP, String filePath, String sSize)
        {
            try
            {
                int origWidth = originalBMP.Width;
                int origHeight = originalBMP.Height;

                int newWidth = origWidth;
                int newHeight = origHeight;

                int cropWidth = 0;
                int cropHeight = 0;

                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;

                // Check size
                GetSizeFromCropSize(sSize, ref cropWidth, ref cropHeight);

                nPercentW = ((float)cropWidth / (float)origWidth);
                nPercentH = ((float)cropHeight / (float)origHeight);

                if (cropWidth == 0)//Auto width
                {
                    nPercent = nPercentH;
                }
                else if (cropHeight == 0)//auto height
                {
                    nPercent = nPercentW;
                }
                else if (nPercentW > nPercentH)
                {
                    nPercent = nPercentH;
                }
                else
                {
                    nPercent = nPercentW;
                }

                if (nPercent < 1)
                {
                    newWidth = (int)(origWidth * nPercent);
                    newHeight = (int)(origHeight * nPercent);
                }
                else
                {
                    newWidth = (int)(origWidth);
                    newHeight = (int)(origHeight);
                }

                //float cropRate = (float)cropWidth / cropHeight;
                //float origRate = (float)origWidth / origHeight;


                //if (cropRate >= origRate)
                //{
                //    newWidth = Convert.ToInt32(cropWidth);
                //    newHeight = newWidth * origHeight / origWidth;
                //}
                //else
                //{
                //    newHeight = Convert.ToInt32(cropHeight);
                //    newWidth = newHeight * origWidth / origHeight;
                //}

                // Create a new bitmap which will hold the previous resized bitmap
                Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);

                // Create a graphic based on the new bitmap
                Graphics oGraphics = Graphics.FromImage(newBMP);
                // Set the properties for the new graphic file
                oGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                oGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                oGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

                if (!File.Exists(filePath))
                {
                    System.IO.FileStream fs = new System.IO.FileStream(filePath, FileMode.OpenOrCreate,
                                                                       FileAccess.Write);
                    // Rectangle section = new Rectangle(new Point(0, 0),new Size(Convert.ToInt32(cropWidth), Convert.ToInt32(cropHeight)));

                    // Draw the new graphic based on the resized bitmap
                    //oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);
                    System.Drawing.Rectangle rectDestination = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                    oGraphics.DrawImage(originalBMP, rectDestination, 0, 0, origWidth, origHeight, GraphicsUnit.Pixel);

                    newBMP = CropImage(newBMP, rectDestination);

                    newBMP.Save(fs, FileExtension);

                    fs.Dispose();
                }
                newBMP.Dispose();
                oGraphics.Dispose();
                return String.Format("{0}{1}_{2}{3}", sVirDirFull, sFileName, sSize, sFileExt);
            }
            catch (Exception ex)
            {
                //CommonLib.Common.Info.Instance.WriteToLog(ex.ToString());
                return String.Empty;
            }
        }
        /// <summary>
        /// Create Image
        /// </summary>
        /// <param name="originalBMP"></param>
        /// <param name="filePath"></param>
        /// <param name="iWidth"></param>
        /// <param name="iHeight"></param>
        /// <param name="sSize"></param>
        /// <returns></returns>
        private static String CreateImage(Bitmap originalBMP, String filePath, int iWidth, int iHeight, String sSize)
        {
            try
            {
                // Create a new bitmap which will hold the previous resized bitmap
                Bitmap newBMP = new Bitmap(originalBMP, iWidth, iHeight);

                // Create a graphic based on the new bitmap
                Graphics oGraphics = Graphics.FromImage(newBMP);
                // Set the properties for the new graphic file
                oGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                oGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                oGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

                // Draw the new graphic based on the resized bitmap
                //oGraphics.DrawImage(originalBMP, 0, 0, iWidth, iHeight);
                System.Drawing.Rectangle rectDestination = new System.Drawing.Rectangle(0, 0, iWidth, iHeight);
                oGraphics.DrawImage(originalBMP, rectDestination, 0, 0, iWidth, iHeight, GraphicsUnit.Pixel);

                if (!File.Exists(filePath))
                {
                    System.IO.FileStream fs = new System.IO.FileStream(filePath, System.IO.FileMode.OpenOrCreate,
                                                                       System.IO.FileAccess.Write);

                    newBMP.Save(fs, FileExtension);

                    fs.Dispose();
                }
                newBMP.Dispose();
                oGraphics.Dispose();
                return String.Format("{0}{1}_{2}{3}", sVirDirFull, sFileName, sSize, sFileExt);
            }
            catch (Exception ex)
            {
                //CommonLib.Common.Info.Instance.WriteToLog(ex.ToString());
                return String.Empty;
            }
        }
        /// <summary>
        /// Get Directory
        /// </summary>
        /// <param name="sVirHome"></param>
        /// <param name="UserId"></param>
        /// <param name="dCreated"></param>
        /// <param name="bVirtual"></param>
        /// <returns></returns>
        public static String GetDirectory(String sVirHome, long UserId, DateTime dCreated, bool bVirtual)
        {
            //Neu tao duong dan luu trong Database
            if (bVirtual)
            {
                return String.Format(@"{0}{1}/{2:0###}/{3:0#}/{4:0#}/",
                                     sVirHome,
                                     UserId,
                                     dCreated.Year,
                                     dCreated.Month,
                                     dCreated.Day).Replace("//", "/");
            }
            //tao duong dan luu tren o cung
            else
            {
                return String.Format(@"{0}{1}\{2:0###}\{3:0#}\{4:0#}\",
                                     sVirHome.Replace("/", @"\"),
                                     UserId,
                                     dCreated.Year,
                                     dCreated.Month,
                                     dCreated.Day).Replace(@"\\", @"\");
            }
        }
        /// <summary>
        /// SaveFileImage
        /// </summary>
        /// <param name="stmImage"></param>
        /// <param name="sSize"></param>
        /// <returns></returns>
        public static String SaveFileImage(Stream stmImage, String sSize)
        {
            if (!Directory.Exists(sPhyDirFull))
                Directory.CreateDirectory(sPhyDirFull);

            if (stmImage == null)
                return String.Empty;
            try
            {
                //CommonLib.Common.Info.Instance.WriteToLog(sPhyDirFull + "\n" + sFileName + "\n" + sSize);
                Bitmap originalBMP = new Bitmap(stmImage);
                String filePath = String.Format("{0}{1}{2}{3}", sPhyDirFull, sFileName, sSize.ToLower() == "raw" ? "" : "_" + sSize.ToUpper(), sFileExt);
                //CommonLib.Common.Info.Instance.WriteToLog(filePath);
                // Calculate the new image dimensions
                int origWidth = originalBMP.Width;
                int origHeight = originalBMP.Height;

                int newWidth = origWidth;
                int newHeight = origHeight;

                String sPrefix = sSize.Substring(0, 1);



                if (sPrefix.Equals("C"))
                {
                    sFileNameResult = CreateCropImage(originalBMP, filePath, sSize);

                }
                else if (sPrefix.Equals("V") || sPrefix.Equals("H"))
                {
                    if (sPrefix.Equals("V"))
                    {
                        newHeight = Convert.ToInt32(sSize.Substring(1, sSize.Length - 1));
                        newWidth = newHeight * origWidth / origHeight;
                    }
                    else if (sPrefix.Equals("H"))
                    {
                        newWidth = Convert.ToInt32(sSize.Substring(1, sSize.Length - 1));
                        newHeight = newWidth * origHeight / origWidth;
                    }

                    sFileNameResult = CreateImage(originalBMP, filePath, newWidth, newHeight, sSize);

                }
                else
                {
                    //Save Raw Image
                    if (!File.Exists(filePath))
                    {
                        System.IO.FileStream fst = new System.IO.FileStream(filePath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                        originalBMP.Save(fst, FileExtension);
                        fst.Dispose();
                        originalBMP.Dispose();

                        sFileNameResult = String.Format("{0}{1}{2}", sVirDirFull, sFileName, sFileExt);
                    }
                }
                return sFileNameResult;
            }
            catch (Exception ex)
            {
                return String.Empty;
            }
        }
        /// <summary>
        /// DeleteFile folder
        /// </summary>
        /// <param name="fullPathName"></param>
        /// <param name="firstFileName"></param>
        /// <param name="extentionFile"></param>
 
        public static void DeleteFile(string fullPathName, string firstFileName, string extentionFile)
        {
            try
            {

                if (!string.IsNullOrEmpty(firstFileName))
                {
                    //string fullPathName = Server.MapPath(pathName);
                    //browse folder
                    DirectoryInfo directoryInfo = new DirectoryInfo(fullPathName);
                    string allExtentionFile = string.Format("*{0}", extentionFile);
                    // string urlVi = ImageUtilities.GetDirectory("/Uploads/", UserId, DateTime.Now, true);
                    //get all files that contain exactly extention file
                    FileInfo[] fileInfos = directoryInfo.GetFiles(allExtentionFile);
                    //find file to delete
                    foreach (FileInfo fileInfo in fileInfos)
                    {
                        if (fileInfo.Name.Contains(firstFileName))
                        {
                            fileInfo.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
 
        /// <summary>
        /// Delete image folder
        /// </summary>
        /// <param name="VirFullPath"></param>
        /// <param name="sizesImg"></param>
        public static void ExecuteDeleteImage(string VirFullPath, string sizesImg)
        {
            string sFileName = string.Empty;
            string sFileExt = string.Empty;
            string sVirHome = string.Empty;
            SplitFileName(VirFullPath, ref sFileName, ref sVirHome, ref sFileExt);
            if (sizesImg != string.Empty)
            {
                string[] strArray = sizesImg.Split(new char[] { ',' });
                for (int i = 0; i < strArray.Length; i++)
                {
                    File.Delete(HttpContext.Current.Server.MapPath(sVirHome + sFileName + "_" + strArray[i] + sFileExt));
                }
            }
            File.Delete(HttpContext.Current.Server.MapPath(VirFullPath));
        }
        /// <summary>
        /// Split File Name
        /// </summary>
        /// <param name="sVirFullPath"></param>
        /// <param name="sFileName"></param>
        /// <param name="sVirHome"></param>
        /// <param name="sFileExt"></param>
        public static void SplitFileName(string sVirFullPath, ref string sFileName, ref string sVirHome, ref string sFileExt)
        {
            try
            {
                int num = sVirFullPath.LastIndexOf("/");
                int index = sVirFullPath.IndexOf(".");
                sVirHome = sVirFullPath.Substring(0, num + 1);
                sFileName = sVirFullPath.Substring(num + 1, (index - num) - 1);
                sFileExt = sVirFullPath.Substring(index);
            }
            catch (Exception exception)
            {
                sVirHome = string.Empty;
                sFileExt = string.Empty;
                sFileName = string.Empty;
            }
        }


        public static bool Resize(string sourcePathFile, Size size, string pathToSave)
        {
            try
            {
                //used using for remove error "The process cannot access the file"
                using (Image imgToResize = new Bitmap(sourcePathFile, true))
                {
                    int sourceWidth = imgToResize.Width;
                    int sourceHeight = imgToResize.Height;

                    float nPercent = 0;
                    float nPercentW = 0;
                    float nPercentH = 0;

                    nPercentW = ((float)size.Width / (float)sourceWidth);
                    nPercentH = ((float)size.Height / (float)sourceHeight);

                    if (nPercentH < nPercentW)
                        nPercent = nPercentH;
                    else
                        nPercent = nPercentW;

                    int destWidth = (int)(sourceWidth * nPercent);
                    int destHeight = (int)(sourceHeight * nPercent);

                    var b = new Bitmap(destWidth, destHeight);
                    Graphics g = Graphics.FromImage((Image)b);
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
                    g.Dispose();

                    pathToSave += Convert.ToString(Path.GetExtension(sourcePathFile));
                    if (!File.Exists(pathToSave))
                    {
                        using (var fs = new FileStream(pathToSave, FileMode.OpenOrCreate, FileAccess.Write))
                        {
                            b.Save(fs, imgToResize.RawFormat);
                            fs.Dispose();
                            fs.Flush();
                            fs.Close();
                        }
                        b.Dispose();
                        imgToResize.Dispose();

                        return true;
                    }
                }
            }
            catch (Exception exception)
            {
                return false;
            }
            return false;
        }

    }
}
