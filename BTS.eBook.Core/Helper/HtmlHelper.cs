﻿using System.Globalization;
using System.Text;

namespace System.Web.Mvc.Html
{
    public static class HtmlHelpers
    {
        public static HtmlString PagingHelper(this HtmlHelper htmlHelper, string actionName, string strControllerName,
            int totalPage, int pageSize, int currentPage, string sortColumn, string sortDirection)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            var str = new StringBuilder();
            var totalPages = Convert.ToInt32(Math.Ceiling((totalPage*1.0)/pageSize));
            if (totalPages > 1)
            {
                str.Append("<ul class=\"pagination pagination-sm\">");
                int pageMin, pageMax;
                int prev, next;
                var first = 1;
                var last = totalPages;

                //low
                if (currentPage <= 3)
                    pageMin = 1;
                else
                    pageMin = (currentPage - 3);

                //this is previous
                if (currentPage == 1)
                {
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-fast-backward'></i></a>");
                    str.Append("</li>");
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-arrow-left'></i></ a>");
                }
                else
                {
                    prev = currentPage - 1;
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new {column = sortColumn, direction = sortDirection, page = first},
                                new {@class = "previous paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-fast-backward'></i>"));
                    str.Append("</li>");
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new {column = sortColumn, direction = sortDirection, page = prev},
                                new {@class = "previous paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-arrow-left'></i>"));
                    str.Append("</li>");
                }

                if ((totalPages - (pageMin + 6)) >= 0)
                {
                    pageMax = pageMin + 6;
                }
                else
                {
                    pageMax = totalPages;
                }

                for (var i = pageMin; i <= pageMax; i++)
                {
                    object objAttrib = null;
                    string active;
                    if (i == currentPage)
                    {
                        active = "active";
                        objAttrib = new {@class = "active"};
                    }
                    else
                    {
                        active = "paginate_button";
                        objAttrib = new {@class = "paginate_button"};
                    }
                    str.Append("<li class='" + active + "'>");
                    str.Append(htmlHelper.ActionLink(i.ToString(CultureInfo.InvariantCulture), actionName,
                        strControllerName,
                        new {column = sortColumn, direction = sortDirection, page = i}, objAttrib));
                    str.Append("</li>");
                }

                //this is next
                if (currentPage == totalPages)
                {
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-arrow-right'></i></a>");
                    str.Append("</li>");
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-fast-forward'></i></a>");
                }
                else
                {
                    next = currentPage + 1;
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new {column = sortColumn, direction = sortDirection, page = next},
                                new {@class = "next paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-arrow-right'></i>"));
                    str.Append("</li>");
                    str.Append("</li>");
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new {column = sortColumn, direction = sortDirection, page = last},
                                new {@class = "last paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-fast-forward'></i>"));
                    str.Append("</li>");
                }
                str.Append("</ul>");
            }
            return new HtmlString(str.ToString());
        }
          
        public static HtmlString PagingOrderLineItem(this HtmlHelper htmlHelper, string actionName,
            string strControllerName, int totalPage, int pageSize, int currentPage, string sortColumn,
            string sortDirection, int status)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            var str = new StringBuilder();
            var totalPages = Convert.ToInt32(Math.Ceiling((totalPage*1.0)/pageSize));
            if (totalPages > 1)
            {
                str.Append("<ul class=\"pagination pagination-sm\">");
                int pageMin, pageMax;
                int prev, next;
                var first = 1;
                var last = totalPages;

                //low
                if (currentPage <= 3)
                    pageMin = 1;
                else
                    pageMin = (currentPage - 3);

                //this is previous
                if (currentPage == 1)
                {
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-fast-backward'></i></a>");
                    str.Append("</li>");
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-arrow-left'></i></ a>");
                }
                else
                {
                    prev = currentPage - 1;
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new {column = sortColumn, direction = sortDirection, page = first, status},
                                new {@class = "previous paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-fast-backward'></i>"));
                    str.Append("</li>");
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new {column = sortColumn, direction = sortDirection, page = prev, status},
                                new {@class = "previous paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-arrow-left'></i>"));
                    str.Append("</li>");
                }

                if ((totalPages - (pageMin + 6)) >= 0)
                {
                    pageMax = pageMin + 6;
                }
                else
                {
                    pageMax = totalPages;
                }

                for (var i = pageMin; i <= pageMax; i++)
                {
                    object objAttrib = null;
                    string active;
                    if (i == currentPage)
                    {
                        active = "active";
                        objAttrib = new {@class = "active"};
                    }
                    else
                    {
                        active = "paginate_button";
                        objAttrib = new {@class = "paginate_button"};
                    }
                    str.Append("<li class='" + active + "'>");
                    str.Append(htmlHelper.ActionLink(i.ToString(CultureInfo.InvariantCulture), actionName,
                        strControllerName,
                        new {column = sortColumn, direction = sortDirection, page = i, status}, objAttrib));
                    str.Append("</li>");
                }

                //this is next
                if (currentPage == totalPages)
                {
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-arrow-right'></i></a>");
                    str.Append("</li>");
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-fast-forward'></i></a>");
                }
                else
                {
                    next = currentPage + 1;
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new {column = sortColumn, direction = sortDirection, page = next, status},
                                new {@class = "next paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-arrow-right'></i>"));
                    str.Append("</li>");
                    str.Append("</li>");
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new {column = sortColumn, direction = sortDirection, page = last, status},
                                new {@class = "last paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-fast-forward'></i>"));
                    str.Append("</li>");
                }
                str.Append("</ul>");
            }
            return new HtmlString(str.ToString());
        }

        public static HtmlString AjaxPagingHelper(this HtmlHelper htmlHelper, string actionAjax, int countRecord,
            int pageSize, int currentPage, string sortColumn, string sortDirection)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            var str = new StringBuilder();
            var totalPages = Convert.ToInt32(Math.Ceiling((countRecord*1.0)/pageSize));
            if (totalPages > 0)
            {
                str.Append("<ul class=\"pagination pagination-sm\">");
                int pageMin, pageMax;
                int prev, next;
                var first = 1;
                var last = totalPages;

                //low
                if (currentPage <= 3)
                    pageMin = 1;
                else
                    pageMin = (currentPage - 3);

                //this is previous
                if (currentPage == 1)
                {
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-fast-backward'></i></a>");
                    str.Append("</li>");
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-arrow-left'></i></ a>");
                }
                else
                {
                    prev = currentPage - 1;
                    str.Append("<li class='cursor-pointer'>");
                    str.Append("<a class = 'first paginate_button'" +
                               " onclick='" + actionAjax + "(" + first + ")'> <i class='fa fa-fast-backward'></i></a>");
                    str.Append("</li>");
                    str.Append("<li class='cursor-pointer'>");
                    str.Append("<a class = 'previous paginate_button'" +
                               " onclick='" + actionAjax + "(" + prev + ")'> <i class='fa fa-arrow-left'></i></a>");
                    str.Append("</li>");
                }

                if ((totalPages - (pageMin + 6)) >= 0)
                {
                    pageMax = pageMin + 6;
                }
                else
                {
                    pageMax = totalPages;
                }

                for (var i = pageMin; i <= pageMax; i++)
                {
                    string objAttrib;
                    string active;
                    if (i == currentPage)
                    {
                        active = "active";
                        objAttrib = "class = 'paginate_active'";
                    }
                    else
                    {
                        active = "paginate_button";
                        objAttrib = "class='paginate_button'";
                    }

                    str.Append("<li class='cursor-pointer " + active + "'>");
                    str.Append("<a " + objAttrib + " onclick='" + actionAjax + "(" + i + ")'> " + i + "</a>");

                    str.Append("</li>");
                }

                //this is next
                if (currentPage == totalPages)
                {
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-arrow-right'></i></a>");
                    str.Append("</li>");
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-fast-forward'></i></a>");
                }
                else
                {
                    next = currentPage + 1;
                    str.Append("<li class='cursor-pointer'>");
                    str.Append("<a class = 'next paginate_button' " +
                               " onclick='" + actionAjax + "(" + next + ")'> <i class='fa fa-arrow-right'></i></a>");
                    str.Append("</li>");
                    str.Append("</li>");
                    str.Append("<li class='cursor-pointer'>");
                    str.Append("<a class = 'last paginate_button' " +
                               " onclick='" + actionAjax + "(" + last + ")'><i class='fa fa-fast-forward'></i></a>");
                    str.Append("</li>");
                }
                str.Append("</ul>");
            }
            return new HtmlString(str.ToString());
        }

        public static HtmlString PagingSearchHelper(this HtmlHelper htmlHelper, string actionName,
            string strControllerName, int totalPage, int pageSize, int currentPage, string sortColumn,
            string sortDirection, string name, string languageId, string categoryId)
        {
            currentPage = (currentPage <= 0) ? 1 : currentPage;
            var str = new StringBuilder();
            var totalPages = Convert.ToInt32(Math.Ceiling((totalPage*1.0)/pageSize));
            if (totalPages > 1)
            {
                str.Append("<ul class=\"pagination pagination-sm\">");
                int pageMin, pageMax;
                int prev, next = 0;
                var first = 1;
                var last = totalPages;

                //low
                if (currentPage <= 3)
                    pageMin = 1;
                else
                    pageMin = (currentPage - 3);

                //this is previous
                if (currentPage == 1)
                {
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-fast-backward'></i></a>");
                    str.Append("</li>");
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-arrow-left'></i></ a>");
                }
                else
                {
                    prev = currentPage - 1;
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new
                                {
                                    column = sortColumn,
                                    direction = sortDirection,
                                    name,
                                    languageId,
                                    categoryId,
                                    page = first
                                }, new {@class = "previous paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-fast-backward'></i>"));
                    str.Append("</li>");
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new
                                {
                                    column = sortColumn,
                                    direction = sortDirection,
                                    name,
                                    languageId,
                                    categoryId,
                                    page = prev
                                }, new {@class = "previous paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-arrow-left'></i>"));
                    str.Append("</li>");
                }

                if ((totalPages - (pageMin + 6)) >= 0)
                {
                    pageMax = pageMin + 6;
                }
                else
                {
                    pageMax = totalPages;
                }

                for (var i = pageMin; i <= pageMax; i++)
                {
                    object objAttrib = null;
                    string active;
                    if (i == currentPage)
                    {
                        active = "active";
                        objAttrib = new {@class = "active"};
                    }
                    else
                    {
                        active = "paginate_button";
                        objAttrib = new {@class = "paginate_button"};
                    }
                    str.Append("<li class='" + active + "'>");
                    str.Append(htmlHelper.ActionLink(i.ToString(CultureInfo.InvariantCulture), actionName,
                        strControllerName,
                        new {column = sortColumn, direction = sortDirection, name, languageId, categoryId, page = i},
                        objAttrib));
                    str.Append("</li>");
                }

                //this is next
                if (currentPage == totalPages)
                {
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-arrow-right'></i></a>");
                    str.Append(htmlHelper.ActionLink("Next", actionName, strControllerName,
                        new {column = sortColumn, direction = sortDirection, name, languageId, categoryId, page = next},
                        new {@class = "next paginate_button"}));
                    str.Append("</li>");
                    str.Append("<li class='disabled'>");
                    str.Append("<a href='javascript:void(0)'><i class='fa fa-fast-forward'></i></a>");

                    str.Append(htmlHelper.ActionLink("", actionName, strControllerName,
                        new {column = sortColumn, direction = sortDirection, name, languageId, categoryId, page = last},
                        new {@class = "last paginate_button disabled"}));
                }
                else
                {
                    next = currentPage + 1;
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new
                                {
                                    column = sortColumn,
                                    direction = sortDirection,
                                    name,
                                    languageId,
                                    categoryId,
                                    page = next
                                }, new {@class = "next paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-arrow-right'></i>"));
                    str.Append("</li>");
                    str.Append("</li>");
                    str.Append("<li>");
                    str.Append(
                        string.Format(
                            htmlHelper.ActionLink("{0}", actionName, strControllerName,
                                new
                                {
                                    column = sortColumn,
                                    direction = sortDirection,
                                    name,
                                    languageId,
                                    categoryId,
                                    page = last
                                }, new {@class = "last paginate_button"}).ToHtmlString(),
                            "<i class='fa fa-fast-forward'></i>"));
                    str.Append("</li>");
                }
                str.Append("</ul>");
            }
            return new HtmlString(str.ToString());
        }

        public static HtmlString T(this HtmlHelper htmlHelper, string resourceKey)
        {
            return new HtmlString("");
        }

        public static MvcHtmlString SubStringMvc(this object textString, int length)
        {
            var s = textString.ToString();
            if (String.IsNullOrEmpty(s))
                throw new ArgumentNullException(s);
            var words = s.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if (words[0].Length > length)
                throw new ArgumentException("Từ đầu tiên dài hơn chuỗi cần cắt");
            var sb = new StringBuilder();
            foreach (var word in words)
            {
                if ((sb + word).Length > length)
                    return MvcHtmlString.Create(string.Format("{0}...", sb.ToString().TrimEnd(' ')));
                sb.Append(word + " ");
            }
            return MvcHtmlString.Create(string.Format("{0}", sb.ToString().TrimEnd(' ')));
        }
    }
}