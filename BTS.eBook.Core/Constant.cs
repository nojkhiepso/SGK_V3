﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core
{
    public class Constant
    {
        public const int DefaultPageSize = 25;
        public const string SESSION_LOGIN_USER = "SESSION_LOGIN_USER";
        public const string RegexImageUpload = @"(.*?)\.(jpg|jpeg|png|gif)$";
        public static int CacheTimeout = 20;

        public const string Addnew = "Thêm mới bản ghi";
        public const string Edit = "Sửa bản ghi";
        public const string Delete = "Xóa bản ghi";
        public const string OnOff = "Tắt - bật";
        public const string ChangePass = "Đổi mật khẩu";
        public const string DeleteFile = "Xóa file";
        public const string Login = "Đăng nhập";
        public const string UploadFile = "Tải file";

        public class ModuleName
        {
            public const string Category = "Quản lý sách";
            public const string Cover = "Tổng hợp";
            public const string CoverVersionGroup = "Tổng hợp - Phiên bản";
            public const string CoverGroup = "Tổng hợp - Nhóm";
            public const string Version = "Quản lý nội dung";
            public const string Group = "Quản lý nhóm";
            public const string User = "Quản lý người dùng";
            public const string Upload = "Tải file";
            public const string GroupUser = "Nhóm-người dùng";
            public const string VersionGroup = "Nhóm-phiên bản";
            public const string Role = "Quyền";
            public const string RoleModule = "Quyền chức năng";
            public const string UserRole = "Quyền người dùng";
            public const string Module = "Quản lý chức năng";
            public const string Region = "Quản lý lĩnh vực";
        }

        public class ModuleFunction
        {
            public const string Category = "Category";
            public const string Cover = "Cover";
            public const string Version = "Version";
            public const string Group = "Group";
            public const string User = "User";
            public const string Upload = "Upload";
            public const string GroupUser = "GroupUser";
            public const string VersionGroup = "VersionGroup";
            public const string Role = "Role";
            public const string RoleModule = "RoleModule";
            public const string UserRole = "UserRole";
            public const string Module = "Module";
            public const string Setting = "Setting";
            public const string Region = "Region";
        }

        public class VersionTemplate
        {
            public const string Draf = "Bản nháp";
            public const string Publish = "Xuất bản";
        }

        public string[] VersionStatus()
        {
            return new[]
            {
               VersionTemplate.Draf,
               VersionTemplate.Publish
            };
        }

    }
}
