﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.eBook.Core.CacheHelper
{
    
    public interface ICacheProvider
    {
        T Get<T>(string key) where T : class;
        void Remove(string key);

        void Insert<T>(string key, T obj, int time) where T : class;

        bool Contains(string key);

        IEnumerator<string> GetKeysEnumerator();
    }
}
