﻿using System;
using System.Collections.Generic;
using BTS.eBook.Core.Patterns;
using BTS.eBook.Core.Helper;

namespace BTS.eBook.Core.CacheHelper
{
    public class CacheManager
    {
        private static bool enabled = true;
        private static object tobj = new object();
        public const int CACHE_DEFAULT_TIME = 20;
        private ICacheProvider _cache;

        public CacheManager()
        {
            if (_cache == null)
                _cache = new CacheProvider();
        }

        public static CacheManager Instance
        {
            get
            {
                return Singleton<CacheManager>.Instance;
            }
        }

        public void SetCache<T>(string cacheKey, T obj, int time) where T : class
        {
            if (!enabled) return;
            lock (tobj)
            {
                if (obj == null) return;
                this._cache.Remove(cacheKey);
                this._cache.Insert(cacheKey, obj, time);
            }
        }

        public void SetCache<T>(string cacheKey, T obj) where T : class
        {
            SetCache(cacheKey, obj, CACHE_DEFAULT_TIME);
        }

        public void SetCacheByKeys<T>(T obj, params object[] keys) where T : class
        {
            SetCacheByKeys<T>(obj, CACHE_DEFAULT_TIME, keys);
        }
        public void SetCacheByKeys<T>(T obj, int time, params object[] keys) where T : class
        {
            if (!enabled) return;
            if (obj == null) return;
            string key = ConstructCacheKey(keys);
            if (!string.IsNullOrEmpty(key)) SetCache(key, obj, time);
        }

        public T GetCacheByKeys<T>(params object[] keys) where T : class
        {
            if (!enabled) return default(T);
            string ckey = ConstructCacheKey(keys);
            return GetCache<T>(ckey);

        }

        public T GetCache<T>(string cacheKey) where T : class
        {
            if (!enabled) return default(T);
            lock (tobj)
            {
                object obj = this._cache.Get<T>(cacheKey);

                return (obj != null && obj is T) ? (T)obj : default(T);
            }
        }

        public void RemoveCache(string cacheKey)
        {
            if (!enabled) return;

            lock (tobj)
            {
                this._cache.Remove(cacheKey);
            }
        }

        public void RemoveAll()
        {
            lock (tobj)
            {
                var ce = this._cache.GetKeysEnumerator();
                while (ce.MoveNext()) this._cache.Remove(ce.Current);
            }
        }

        public void RemoveCacheByKeys(params object[] oKeys)
        {
            if (!enabled) return;

            if (oKeys == null || oKeys.Length <= 0) return;

            string[] keys = GetCacheItemStrings(oKeys);

            lock (tobj)
            {
                var ce = this._cache.GetKeysEnumerator();

                while (ce.MoveNext())
                {
                    string key = ce.Current;
                    bool ok = true;
                    foreach (string k in keys)
                    {
                        if (!string.IsNullOrEmpty(k))
                        {
                            if (!key.Contains(k, StringComparison.InvariantCultureIgnoreCase))
                            {
                                ok = false;
                                break;
                            }
                        }
                    }

                    if (ok)
                    {
                        this._cache.Remove(key);
                    }
                }
            }
        }

        public void RemoveCacheIfKeyContains(params string[] keys)
        {
            if (!enabled) return;

            if (keys == null || keys.Length == 0) return;
            lock (tobj)
            {
                var ce = this._cache.GetKeysEnumerator();

                while (ce.MoveNext())
                {
                    string key = ce.Current;
                    foreach (string k in keys)
                    {
                        if (!string.IsNullOrEmpty(k))
                        {
                            if (key.Contains(k, StringComparison.InvariantCultureIgnoreCase))
                            {
                                // Remove cache luon.
                                this._cache.Remove(key);

                                break;
                            }
                        }
                    }
                }
            }
        }

        private string[] GetCacheItemStrings(object[] arr)
        {
            if (arr == null || arr.Length == 0) return null;

            string[] keys = new string[arr.Length];
            for (int i = 0; i < arr.Length; ++i)
            {
                object ob = TypeHelper.GetDefaultValue<object>(arr[i]);

                if (ob is CacheKeys)
                {
                    keys[i] = "[" + ob.ToString().ToLower() + "]";
                }
                else keys[i] = "(" + ob.ToString().ToLower() + ")";
            }
            return keys;

        }

        public string ConstructCacheKey(params object[] objs)
        {
            string[] keys = GetCacheItemStrings(objs);

            if (keys == null) return string.Empty;

            return string.Join("_", keys);
        }

        public bool CheckCacheKey(params string[] keys)
        {
            bool ok = false;
            if (!enabled) return false;

            if (keys == null || keys.Length == 0) return false;

            lock (tobj)
            {
                var ce = this._cache.GetKeysEnumerator();

                while (ce.MoveNext())
                {
                    string key = ce.Current;
                    foreach (string k in keys)
                    {
                        if (!string.IsNullOrEmpty(k))
                            if (key.Contains(k))
                            {
                                ok = true;
                                break;
                            }
                    }
                }
            }
            return ok;
        }

        public void Remove(Predicate<string> predicate)
        {
            // GetEnumerator
            var key = this._cache.GetKeysEnumerator();

            // cycle through cache keys
            while (key.MoveNext())
            {
                // remove cache item if predicate returns true
                if (predicate(key.Current))
                {
                    this._cache.Remove(key.Current);
                }
            }
        }

        public string GetAllKeys()
        {
            List<string> keys = null;
            lock (tobj)
            {
                keys = new List<string>();
                var ce = this._cache.GetKeysEnumerator();
                while (ce.MoveNext())
                {
                    keys.Add(ce.Current);
                }
            }
            return (keys != null && keys.Count > 0) ? string.Join("<br/> - ", keys) : string.Empty;
        }
    }
}
