﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace BTS.eBook.Core.CacheHelper
{
    
    public class CacheProvider : ICacheProvider
    {
        private readonly System.Web.Caching.Cache _cache = HttpRuntime.Cache;


        public T Get<T>(string key) where T : class
        {
            return this._cache[key] as T;
        }

        
        public void Remove(string key)
        {
            this._cache.Remove(key);
        }

        public void Insert<T>(string key, T obj, int time) where T : class
        {
            if (time > 0)
                this._cache.Insert(key, obj, null, DateTime.Now.AddMinutes(time), TimeSpan.Zero);
            else this._cache.Insert(key, obj, null, Cache.NoAbsoluteExpiration, TimeSpan.Zero);
        }

        public bool Contains(string key)
        {
            return this._cache[key] != null;
        }
         
        public IEnumerator<string> GetKeysEnumerator()
        {
            return this._cache.Cast<DictionaryEntry>().Select(d => d.Key.ToString()).GetEnumerator();
        }
    }
}
