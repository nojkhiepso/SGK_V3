﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BTS.eBook.Admin.Controllers.Api.Model;
using BTS.eBook.Core.Contracts.Client;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.Core.Helper;
using BTS_eBook.Tests.ServiceReference;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BTS_eBook.Tests
{
    [TestClass]
    public class TestCommon
    {
        protected static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [TestMethod]
        public void Md5Password()
        {

            var pass = "12345678";

            string hash = CommonHelper.GetMd5Hash(pass);

            Console.WriteLine(hash);

            // De
            var deHash = CommonHelper.VerifyMd5Hash(pass, hash);

            Assert.AreEqual(true, deHash);
        }

        [TestMethod]
        public void Test()
        {
            UserRecord[] u = new UserRecord[0];

            var lst = new List<UserRecord>()
            {
                new UserRecord()
                {
                    UserId = Guid.NewGuid(),
                    UserName = "BC"
                },
                new UserRecord()
                {
                    UserId = Guid.NewGuid(),
                    UserName = "A"
                },
                new UserRecord()
                {
                    UserId = Guid.NewGuid(),
                    UserName = "B"
                },
            };

            //u.Append(lst.ToArray());

            Console.Write(u);
        }


        [TestMethod]
        public void GetFile()
        {
            var txt = "/e_ContentFiles/1/2017/01/05/05.rar";

            var fileName = txt.Substring(txt.LastIndexOf('/') + 1);

            Console.WriteLine(fileName);

        }

        [TestMethod]
        public void Loop()
        {
            var versionGroup = new List<UserVersionRecord>()
            {
                new UserVersionRecord()
                {
                    CoverId = Guid.NewGuid(),
                    CoverVersion = 1
                },
                //new UserVersionRecord()
                //{
                //    CoverId = Guid.NewGuid(),
                //    CoverVersion = 2
                //},
                new UserVersionRecord()
                {
                    CoverId = Guid.NewGuid(),
                    CoverVersion = 3
                }
            };
            var vs = new List<VersionList>();

            var noMax = 1;

            vs = versionGroup.Where(x => x.CoverVersion > noMax).Select(x => new VersionList()
            {
                Url = x.PathUrl,
                Version = x.CoverVersion,
                Id = x.ParentId
            }).ToList();

            Console.WriteLine(vs.JsonSerialize());
        }

        [TestMethod]
        public void PartInt()
        {
            var oi = 0;
            var s = "";

            int.TryParse(s, out oi);

            Assert.AreNotEqual(oi, 1);
            Assert.AreNotEqual(oi, 0);
        }

        [TestMethod]
        public void Parse()
        {
            var str = "[{\"id\":\"ba79a9a2-5586-496b-8e3f-a254b5e16931\",\"version\":\"0\"}]";

            var par = JsonConvert.DeserializeObject<List<Test>>(str);

            Console.WriteLine(par.JsonSerialize());
        }


        [TestMethod]
        public void Random()
        {
            var s = CommonHelper.Random("BTS00001");

            Console.WriteLine(s);
        }

        [TestMethod]
        public void Date()
        {
            var dateNow = DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture);
            var dateConvert = ConvertDateTime(dateNow);

            var toDate = ConvertDateTime("20170114");

            if (dateConvert >= toDate)
            {
                Console.WriteLine("Error");
            }
            else
            {
                Console.WriteLine("OK");
            }


        }

        private static DateTime ConvertDateTime(string datetime)
        {
            return DateTime.ParseExact(datetime, "yyyyMMdd", CultureInfo.InvariantCulture);
        }


        [TestMethod]
        public void Join()
        {
            int[] arr = new[] { 1, 2, 3 };

            var j = ConvertStringArrayToString(arr);

            Console.WriteLine(j);
        }
        static string ConvertStringArrayToString(int[] array)
        {
            var builder = new StringBuilder();
            foreach (var value in array)
            {
                builder.AppendFormat("'{0}'", value);
                builder.Append(',');
            }
            return builder.ToString().TrimEnd(',');
        }

    }

    public class Test
    {
        public string Id { get; set; }

        public string Version { get; set; }
    }


}
