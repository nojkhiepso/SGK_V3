﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BTS.eBook.Admin.Controllers.Api.Model;
using BTS.eBook.Core.Contracts.Cover;
using BTS.eBook.Core.Helper;
using BTS.eBook.Data.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BTS_eBook.Tests
{
    [TestClass]
    public class TestWebApi
    {
        public IContainer Init()
        {
            return TestContainer.Build();
        }

        [TestMethod]
        public void Login()
        {
            var jsonSerializer = new JsonSerializer();
            var request = new LoginRequest()
            {
                Key = "1",
                PassWord = "25d55ad283aa400af464c76d713c07ad",
                UserName = "admin",
                UniqueNumber = "25d55ad283aa400af464c76d713c07ad",
                Version = "0"
            };

            var url = ConfigurationManager.AppSettings["ApiUrl"];

            var sw = new Stopwatch();
            sw.Start();
            var httpClient = CreateHttpClient();
            var response = httpClient.PostAsync(url + "api/login", new StringContent(request.JsonSerialize(), Encoding.UTF8, "application/json")).Result;
            response.EnsureSuccessStatusCode();

            var r = response.Content.ReadAsStreamAsync().Result;

            var l = new LoginResponse();

            using (var streamReader = new StreamReader(r))
            {
                using (var jsonReader = new JsonTextReader(streamReader))
                {
                    l = jsonSerializer.Deserialize<LoginResponse>(jsonReader);
                }
            }
            sw.Stop();
            Console.WriteLine(TestContainer.TimeSpan(sw));
            Console.WriteLine(l.JsonSerialize());
        }

        [TestMethod]
        public void GetVersionByUserV2()
        {
            using (var i = Init())
            {
                var sw = new Stopwatch();
                sw.Start();
                var uRepo = i.Resolve<IUserRepository>();

                var u = uRepo.GetVersionByUser(new Guid(), null, 0);
                sw.Stop();
                // Get the elapsed time as a TimeSpan value.

                Console.WriteLine(TestContainer.TimeSpan(sw));

                Assert.IsNotNull(u);
            }
        }

        [TestMethod]
        public void GetVersionByUserV3()
        {
            using (var i = Init())
            {
                var uRepo = i.Resolve<IUserRepository>();

                var u = uRepo.GetVersionByUser(new Guid(), new List<OrderRecord>(), 0);

                Assert.IsNotNull(u);
            }
        }


        private HttpClient CreateHttpClient()
        {
            var handler = new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            var httpClient = new HttpClient(handler)
            {
                Timeout = TimeSpan.FromMinutes(5)
            };
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //httpClient.DefaultRequestHeaders.Add("x-access-site", ConfigurationManager.AppSettings["AccessSite"]);
            //httpClient.DefaultRequestHeaders.Add("x-access-token", ConfigurationManager.AppSettings["AccessToken"]);

            return httpClient;
        }
    }
}
