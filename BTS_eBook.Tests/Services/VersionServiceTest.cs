﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Contracts.Version;
using BTS.eBook.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BTS_eBook.Tests.Services
{
    [TestClass]
    public class VersionServiceTest
    {
        public IContainer Init()
        {
            return TestContainer.Build();
        }

        [TestMethod]
        public void GetVerNo()
        {
            using (var s=Init())
            {
                var versionService = s.Resolve<IVersionService>();
                // 0e49c58d-8a97-4d78-bd37-ba4433004472
                // 6bbefd12-1248-4993-ae50-aa53cccaf243
                var v1 = versionService.GetVersionNo(Guid.Parse("7538ec5a-862e-41fd-ab3b-8c3c5f6ba650"));

                //var v3 = versionService.GetVersionNo(Guid.Parse("0e49c58d-8a97-4d78-bd37-ba4433004474"));
                //var v2 = versionService.GetVersionNo(Guid.Parse("6bbefd12-1248-4993-ae50-aa53cccaf243"));

                Console.WriteLine(v1);
                //Console.WriteLine(v2);
                //Console.WriteLine(v3);

                Assert.IsNotNull(v1);
                //Assert.AreEqual(v3, 0);
            }
        }

        [TestMethod]
        public void GetRecordById()
        {
            using (var s=Init())
            {
                var services = s.Resolve<IVersionService>();
                var v = services.GetRecordById(Guid.Parse("7538ec5a-862e-41fd-ab3b-8c3c5f6ba650"));

                Assert.IsNotNull(v);
            }
        }
        [TestMethod]
        public void GetById()
        {
            using (var s = Init())
            {
                var services = s.Resolve<IVersionService>();
                var v = services.GetById(Guid.Parse("7538ec5a-862e-41fd-ab3b-8c3c5f6ba650"));

                Assert.IsNotNull(v);
            }
        }

        [TestMethod]
        public void SearchVersion()
        {
            using (var s = Init())
            {
                var services = s.Resolve<IVersionService>();
                var v = services.SearchPagging(new SearchVersionRequest()
                {
                    Paging = new PagingRequest()
                    {
                        PageSize = 25,
                        PageIndex = 1
                    }
                });
                Console.WriteLine(JsonConvert.SerializeObject(v.Records));
                Assert.IsNotNull(v);
            }
        }
    }
}
