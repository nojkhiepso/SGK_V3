﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.Data.Repositories;
using BTS.eBook.Services;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BTS_eBook.Tests.Services
{
    [TestClass]
    public class UserServiceTest
    {
        public IContainer Init()
        {
            return TestContainer.Build();
        }

        [TestMethod]
        public void SearchUser()
        {
            using (var cn = Init())
            {
                var userService = cn.Resolve<IUserService>();
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                var users = userService.SearchUser(new SearchUserRequest()
                {
                    Paging = new PagingRequest()
                    {
                        PageIndex = 1,
                        PageSize = 25
                    }
                });
                stopWatch.Stop();

                Console.WriteLine(stopWatch.ElapsedMilliseconds);
                Console.WriteLine(JsonConvert.SerializeObject(users));
                Assert.IsNotNull(users);
            }
        }

        [TestMethod]
        public void GetRecordById()
        {
            using (var cn = Init())
            {
                var userService = cn.Resolve<IUserService>();
                var stopWatch = new Stopwatch();
                stopWatch.Start();

                var users = userService.GetRecordById(Guid.Parse("01b7f6a2-d280-4349-accc-74111047cdaa"));
                stopWatch.Stop();

                //Logger.Info(JsonConvert.SerializeObject(users));

                Console.WriteLine(stopWatch.ElapsedMilliseconds);
                Console.WriteLine(JsonConvert.SerializeObject(users));
                Assert.IsNotNull(users);
            }
        }

        [TestMethod]
        public void GetCountUser()
        {

            using (var cn=Init())
            {
                var userRepository = cn.Resolve<IUserRepository>();
                var c = userRepository.GetCountUser("");

                Console.WriteLine(c);


            }

        }
    }
}
