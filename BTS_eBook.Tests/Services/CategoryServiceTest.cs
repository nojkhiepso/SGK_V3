﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BTS.eBook.Core.Helper;
using BTS.eBook.Data.Repositories;
using BTS.eBook.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS_eBook.Tests.Services
{
    [TestClass]
    public class CategoryServiceTest
    {
        public IContainer Init()
        {
            return TestContainer.Build();
        }

        [TestMethod]
        public void GetCategory()
        {
            using (var cn = Init())
            {
                var categoryRepository = cn.Resolve<ICategoryRepository>();

                var categorys = categoryRepository.GetAll().Where(x => x.CategoryId == new Guid("4BCB7FFB-4C15-496B-AEE2-1E4790BC1C21"));

                Console.WriteLine(categorys.JsonSerialize());

                Assert.IsNotNull(categorys);
            }
        }
    }
}
