﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BTS.eBook.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BTS_eBook.Tests.Services
{
    [TestClass]
    public class CoreUploadServiceTest
    {
        [TestMethod]
        public void DeleteMany()
        {
            using (var s = Init())
            {
                var cs = s.Resolve<ICoreUploadService>();
                Guid[] id = {
                    new Guid("97d3e247-a40e-4115-a862-727d7677bd0c"),
                    new Guid("70f49099-904d-4301-b779-78906e7b4bad"),
                };
                var u = cs.DeleteById(id, "79f53a6fff3eee427ce8c5b5b41c5f01");

                Assert.AreEqual(u, true);
            }
        }

        [TestMethod]
        public void GetFile()
        {
            using (var cn=Init())
            {
                var service = cn.Resolve<ICoreUploadService>();
                var u = service.GetFileBy("7951c3e6e5e53a0f88e26358be60afff");

                Console.Write(JsonConvert.SerializeObject(u));

                Assert.IsNotNull(u);
            }
        } 

        private IContainer Init()
        {
            return TestContainer.Build();
        }
    }
}
