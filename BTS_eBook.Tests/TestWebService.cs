﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using BTS.eBook.Admin.Controllers.Api.Model;
using BTS.eBook.Core.Helper;
using BTS_eBook.Tests.ServiceReference;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BTS_eBook.Tests
{
    [TestClass]
    public class TestWebService
    {
        [TestMethod]
        public string Login()
        {
            var sw = new Stopwatch();
            sw.Start();
            var url = "http://ebook.hlus.com.vn";
            BTSeBookSoapClient c = new BTSeBookSoapClient("BTS-eBookSoap", new EndpointAddress(url + "/eBookServices/eBookServices.asmx"));
            var str = c.Login("1", "dapdv", "25d55ad283aa400af464c76d713c07ad", "1", "0");
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            Console.WriteLine(str);

            Assert.IsNotNull(str);

            return JsonConvert.DeserializeObject<LoginResponse>(str).TokenKey;
        }

        [TestMethod]
        public void SgkV2Login()
        {
            var sw = new Stopwatch();
            sw.Start();
            var url = "http://localhost:51146/";
            BTSeBookSoapClient c = new BTSeBookSoapClient("BTS-eBookSoap", new EndpointAddress(url + "/eBookServices/eBookServices.asmx"));
            var str = c.Login("1", "sgkv2_1", "25d55ad283aa400af464c76d713c07ad", "1", "0");
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            Console.WriteLine(str);

            Assert.IsNotNull(str);

            //return JsonConvert.DeserializeObject<LoginResponse>(str).TokenKey;
        }

        [TestMethod]
        public void SgkV2Login_Version()
        {
            var sw = new Stopwatch();
            sw.Start();
            var url = "http://localhost:51146/";
            BTSeBookSoapClient c = new BTSeBookSoapClient("BTS-eBookSoap", new EndpointAddress(url + "/eBookServices/eBookServices.asmx"));
            var str = c.Login("1", "sgkv2_1", "25d55ad283aa400af464c76d713c07ad", "1", "1");
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            Console.WriteLine(str);

            Assert.IsNotNull(str);

            //return JsonConvert.DeserializeObject<LoginResponse>(str).TokenKey;
        }

        [TestMethod]
        public void SgkV3()
        {
            var sw = new Stopwatch();
            sw.Start();
            var url = "http://localhost:51146/";
            BTSeBookSoapClient c = new BTSeBookSoapClient("BTS-eBookSoap", new EndpointAddress(url + "/eBookServices/eBookServices.asmx"));
            var str = c.Login("1", "sgkv2_1", "25d55ad283aa400af464c76d713c07ad", "1", "1");
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            Console.WriteLine(str);

            Assert.IsNotNull(str);

            //return JsonConvert.DeserializeObject<LoginResponse>(str).TokenKey;
        }

        public class LoginResponse
        {
            [DisplayName("tokenKey")]
            public string TokenKey { get; set; }
        }

        [TestMethod]
        public void Download()
        {
            var token = Login();

            var sw = new Stopwatch();
            using (var client = new WebClient())
            {
                client.Headers.Add("post", "0");
                client.Headers.Add("url", "/e_ContentFiles/1/2017/01/05/05.rar");
                client.Headers.Add("tokenKey", token);

                sw = new Stopwatch();
                sw.Start();
                client.DownloadFile("http://ebook.hlus.com.vn/eBookServices/eBookServices.asmx/Download", "Test1.zip");
                sw.Stop();

                Console.WriteLine(string.Format("{0:0,.00}s", sw.ElapsedMilliseconds));
            }
            //var file = ByteArrayToFile("05.rar", byteFile);


            //Assert.IsTrue(file);
        }

    }
}
