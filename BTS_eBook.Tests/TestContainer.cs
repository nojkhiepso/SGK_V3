﻿using Autofac;
using BTS.eBook.Data;
using BTS.eBook.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using Autofac.Integration.Mvc;
using System.Web.Mvc;
using Autofac.Integration.WebApi;
using BTS.eBook.CoreData;
using BTS.eBook.CoreData.Repositories;

namespace BTS_eBook.Tests
{
    public class TestContainer
    {
        public static Autofac.IContainer Build()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType(typeof(BTS_ebookEntities)).InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(Assembly.Load("BTS.eBook.Data"))
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(Assembly.Load("BTS.eBook.Services"))
               .Where(t => t.Name.EndsWith("Service"))
               .AsImplementedInterfaces()
               .InstancePerLifetimeScope();

            // Register your MVC controllers.
            var callingAssembly = Assembly.GetExecutingAssembly();
            builder.RegisterControllers(callingAssembly);


            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // Set the dependency resolver to be Autofac.
            var resolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(resolver);


            return container;
        }

        public static string TimeSpan(Stopwatch sw)
        {
            TimeSpan ts = sw.Elapsed;

            // Format and display the TimeSpan value.
            return string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
        }

    }
}
