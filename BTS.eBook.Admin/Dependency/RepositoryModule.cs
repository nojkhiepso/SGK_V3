﻿using System.Reflection; 
using Autofac;
using BTS.eBook.CoreData.Repositories;
using BTS.eBook.Data.Repositories;
using Module = Autofac.Module;

namespace BTS.eBook.Admin.Dependency
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof (Repository<>))
                .As(typeof (IRepository<>))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(Assembly.Load("BTS.eBook.Data"))
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}