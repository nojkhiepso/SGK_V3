﻿using Autofac;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using Module=Autofac.Module;

namespace BTS.eBook.Admin.Dependency
{
    public class EfModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof (BTS_ebookEntities)).InstancePerLifetimeScope();
        }
    }
}