﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;
using BTS.eBook.Admin.Authentication;
using BTS.eBook.Admin.Hubs;
using BTS.eBook.Admin.Models;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Module;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.Core.Helper;
using BTS.eBook.Core.Mvc;
using BTS.eBook.Services;
using log4net.Repository.Hierarchy;
using MvcPaging;
using Newtonsoft.Json;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.CoreData;
using BTS.eBook.Data;
using Microsoft.AspNet.SignalR;
using NPOI.HSSF.UserModel;

namespace BTS.eBook.Admin.Controllers
{
    [RoutePrefix("users")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly IModuleService _moduleService;
        private readonly IGroupService _groupService;
        private readonly IGroupUserService _groupUserService;

        public UserController(IUserService userService, IRoleService roleService, IModuleService moduleService, IGroupService groupService, IGroupUserService groupUserService)
        {
            this._userService = userService;
            _roleService = roleService;
            _moduleService = moduleService;
            _groupService = groupService;
            _groupUserService = groupUserService;
        }

        [Route("index", Name = "u-index")]
        [SessionExpire]
        public ActionResult Index(string userName, string email, int? page)
        {
            var currentPage = page ?? 1;
            var request = new SearchUserRequest()
            {
                Header = new RequestHeader
                {
                    CallerName = "Search User"
                },
                UserName = userName,
                Email = email,
                Paging = new PagingRequest
                {
                    PageSize = Constant.DefaultPageSize,
                    PageIndex = currentPage
                }
            };

            var result = _userService.SearchUser(request);
            if (result.Status == null) return View();

            var users = result.Records.ToPagedList(1, Constant.DefaultPageSize, result.Paging.TotalRecords);

            ViewBag.UserName = userName;
            ViewBag.Email = email;
            ViewBag.PageNumber = currentPage;

            if (Session["import_user"] != null)
            {
                ViewBag.Import = 1;
            }

            return View(users);
        }

        [Route("create-new", Name = "u-create")]
        [SessionExpire]
        public ActionResult Create()
        {
            var roles = _roleService.GetAllRoles();

            return View(new UserModel()
            {
                IsActive = true,
                RoleModel = roles?.ToList()
            });
        }

        [Route("create-new", Name = "u-add")]
        [HttpPost]
        public ActionResult Create(UserModel model, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                var isExistEmail = _userService.ExitsEmail(model.Email);
                if (!isExistEmail)
                {
                    var c = _userService.GetCountUser(model.UserName);

                    var request = new UserRequest
                    {
                        Header = new RequestHeader
                        {
                            CallerName = "Add new",
                            Action = 1,
                            UserId = GetCurrentUser().UserId
                        },
                        Record = new UserRecord
                        {
                            UserId = Guid.NewGuid(),
                            UserName = model.UserName,
                            FullName = model.FullName,
                            Phone = model.Phone,
                            Email = model.Email,
                            PasswordHash = CommonHelper.GetMd5Hash(model.PasswordHash),
                            IsLocked = false,
                            CreateTime = DateTime.Now,
                            IsActive = true,
                            Roles = model.Roles,
                            UserType = c + 1
                        }
                    };
                    var isCreate = _userService.CreateOrUpdate(request);
                    if (isCreate)
                        AddMessage("Thêm người dùng thành công.");
                    else
                        AddError("Người dùng đã tồn tại trong hệ thống. Vui lòng nhập tài khoản khác.");

                    int ctn;
                    int.TryParse(fc["hdIsSaveContinue"], out ctn);

                    return RedirectToRoute(ctn == 0 ? "u-index" : "u-create");
                }
                ViewBag.Error = "Tồn tại Email. Vui lòng nhập email khác.";
            }
            else
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            model.RoleModel = _roleService.GetAllRoles()?.ToList();

            return View(model);
        }

        [Route("edit", Name = "u-edit")]
        [SessionExpire]
        public ActionResult Edit(Guid id, string userName, string email, int? page)
        {
            var user = _userService.GetRecordById(id);
            if (user == null) return RedirectToRoute("u-index");
            var userModel = ToUserModel(user);
            userModel.RoleModel = _roleService.GetAllRoles()?.ToList();
            userModel.Roles = _roleService.GetUserRole(id);

            return View(userModel);
        }

        [Route("edit", Name = "u-updated")]
        [HttpPost]
        public ActionResult Edit(UserModel model)
        {
            if (!ModelState.IsValid)
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            else
            {
                //var email = _userService.ExitsEmail(model.Email);
                //if (email)
                //{
                //    AddError("Email đã tồn tại. Vui lòng nhập Email khác");

                //    // error
                //    var u = _userService.GetRecordById(model.UserId);
                //    var um = ToUserModel(u);
                //    um.RoleModel = _roleService.GetAllRoles()?.ToList();
                //    um.Roles = _roleService.GetUserRole();

                //    return View(um);
                //}

                var request = new UserRequest()
                {
                    Header = new RequestHeader
                    {
                        Action = 2,
                        CallerName = "Update",
                        UserId = GetCurrentUser().UserId
                    },
                    Record = new UserRecord()
                    {
                        UserId = model.UserId,
                        UserName = model.UserName,
                        FullName = model.FullName,
                        Phone = model.Phone,
                        IsLocked = false,
                        IsActive = model.IsActive,
                        Roles = model.Roles,
                        Email = model.Email
                    }
                };
                var response = _userService.CreateOrUpdate(request);
                if (!response)
                {
                    AddError("Cập nhật không thành công.");
                }
                else
                {
                    AddMessage("Cập nhật thành công.");
                }
                return RedirectToRoute("u-index");
            }

            // error
            var user = _userService.GetRecordById(model.UserId);
            var userModel = ToUserModel(user);
            userModel.RoleModel = _roleService.GetAllRoles()?.ToList();
            userModel.Roles = _roleService.GetUserRole();

            return View(userModel);
        }

        [Route("delete", Name = "u-delete")]
        public ActionResult Delete(Guid id, string userName, string email, int? page)
        {
            try
            {
                var response = _userService.DeleteBy(id, GetCurrentUser().UserId);
                if (!response)
                {
                    AddError("Xóa không thành công.");
                    return RedirectToRoute("u-index");
                }

                AddMessage("Xóa thành công.");
                return RedirectToRoute("u-index", new { page = page ?? 1, userName, email });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return RedirectToRoute("u-index");
            }
        }

        [Route("change-pass/{id}", Name = "u-change-pass")]
        [SessionExpire]
        public ActionResult ChangePassword(Guid id)
        {
            var user = _userService.GetRecordById(id);
            if (user == null)
            {
                AddError("Không tồn tại người dùng.");
                return RedirectToRoute("u-index");
            }
            var userModel = ToUserModel(user);
            return View(userModel);
        }

        [Route("change-pass", Name = "u-save-change-pass")]
        [HttpPost]
        public ActionResult ChangePassword(UserModel model)
        {
            if (!ModelState.IsValid)
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            else
            {
                var request = new UserRequest()
                {
                    Header = new RequestHeader
                    {
                        CallerName = "Change pass",
                        UserId = GetCurrentUser().UserId
                    },
                    Record = new UserRecord()
                    {
                        UserId = model.UserId,
                        PasswordHash = CommonHelper.GetMd5Hash(model.PasswordHash)
                    }
                };
                var response = _userService.ChangePassword(request);
                if (!response)
                {
                    AddError("Thay đổi mật khẩu thành công.");
                }
                else
                {
                    AddMessage("Thay đổi mật khẩu thành công.");
                }
                return RedirectToRoute("u-index");
            }

            // error
            var user = _userService.GetRecordById(model.UserId);
            var userModel = ToUserModel(user);
            return View(userModel);
        }

        [Route("user-function", Name = "u-function")]
        public ActionResult UserFunction(Guid id)
        {
            var user = _userService.GetRecordById(id);

            var ufunction = new UserFunction()
            {
                FullName = user.FullName,
                UserId = user.UserId
            };
            return View(ufunction);
        }

        private static UserModel ToUserModel(UserRecord user)
        {
            var userModel = new UserModel()
            {
                UserName = user.UserName,
                UserId = user.UserId,
                PasswordHash = "******",
                FullName = user.FullName,
                Phone = user.Phone,
                Email = user.Email,
                IsActive = user.IsActive,
                IsLocked = user.IsLocked
            };
            return userModel;
        }


        [Route("guest-change-pass/{id}", Name = "g-change-pass")]
        [SessionExpire]
        public ActionResult GuestChangePassword(Guid id)
        {
            var user = _userService.GetRecordById(id);
            if (user == null)
            {
                AddError("Không tồn tại người dùng.");
                return RedirectToRoute("u-index");
            }
            var userModel = ToUserModel(user);
            return View(userModel);
        }

        [Route("guest-change-pass", Name = "g-save-change-pass")]
        [HttpPost]
        public ActionResult GuestChangePassword(UserModel model)
        {
            if (!ModelState.IsValid)
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            else
            {
                var request = new UserRequest()
                {
                    Header = new RequestHeader
                    {
                        CallerName = "Change pass",
                        UserId = GetCurrentUser().UserId
                    },
                    Record = new UserRecord()
                    {
                        UserId = model.UserId,
                        PasswordHash = CommonHelper.GetMd5Hash(model.PasswordHash)
                    }
                };
                var response = _userService.ChangePassword(request);
                if (!response)
                {
                    AddError("Thay đổi mật khẩu thành công.");
                }
                else
                {
                    AddMessage("Thay đổi mật khẩu thành công.");
                }
                return RedirectToRoute("index");
            }

            // error
            var user = _userService.GetRecordById(model.UserId);
            var userModel = ToUserModel(user);

            return View(userModel);
        }


        [Route("import-user", Name = "u-import-user")]
        public ActionResult ImportUser()
        {
            var model = new ImportModel()
            {
                GroupRecord = _groupService.GetAllGroupBy(new[] { 1, 2, 3 })?.ToList()// 1: admin, 2: aprroval, 3: normal
            };
            return View(model);
        }

        [Route("save-import-user", Name = "u-save-import-user")]
        public ActionResult ImportUser(ImportModel model, HttpPostedFileBase file)
        {
            if (model.Groups == null || !model.Groups.Any())
            {
                AddError("Chọn Nhóm.");
            }
            else if (file == null)
            {
                AddError("Chọn file.");
            }
            else if (!file.FileName.EndsWith("xls"))
            {
                AddError("Định dạng file .xls.");
            }
            else
            {
                try
                {
                    var excelHelper = new ExcelHelper();
                    var properties = excelHelper.GetProperties(typeof(UserImport), new[] { "STT", "FullName", "Email", "Tel" });
                    if (file.ContentLength > 0)
                    {
                        var ui = excelHelper.ReadData<UserImport>(file.InputStream, file.FileName, properties);

                        var users = new List<CoreData.User>();
                        var userTmp = new List<CoreData.User>();

                        var i = 0;
                        var c = _userService.GetCountUser(model.Title);

                        var k = 1;

                        if (c > 0)
                        {
                            k = c + 1;
                        }

                        Guid[] uId = { };
                        if (ui != null && ui.Any())
                        {
                            uId = new Guid[ui.Count];
                            foreach (var userImport in ui)
                            {
                                var u = Guid.NewGuid();

                                uId[i] = u;

                                i++; // count u
                                var pass = Guid.NewGuid().ToString().Replace("-", "");
                                if (model.IsDefault)
                                {
                                    pass = ConfigurationManager.AppSettings["PassDefault"];
                                }
                                users.Add(new CoreData.User()
                                {
                                    UserId = u,
                                    IsActive = true,
                                    IsLocked = false,
                                    SortOrder = k, // sort order
                                    UserName = model.Title + CommonHelper.GenerateCode(k),
                                    FullName = userImport.FullName,
                                    CreateTime = DateTime.Now,
                                    Email = userImport.Email,
                                    Phone = userImport.Tel,
                                    PasswordHash = CommonHelper.GetMd5Hash(pass)
                                });

                                userTmp.Add(new CoreData.User()
                                {
                                    UserId = u,
                                    IsActive = true,
                                    IsLocked = false,
                                    SortOrder = k, // sort order
                                    UserName = model.Title + CommonHelper.GenerateCode(k),
                                    FullName = userImport.FullName,
                                    CreateTime = DateTime.Now,
                                    Email = userImport.Email,
                                    Phone = userImport.Tel,
                                    PasswordHash = CommonHelper.GetMd5Hash(pass),
                                    AvatarUrl = pass, // orgipassword
                                });

                                k++; // count user
                            }
                        }

                        if (_userService.AddMany(users))
                        {
                            CacheManager.Instance.RemoveCacheIfKeyContains("User_");
                            if (model.Groups != null && model.Groups.Any())
                            {
                                //_groupUserService.AddMany()
                                foreach (var g in model.Groups)
                                {
                                    var groupUsers = uId.Select(t => new GroupUser()
                                    {
                                        GroupId = g,
                                        UserId = t,
                                        GroupUserId = Guid.NewGuid()
                                    }).ToList();

                                    _groupUserService.AddMany(groupUsers, GetCurrentUser().UserId);
                                }
                                Session["import_user"] = userTmp;
                            }

                            AddMessage("Import người dùng thành công.");

                            return RedirectToRoute("u-index");
                        }

                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    AddError("Lỗi import người dùng.");

                    return RedirectToRoute("u-index");
                }
            }

            var md = new ImportModel()
            {
                GroupRecord = _groupService.GetAllGroupBy(new[] { 1, 2, 3 })?.ToList()
            };
            return View(md);
        }

        [Route("download-file", Name = "download-file")]
        public ActionResult DownloadFile()
        {
            // export
            var us = Session["import_user"] as List<CoreData.User>;
            if (us != null && us.Any())
            {
                // export user
                var fs = new FileStream(Server.MapPath("/Template/ExportTemplate.xls"), FileMode.Open, FileAccess.Read);
                var workbook = new HSSFWorkbook(fs, true);

                var sheet = workbook.GetSheetAt(0);
                if (sheet != null)
                {
                    var j = 0;
                    foreach (var user in us)
                    {
                        j++;
                        var r1 = sheet.CreateRow(j);
                        var cell0 = r1.CreateCell(0);
                        cell0.SetCellValue(j.ToString());

                        var cell1 = r1.CreateCell(1);
                        cell1.SetCellValue(user.UserName);

                        var cell2 = r1.CreateCell(2);
                        cell2.SetCellValue(user.FullName);

                        var cell3 = r1.CreateCell(3);
                        cell3.SetCellValue(user.Email);

                        var cell4 = r1.CreateCell(4);
                        cell4.SetCellValue(user.AvatarUrl);// temp pass

                        var cell5 = r1.CreateCell(5);
                        cell5.SetCellValue(user.Phone);
                    }
                }
                Session["import_user"] = null;
                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);
                    string saveAsFileName = string.Format("export-{0:yyyyMMddhhss}.xls", DateTime.Now).Replace("/", "-");

                    byte[] bytes = exportData.ToArray();
                    return File(bytes, "application/vnd.ms-excel", saveAsFileName);
                }
            }
            return RedirectToRoute("u-index");
        }

        [Route("export-user", Name = "u-export-user")]
        public ActionResult ExportUser(string name)
        {
            // export user
            var fs = new FileStream(Server.MapPath("/Template/UserTemplate.xls"), FileMode.Open, FileAccess.Read);
            var workbook = new HSSFWorkbook(fs, true);

            var sheet = workbook.GetSheetAt(0);

            var users = _userService.GetAllUserBy(name);

            if (sheet != null && users != null && users.Any())
            {
                var j = 0;
                foreach (var user in users)
                {
                    j++;
                    var r1 = sheet.CreateRow(j);
                    var cell0 = r1.CreateCell(0);
                    cell0.SetCellValue(j.ToString());

                    var cell1 = r1.CreateCell(1);
                    cell1.SetCellValue(user.UserName);

                    var cell2 = r1.CreateCell(2);
                    cell2.SetCellValue(user.FullName);

                    var cell3 = r1.CreateCell(3);
                    cell3.SetCellValue(user.Email);

                    var cell4 = r1.CreateCell(4);
                    cell4.SetCellValue(user.Phone);
                }
            }

            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                string saveAsFileName = string.Format("export-{0:yyyyMMddhhss}.xls", DateTime.Now).Replace("/", "-");

                byte[] bytes = exportData.ToArray();
                return File(bytes, "application/vnd.ms-excel", saveAsFileName);
            }
        }


        #region Login

        [Route("login", Name = "u-login")]
        public ActionResult Login()
        {
            return View();
        }

        [Route("login", Name = "u-post-login")]
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var userName = model.UserName.Trim();
                var password = model.Password;
                var rememberMe = model.RememberMe;

                if (userName != Host)
                {
                    if (Membership.ValidateUser(userName, password) && userName != Host)
                    {
                        var userRecord = new UserRecord();

                        var cacheItem = CacheManager.Instance.GetCache<UserRecord>("_Login_" + userName);
                        if (cacheItem != null)
                        {
                            userRecord = cacheItem;
                        }

                        if (userRecord.UserId == Guid.Empty)
                        {
                            ViewBag.Error = "Lỗi đăng nhập. Vui lòng liên hệ với quản trị";
                            return View(model);
                        }

                        var check = true;

                        if (userRecord.IsLocked)
                        {
                            check = false;
                            ViewBag.Error = "Tài khoản đã bị khóa.";
                        }

                        if (check)
                        {
                            SetLoginUser(SetSessionLoginCurrent(userRecord));

                            var isUpdate = _userService.UpdateUserById(userRecord.UserId);
                            if (isUpdate)
                            {
                                var cookie = Response.Cookies["_culture"];
                                if (cookie != null)
                                {
                                    cookie.Value = CultureHelper.GetDefaultCulture(); // update cookie value
                                    cookie.Expires = DateTime.UtcNow.AddDays(1);
                                }
                                else
                                {
                                    cookie = new HttpCookie("_culture")
                                    {
                                        Value = CultureHelper.GetDefaultCulture(),
                                        Expires = DateTime.UtcNow.AddDays(1)
                                    };
                                }
                                Response.Cookies.Add(cookie);
                                //
                                FormsAuthentication.RedirectFromLoginPage(userName, rememberMe);

                                //return RedirectToRoute("ad-Index");
                                return RedirectToRoute("Index");
                            }
                            ViewBag.Error = "Lỗi đăng nhập. Vui lòng liên hệ với quản trị";
                        }
                    }
                    ViewBag.Error = "Người dùng hoặc Mật khẩu không chính xác";
                    return View(model);
                }
                else if (Host == "host")
                {
                    Session[Constant.SESSION_LOGIN_USER] = new EbookMembershipUserCurrent()
                    {
                        UserName = "host",
                        UserId = Guid.NewGuid(),
                        FullName = "Host"
                    };
                    return RedirectToRoute("Index");
                }

                ViewBag.Error = "Người dùng hoặc Mật khẩu không chính xác";
            }
            return View(model);
        }

        [Route("logout", Name = "u-logout")]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session[Constant.SESSION_LOGIN_USER] = null;
            CacheManager.Instance.RemoveAll();

            return RedirectToRoute("Index");
        }

        protected void SetLoginUser(EbookMembershipUserCurrent user)
        {
            Session[Constant.SESSION_LOGIN_USER] = user;
        }

        private EbookMembershipUserCurrent SetSessionLoginCurrent(UserRecord userResult)
        {
            EbookMembershipUserCurrent loginUserCurrent = null;
            if (userResult != null)
            {
                // get role
                var roles = _roleService.GetUserRole(userResult.UserId);
                var listModule = new List<ModuleViewModel>();
                if (roles != null && roles.Length > 0)
                {
                    foreach (var item in roles)
                    {
                        var rm = _roleService.GetRoleModules(item);
                        foreach (var r in rm)
                        {
                            if (r.Add || r.Edit || r.Delete || r.View || r.Publish)
                            {
                                listModule.Add(new ModuleViewModel()
                                {
                                    ModuleId = r.ModuleId,
                                    Name = r.Name,
                                    Code = r.Code,
                                    RoleModuleModel = new RoleModuleModel()
                                    {
                                        Add = r.Add,
                                        Edit = r.Edit,
                                        Delete = r.Delete,
                                        Publish = r.Publish,
                                        View = r.View,
                                        RoleId = r.RoleId,
                                        ModuleId = r.ModuleId,
                                        RoleModuleId = r.RoleModuleId
                                    }
                                });
                            }
                        }
                    }
                }

                loginUserCurrent = new EbookMembershipUserCurrent()
                {
                    UserId = userResult.UserId,
                    UserType = userResult.UserType,
                    Avatar = userResult.AvatarUrl,
                    FullName = userResult.FullName,
                    UserName = userResult.UserName,
                    ListRoles = userResult.Roles,
                    ListModules = listModule,
                };
            }

            return loginUserCurrent;
        }
        #endregion

        #region Module

        [Route("add-module", Name = "m-create")]
        public ActionResult AddModule()
        {
            return View(new ModuleModel()
            {
                IsActive = true
            });
        }

        [Route("add-module", Name = "m-add")]
        [HttpPost]
        public ActionResult AddModule(ModuleModel model, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                var request = new ModuleRequest()
                {
                    Header = new RequestHeader
                    {
                        CallerName = "Add new",
                        Action = 1,
                        UserId = GetCurrentUser().UserId
                    },
                    Record = new ModuleRecord()
                    {
                        ModuleId = Guid.NewGuid(),
                        Name = model.Name,
                        Code = model.Code,
                        IsActive = true
                    }
                };
                var isCreate = _moduleService.CreateOrUpdate(request);
                if (isCreate)
                    AddMessage("Thêm chức năng thành công.");
                else
                    AddError("Thêm chức năng không thành công.");

                int ctn;
                int.TryParse(fc["hdIsSaveContinue"], out ctn);

                return RedirectToRoute(ctn == 0 ? "u-role-index" : "m-add");
            }
            else
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }

            return View(model);
        }
        #endregion

        #region Roles


        [Route("roles", Name = "u-role-index")]
        public ActionResult Roles()
        {
            var roles = _roleService.GetAllRoles();
            return View(roles);
        }

        [Route("add-roles", Name = "u-role-create")]
        public ActionResult RoleCreate()
        {
            var groupModule = new GroupViewModel()
            {
                ListModule = _moduleService.GetAllModule()?.ToList()
            };
            return View(groupModule);
        }

        [Route("save-roles", Name = "u-role-add")]
        [HttpPost]
        public ActionResult AddRole(GroupViewModel model, FormCollection form)
        {
            var listModule = form["listModule"];
            var modules = JsonConvert.DeserializeObject<List<RoleModuleModel>>(listModule);
            var isNameExist = _roleService.CheckRoleExist(model.RoleViewModel.Name);
            if (!isNameExist)
            {
                var models = new GroupViewModelRequest()
                {
                    Header = new RequestHeader()
                    {
                        Action = 1,
                        UserId = GetCurrentUser().UserId
                    },
                    Record = new GroupViewModelRecord
                    {
                        RoleRecord = new RoleRecord()
                        {
                            Name = model.RoleViewModel.Name,
                            Type = 2,
                            IsActive = true
                        }
                    }
                };

                var roleModules = modules.Select(x => new RoleModuleRecord()
                {
                    Add = x.Add,
                    View = x.View,
                    Edit = x.Edit,
                    Delete = x.Delete,
                    Publish = x.Publish,
                    ModuleId = x.ModuleId,
                    RoleId = x.RoleId
                }).ToList();

                var result = _roleService.UpdateRole(models, roleModules);

                if (result)
                {
                    AddMessage("Thêm mới nhóm chức năng thành công.");
                }
                
                return RedirectToRoute("u-role-index");
            }

            var groupModule = new GroupViewModel()
            {
                ListModule = _moduleService.GetAllModule()?.ToList()
            };
            AddError("Nhóm đã tồn tại");
            return View("RoleCreate", groupModule);
        }

        [Route("edit-role", Name = "u-role-edit")]
        public ActionResult RoleEdit(Guid id)
        {
            var roles = _roleService.GetRoleRecordById(id);
            var model = new GroupViewModel();

            model.RoleViewModel = ToRoleModel(roles);
            var rolesModule = _roleService.GetRoleModules(id)?.ToList();
            var modules = _moduleService.GetAllModule()?.ToList();

            var lstModule = new List<ModuleRecord>();
            if (rolesModule != null && rolesModule.Any())
            {
                foreach (var moduleRecord in modules)
                {
                    var roleModule = rolesModule.FirstOrDefault(x => x.ModuleId == moduleRecord.ModuleId);
                    moduleRecord.RoleModuleRecord = roleModule;

                    lstModule.Add(moduleRecord);
                }
            }
            else
            {
                lstModule = modules;
            }
            model.ListModule = lstModule;

            return View(model);
        }

        [Route("save-edit-role", Name = "u-role-update")]
        [HttpPost]
        public ActionResult UpdateRole(GroupViewModel model, FormCollection form)
        {
            var listModule = form["listModule"];
            var modules = JsonConvert.DeserializeObject<List<RoleModuleModel>>(listModule);
            var isNameExist = _roleService.CheckRoleExist(model.RoleViewModel.Name);
            var models = new GroupViewModelRequest()
            {
                Header = new RequestHeader()
                {
                    Action = 2,
                    UserId = GetCurrentUser().UserId
                },
                Record = new GroupViewModelRecord()
                {
                    RoleRecord = new RoleRecord()
                    {
                        Name = model.RoleViewModel.Name,
                        RoleId = model.RoleViewModel.RoleId,
                        Type = 2,
                        IsActive = true
                    }
                }
            };

            var roleModules = modules.Select(x => new RoleModuleRecord()
            {
                Add = x.Add,
                View = x.View,
                Edit = x.Edit,
                Delete = x.Delete,
                ModuleId = x.ModuleId,
                Publish = x.Publish,
                RoleId = x.RoleId
            }).ToList();

            var result = _roleService.UpdateRole(models, roleModules);
            if (result && Host != "host")
            {
                var sessionLogin = Session[Constant.SESSION_LOGIN_USER] as EbookMembershipUserCurrent;
                if (sessionLogin != null)
                {
                    var checkModule = sessionLogin.ListModules.Where(lm => (lm.ModuleId == modules.First().ModuleId));
                    if (checkModule.Any())
                    {
                        var list = new List<ModuleViewModel>();
                        if (sessionLogin.ListRoles != null)
                        {
                            foreach (var item in sessionLogin.ListRoles)
                            {
                                var moduleRecords = _moduleService.GetModuleBy(item);
                                var temp = moduleRecords.Select(x => new ModuleViewModel()
                                {
                                    ModuleId = x.ModuleId,
                                    Name = x.Name,
                                    Code = x.Code,
                                    IsActive = x.IsActive,
                                    RoleModuleModel = new RoleModuleModel()
                                    {
                                        Add = x.RoleModuleRecord.Add,
                                        Edit = x.RoleModuleRecord.Edit,
                                        View = x.RoleModuleRecord.View,
                                        Publish = x.RoleModuleRecord.Publish,
                                        Delete = x.RoleModuleRecord.Delete
                                    }
                                }).ToList();
                                foreach (var lst in temp)
                                {
                                    list.Add(lst);
                                }
                            }
                        }
                        sessionLogin.ListModules = list;
                    }
                }
                AddMessage("Cập nhật nhóm chức năng thành công.");
            }
            else if (Host == "host")
            {
                AddMessage("Cập nhật nhóm chức năng thành công.");
            }
            else
            {
                AddError("Cập nhật nhóm chức năng không thành công.");
            }
            return RedirectToRoute("u-role-index");
        }

        [Route("delete-role", Name = "u-role-delete")]
        public ActionResult DeleteRole(Guid id)
        {
            try
            {
                var response = _roleService.DeleteRoleById(id, GetCurrentUser().UserId);
                if (!response)
                {
                    AddError("Xóa không thành công.");
                }

                AddMessage("Xóa thành công.");
                return RedirectToRoute("u-role-index");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return RedirectToRoute("u-index");
            }
        }

        public RoleViewModel ToRoleModel(RoleRecord entity)
        {
            var model = new RoleViewModel
            {
                RoleId = entity.RoleId,
                Name = entity.Name,
                IsActive = entity.IsActive,
                Type = entity.Type
            };
            return model;
        }
        #endregion

        #region Change pass

        [Route("thay-doi-mat-khau", Name = "user-change-pass")]
        public ActionResult UserChangePass()
        {
            return View();
        }

        [Route("save-user-change-pass", Name = "save-user-change-pass")]
        [HttpPost]
        public ActionResult UserChangePass(UserModel model)
        {
            if (!ModelState.IsValid)
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            else
            {
                // check user
                var u = _userService.GetUser(model.UserName.Trim());
                if (u != null && u.UserId != Guid.Empty)
                {
                    var newPass = CommonHelper.GetMd5Hash(model.PasswordHash);
                    u.PasswordHash = newPass;
                    u.Email = model.Email;

                    if (_userService.UpdateUser(u))
                    {
                        AddMessage("Thay đổi mật khẩu thành công.");
                        return RedirectToRoute("u-index");
                    }
                }
                else
                {
                    AddError("Tài khoản không tồn tại.");
                }
                // send email

                return View();
            }
            return View();
        }
        #endregion
    }
}