﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BTS.eBook.Admin.Models;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Cover;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Contracts.Version;
using BTS.eBook.Core.Mvc;
using BTS.eBook.Services;
using MvcPaging;
using Newtonsoft.Json;

namespace BTS.eBook.Admin.Controllers
{
    [RoutePrefix("covers")]
    public class CoverController : BaseController
    {
        private readonly ICoverService _coverService;
        private readonly IGroupService _groupService;
        private readonly ICoverGroupService _coverGroupService;
        private readonly ICoverVersionGroupService _coverVersionGroupService;

        public CoverController(ICoverService coverService, IGroupService groupService, ICoverGroupService coverGroupService, ICoverVersionGroupService coverVersionGroupService)
        {
            _coverService = coverService;
            _groupService = groupService;
            _coverGroupService = coverGroupService;
            _coverVersionGroupService = coverVersionGroupService;
        }

        [Route("index", Name = "cv-index")]
        [SessionExpire]
        public ActionResult Index(string name, string code, int? version, int? page)
        {
            var currentPage = page ?? 1;
            var request = new SearchCoverRequest()
            {
                Header = new RequestHeader
                {
                    CallerName = "Search Category"
                },
                Name = name,
                Code = code,
                Version = version,
                Paging = new PagingRequest
                {
                    PageSize = Constant.DefaultPageSize,
                    PageIndex = currentPage
                }
            };

            var result = _coverService.SearchUser(request);
            if (result.Status == null) return View();

            var rs = result.Records.ToPagedList(1, Constant.DefaultPageSize, result.Paging.TotalRecords);

            ViewBag.Name = name;
            ViewBag.Code = code;
            ViewBag.Version = version;
            ViewBag.PageNumber = currentPage;

            return View(rs);
        }


        [Route("create-new", Name = "cv-create")]
        [SessionExpire]
        public ActionResult Create()
        {
            var model = new CoverModel()
            {
                IsActive = true
            };
            ToAllGroup(model);

            model.Version += 1;// default:v.1

            return View(model);
        }


        [Route("create-new", Name = "cv-add")]
        [HttpPost]
        public ActionResult Create(CoverModel model, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                var currentUser = GetCurrentUser().UserId;
                var request = new CoverRequest()
                {
                    Header = new RequestHeader
                    {
                        CallerName = "Add new",
                        Action = 1,
                        UserId = currentUser
                    },
                    Record = new CoverRecord()
                    {
                        Name = model.Name,
                        Code = model.Code,
                        IsActive = model.IsActive,
                        CoverVersion = JsonConvert.DeserializeObject<List<VersionCoverModel>>(model.CategoryVersion),
                        Groups = model.Groups,
                        Version = model.Version,
                        CreateDate = DateTime.Now,
                        CreateBy = currentUser
                    }
                };
                var isCreate = _coverService.CreateOrUpdate(request);
                if (isCreate)
                    AddMessage("Thêm tổng hợp thành công.");
                else
                    AddError("Thêm tổng hợp không thành công.");

                int ctn;
                int.TryParse(fc["hdIsSaveContinue"], out ctn);

                return RedirectToRoute(ctn == 0 ? "cv-index" : "cv-create");
            }
            else
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }

            model.IsActive = true;
            ToAllGroup(model);

            //model.Version = 1;

            return View(model);
        }


        [Route("edit", Name = "cv-edit")]
        [SessionExpire]
        public ActionResult Edit(Guid id, string name, string code, int? page)
        {
            var c = _coverService.GetRecordById(id);
            if (c == null) return RedirectToRoute("cv-index");

            var model = new CoverModel()
            {
                IsActive = true
            };
            ToAllGroup(model);

            // Max version
            var max = _coverService.GetCoverMaxVersion(c.ParentId.Value);
            max += 1;
            model.Version = max;
            model.OldVersion = c.Version;

            return View(model);
        }

        [Route("edit", Name = "cv-updated")]
        [HttpPost]
        public ActionResult Edit(CoverModel model, FormCollection fc)
        {
            if (!ModelState.IsValid)
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            else
            {
                int ctn;
                int.TryParse(fc["hdIsSaveContinue"], out ctn);

                var header = new RequestHeader
                {
                    CallerName = "Update",
                    UserId = GetCurrentUser().UserId
                };
                var record = new CoverRecord()
                {
                    CoverId = model.CoverId,
                    Name = model.Name,
                    Code = model.Code,
                    IsActive = model.IsActive,
                    CoverVersion = JsonConvert.DeserializeObject<List<VersionCoverModel>>(model.CategoryVersion),
                    Groups = model.Groups,
                    ParentId = model.ParentId
                };

                if (ctn > 0)
                {
                    header.Action = 3;// tao version moi
                    record.Version = model.Version;
                }
                else
                {
                    header.Action = 2;// edit
                    record.Version = model.OldVersion;
                }

                var request = new CoverRequest()
                {
                    Header = header,
                    Record = record
                };
                var response = _coverService.CreateOrUpdate(request);
                if (!response)
                {
                    AddError("Cập nhật không thành công.");
                }
                else
                {
                    AddMessage("Cập nhật thành công.");
                }
                return RedirectToRoute("cv-index");
            }

            // error
            var c = _coverService.GetRecordById(model.CoverId);
            if (c == null) return RedirectToRoute("cv-index");
            var md = ToModel(c);

            ToAllGroup(md);

            ToAllCoverVersion(md, model.CoverId);

            // versionGroup
            ToAllCoverGroup(md, model.CoverId);

            // Max version
            var max = _coverService.GetCoverMaxVersion(model.CoverId);
            max += 1;
            md.Version = max;
            md.OldVersion = c.Version;

            return View(md);
        }

        [Route("delete", Name = "cv-delete")]
        public ActionResult Delete(Guid id, string name, string code, int? version, int? page)
        {
            try
            {
                var response = _coverService.DeleteBy(id, GetCurrentUser().UserId);
                if (!response)
                {
                    AddError("Xóa không thành công.");
                    return RedirectToRoute("cv-index");
                }

                AddMessage("Xóa thành công.");
                return RedirectToRoute("cv-index", new { page = page ?? 1, name, code, version });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return RedirectToRoute("cv-index");
            }
        }

        [Route("on-off", Name = "cv-off")]
        public ActionResult OnAndOff(Guid id, string name, string code, int? version, int? page)
        {
            try
            {
                var response = _coverService.OnAndOff(id, GetCurrentUser().UserId);
                if (!response)
                {
                    AddError("Tắt-Bật không thành công.");
                    return RedirectToRoute("cv-index");
                }

                AddMessage("Tắt-Bật thành công.");
                return RedirectToRoute("cv-index", new { page = page ?? 1, name, code, version });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return RedirectToRoute("cv-index");
            }
        }

        private static CoverModel ToModel(CoverRecord item)
        {
            var model = new CoverModel()
            {
                CoverId = item.CoverId,
                Name = item.Name,
                Code = item.Code,
                IsActive = item.IsActive,
                Version = item.Version,
                ParentId = item.ParentId
            };
            return model;
        }

        private void ToAllGroup(CoverModel userModel)
        {
            var users = _groupService.GetAllGroupBy(new[] { 2, 3 }); //3: normal
            if (users != null && users.Any())
            {
                userModel.GroupModels = users.Select(x => new GroupModel()
                {
                    GroupId = x.GroupId,
                    Name = x.Name,
                    Code = x.Code
                }).ToList();
            }
        }

        private void ToAllCoverGroup(CoverModel model, Guid id)
        {
            var g = _coverGroupService.GetCoverGroupBy(id);
            if (g != null && g.Any())
            {
                model.Groups = g;
            }
        }

        private void ToAllCoverVersion(CoverModel model, Guid id)
        {
            var g = _coverVersionGroupService.GetCoverGroupBy(id);
            if (g != null && g.Any())
            {
                model.CoverVersion = g.ToList();
            }
        }

    }
}