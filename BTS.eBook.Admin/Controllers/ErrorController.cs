﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTS.eBook.Admin.Controllers
{
    public class ErrorController : Controller
    {

        public ActionResult General(string message)
        {
            ViewBag.Message = message;

            Response.StatusCode = 404;
            Response.TrySkipIisCustomErrors = true;

            return View();
        }
    }
}