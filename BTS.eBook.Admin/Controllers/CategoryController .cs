﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BTS.eBook.Admin.Models;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Category;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Helper;
using BTS.eBook.Core.Mvc;
using BTS.eBook.Services;
using MvcPaging;

namespace BTS.eBook.Admin.Controllers
{
    [RoutePrefix("categories")]
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;
        private readonly IRegionService _iregionService;
        public CategoryController(ICategoryService categoryService, IRegionService iregionService)
        {
            _categoryService = categoryService;
            _iregionService = iregionService;

        }


        [Route("index", Name = "c-index")]
        [SessionExpire]
        public ActionResult Index(string name, string code, int? page)
        {
            var currentPage = page ?? 1;
            var request = new SearchCategoryRequest()
            {
                Header = new RequestHeader
                {
                    CallerName = "Search Category"
                },
                Name = name,
                Code = code,
                Paging = new PagingRequest
                {
                    PageSize = Constant.DefaultPageSize,
                    PageIndex = currentPage
                }
            };

            var result = _categoryService.SearchUser(request);
            if (result.Status == null) return View();

            var users = result.Records.ToPagedList(1, Constant.DefaultPageSize, result.Paging.TotalRecords);

            ViewBag.Name = name;
            ViewBag.Code = code;
            ViewBag.PageNumber = currentPage;

            return View(users);
        }

        [Route("create-new", Name = "c-create")]
        [SessionExpire]
        public ActionResult Create()
        {
            var model = new CategoryModel()
            {
                IsActive = true
            };

            ToAllregions(model);

            return View(model);
        }

        [Route("create-new", Name = "c-add")]
        [HttpPost]
        public ActionResult Create(CategoryModel model, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                var uId = GetCurrentUser().UserId;
                var request = new CategoryRequest()
                {
                    Header = new RequestHeader
                    {
                        CallerName = "Add new",
                        Action = 1,
                        UserId = uId
                    },
                    Record = new CategoryRecord()
                    {
                        Name = model.Name,
                        Code = model.Code,
                        IsActive = model.IsActive,
                        Body = model.Body,
                        ImageUrl = model.ImageUrl,
                        Summary = model.Summary,
                        RegionId = model.RegionId,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        CreatedBy = uId,
                        ModifiedBy = uId
                    }
                };
                var isCreate = _categoryService.CreateOrUpdate(request);
                if (isCreate)
                    AddMessage("Thêm Sách thành công.");
                else
                    AddError("Thêm Sách không thành công.");

                int ctn;
                int.TryParse(fc["hdIsSaveContinue"], out ctn);

                return RedirectToRoute(ctn == 0 ? "c-index" : "c-create");
                //return new AjaxResult().Reload();
            }
            else
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }

            var md = new CategoryModel()
            {
                IsActive = true
            };

            ToAllregions(md);

            return View(md);
        }

        [Route("edit", Name = "c-edit")]
        [SessionExpire]
        public ActionResult Edit(Guid id, string name, string code, int? page)
        {
            var user = _categoryService.GetRecordById(id);
            if (user == null) return RedirectToRoute("c-index");
            var userModel = ToModel(user);
            ToAllregions(userModel);

            return View(userModel);
        }

        [Route("edit", Name = "c-updated")]
        [HttpPost]
        public ActionResult Edit(CategoryModel model)
        {
            if (!ModelState.IsValid)
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            else
            {
                var uId = GetCurrentUser().UserId;
                var request = new CategoryRequest()
                {
                    Header = new RequestHeader
                    {
                        Action = 2,
                        CallerName = "Update",
                        UserId = uId
                    },
                    Record = new CategoryRecord()
                    {
                        CategoryId = model.CategoryId,
                        Name = model.Name,
                        Code = model.Code,
                        IsActive = model.IsActive,
                        Body = model.Body,
                        ImageUrl = model.ImageUrl,
                        Summary = model.Summary,
                        RegionId = model.RegionId,
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = uId

                    }
                };
                var response = _categoryService.CreateOrUpdate(request);
                if (!response)
                {
                    AddError("Cập nhật không thành công.");
                }
                else
                {
                    AddMessage("Cập nhật thành công.");
                }
                return RedirectToRoute("c-index");
            }

            // error
            var user = _categoryService.GetRecordById(model.CategoryId);
            if (user == null) return RedirectToRoute("c-index");

            var userModel = ToModel(user);
            ToAllregions(userModel);

            return View(model);
        }

        [Route("delete", Name = "c-delete")]
        public ActionResult Delete(Guid id, string name, string code, int? page)
        {
            try
            {
                var response = _categoryService.DeleteBy(id, GetCurrentUser().UserId);
                if (!response)
                {
                    AddError("Xóa không thành công.");
                    return RedirectToRoute("c-index");
                }

                AddMessage("Xóa thành công.");
                return RedirectToRoute("c-index", new { page = page ?? 1, name, code });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return RedirectToRoute("c-index");
            }
        }

        [Route("upload", Name = "c-upload")]
        [HttpPost]
        [SessionExpire]
        public JsonResult Upload(HttpPostedFileBase file)
        {
            string value = string.Empty;
            string lblMessage = string.Empty;
            CommonHelper.Upload(file, ref value, ref lblMessage, "size", 1);

            return Json(string.Concat(value, ";#", lblMessage));
        }

        private static CategoryModel ToModel(CategoryRecord item)
        {
            var model = new CategoryModel()
            {
                CategoryId = item.CategoryId,
                Name = item.Name,
                Code = item.Code,
                IsActive = item.IsActive,
                Body = item.Body,
                ImageUrl = item.ImageUrl,
                Summary = item.Summary
            };
            return model;
        }

        private void ToAllregions(CategoryModel regionModel)
        {
            var items = _iregionService.GetAll();

            if (items != null && items.Any())
            {
                regionModel.RegionModels = items.Select(x => new RegionModel()
                {
                    RegionId = x.RegionId,
                    Name = x.Name,
                    Code = x.Code
                }).ToList();
            }
        }
    }
}