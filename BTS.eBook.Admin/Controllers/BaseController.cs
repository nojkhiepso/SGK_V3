﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BTS.eBook.Admin.Authentication;
using BTS.eBook.Core;
using log4net;

namespace BTS.eBook.Admin.Controllers
{
    public class BaseController : Controller
    {
        protected static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private List<string> _errors;
        private List<string> _messages;
        public static string Host
        {
            get
            {
                var host = ConfigurationManager.AppSettings["Host"];
                return string.IsNullOrEmpty(host) ? "host" : host;
            }
        }

        protected EbookMembershipUserCurrent GetCurrentUser()
        {
            return Session[Constant.SESSION_LOGIN_USER] as EbookMembershipUserCurrent;
        }

        public void AddError(string message)
        {
            TempData["ErrorMessage"] = null;

            if (_errors == null) _errors = new List<string>();
            _errors.Add(message);
            TempData["ErrorMessage"] = _errors;
        }

        public void AddErrorOnView(string message)
        {
            TempData["ErrorMessageOnView"] = null;

            if (_errors == null) _errors = new List<string>();
            _errors.Add(message);
            TempData["ErrorMessageOnView"] = _errors;
        }

        public void AddMessage(string message)
        {
            TempData["InfoMessage"] = null;

            if (_messages == null) _messages = new List<string>();
            _messages.Add(message);
            TempData["InfoMessage"] = _messages;
        }

    }
}