﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BTS.eBook.Admin.Models;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Group;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Mvc;
using BTS.eBook.Services;
using MvcPaging;

namespace BTS.eBook.Admin.Controllers
{
    [RoutePrefix("groups")]
    public class GroupController : BaseController
    {
        private readonly IGroupService _groupService;
        private readonly IUserService _userService;
        private readonly IGroupUserService _groupUserService;
        private readonly ICategoryService _categoryService;
        private readonly IVersionGroupService _versionGroupService;
        private readonly IVersionService _versionService;

        public GroupController(IGroupService groupService, IUserService userService, IGroupUserService groupUserService, ICategoryService categoryService, IVersionGroupService versionGroupService, IVersionService versionService)
        {
            _groupService = groupService;
            _userService = userService;
            _groupUserService = groupUserService;
            _categoryService = categoryService;
            _versionGroupService = versionGroupService;
            _versionService = versionService;
        }

        [Route("index", Name = "g-index")]
        [SessionExpire]
        public ActionResult Index(string name, string code, int? page)
        {
            var currentPage = page ?? 1;
            var request = new SearchGroupRequest()
            {
                Header = new RequestHeader
                {
                    CallerName = "Search Group"
                },
                Name = name,
                Code = code,
                Paging = new PagingRequest
                {
                    PageSize = Constant.DefaultPageSize,
                    PageIndex = currentPage
                }
            };

            var result = _groupService.SearchUser(request);
            if (result.Status == null) return View();

            var users = result.Records.ToPagedList(1, Constant.DefaultPageSize, result.Paging.TotalRecords);

            ViewBag.Name = name;
            ViewBag.Code = code;
            ViewBag.PageNumber = currentPage;

            return View(users);
        }

        [Route("create-new", Name = "g-create")]
        public ActionResult Create()
        {
            var model = new GroupModel()
            {
                IsActive = true
            };
            ToAllUser(model);
            //ToAllCategories(model);

            return View(model);
        }

        [Route("create-new", Name = "g-add")]
        [HttpPost]
        public ActionResult Create(GroupModel model, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                var uId = GetCurrentUser().UserId;
                var request = new GroupRequest()
                {
                    Header = new RequestHeader
                    {
                        CallerName = "Add new",
                        Action = 1,
                        UserId = uId
                    },
                    Record = new GroupRecord()
                    {
                        Name = model.Name,
                        Code = model.Code,
                        IsActive = model.IsActive,
                        Users = model.Users,
                        CategoryId = model.CategoryId,
                        Versions = model.Versions,
                        CreatedDate = DateTime.Now,
                        CreatedBy = uId,
                        ModifiedBy = uId,
                        ModifiedDate = DateTime.Now,
                        GroupType = model.GroupType
                    }
                };
                var isCreate = _groupService.CreateOrUpdate(request);
                if (isCreate)
                    AddMessage("Thêm Nhóm thành công.");
                else
                    AddError("Thêm Nhóm không thành công.");

                int ctn;
                int.TryParse(fc["hdIsSaveContinue"], out ctn);

                return RedirectToRoute(ctn == 0 ? "g-index" : "g-create");
            }
            else
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            ToAllUser(model);
            ToAllCategories(model);

            return View(model);
        }

        [Route("edit", Name = "g-edit")]
        [SessionExpire]
        public ActionResult Edit(Guid id, string name, string code, int? page)
        {
            var group = _groupService.GetRecordById(id);
            if (group == null) return RedirectToRoute("g-index");
            var model = ToModel(group);

            // userModel
            ToAllUser(model);
            // users
            ToAllGroupUser(model, id);
            // categories
            //ToAllCategories(model);
            //var vg = _versionGroupService.GetByGroupId(id);
            //if (vg != null)
            //{
            //    model.CategoryId = (Guid)vg.CategoryId;

            //    // get Versions
            //    ToAllVertionCategory(model, (Guid)vg.CategoryId);
            //}
            // versionGroup
            //ToAllVersionGroup(model, id);

            return View(model);
        }


        [Route("edit", Name = "g-updated")]
        [HttpPost]
        public ActionResult Edit(GroupModel model)
        {
            if (!ModelState.IsValid)
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            else
            {
                var uId = GetCurrentUser().UserId;
                var request = new GroupRequest()
                {
                    Header = new RequestHeader
                    {
                        Action = 2,
                        CallerName = "Update",
                        UserId = uId
                    },
                    Record = new GroupRecord()
                    {
                        GroupId = model.GroupId,
                        Name = model.Name,
                        Code = model.Code,
                        IsActive = model.IsActive,
                        Users = model.Users,
                        CategoryId = model.CategoryId,
                        Versions = model.Versions,
                        ModifiedBy = uId,
                        ModifiedDate = DateTime.Now,
                        GroupType = model.GroupType
                    }
                };
                var response = _groupService.CreateOrUpdate(request);
                if (!response)
                {
                    AddError("Cập nhật không thành công.");
                }
                else
                {
                    AddMessage("Cập nhật thành công.");
                }
                return RedirectToRoute("g-index");
            }

            // error
            var user = _groupService.GetRecordById(model.GroupId);
            var userModel = ToModel(user);

            // userModel
            ToAllUser(userModel);
            // users
            ToAllGroupUser(userModel, model.GroupId);
            // categories
            ToAllCategories(userModel);
            var vg = _versionGroupService.GetByGroupId(model.GroupId);

            if (vg != null)
            {
                userModel.CategoryId = (Guid)vg.CategoryId;

                // get Versions
                ToAllVertionCategory(userModel, (Guid)vg.CategoryId);
            }
            // versionGroup
            ToAllVersionGroup(userModel, model.GroupId);

            return View(userModel);
        }

        [Route("delete", Name = "g-delete")]
        public ActionResult Delete(Guid id, string name, string code, int? page)
        {
            try
            {
                var response = _groupService.DeleteBy(id, GetCurrentUser().UserId);
                if (!response)
                {
                    AddError("Xóa không thành công.");
                    return RedirectToRoute("g-index");
                }

                AddMessage("Xóa thành công.");
                return RedirectToRoute("g-index", new { page = page ?? 1, name, code });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return RedirectToRoute("g-index");
            }
        }

        private static GroupModel ToModel(GroupRecord item)
        {
            var model = new GroupModel()
            {
                GroupId = item.GroupId,
                Name = item.Name,
                Code = item.Code,
                IsActive = item.IsActive,
                GroupType = item.GroupType
            };
            return model;
        }

        private void ToAllUser(GroupModel userModel)
        {
            var users = _userService.GetUserExcludeByGroup();
            if (users != null && users.Any())
            {
                userModel.UserModels = users.Select(x => new UserModel()
                {
                    UserId = x.UserId,
                    UserName = x.UserName,
                    FullName = x.FullName
                }).ToList();
            }
        }

        private void ToAllCategories(GroupModel userModel)
        {
            var items = _categoryService.GetAllCategories();
            if (items != null && items.Any())
            {
                userModel.CategoryModels = items.Select(x => new CategoryModel()
                {
                    CategoryId = x.CategoryId,
                    Name = x.Name,
                    Code = x.Code
                }).ToList();
            }
        }

        private void ToAllVersionGroup(GroupModel model, Guid id)
        {
            var vg = _versionGroupService.GetVersionGroupBy(id);
            if (vg != null && vg.Any())
            {
                model.Versions = vg;
            }
        }

        private void ToAllVertionCategory(GroupModel model, Guid categoryId)
        {
            var vc = _versionService.GetVersions(categoryId);

            if (vc != null && vc.Any())
            {
                model.VersionModels = vc.ToList();
            }
        }

        private void ToAllGroupUser(GroupModel userModel, Guid id)
        {
            var users = _groupUserService.GetUserByGroupId(id);
            if (users != null && users.Any())
            {
                userModel.Users = users;
            }
        }
    }
}