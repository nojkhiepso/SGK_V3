﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using BTS.eBook.Core.Contracts.ContentFilePath;
using BTS.eBook.Core.Contracts.Cover;

namespace BTS.eBook.Admin.Controllers.Api.Model
{
    public class LoginRequest
    {
        [DisplayName("key")]
        public string Key { get; set; }

        [DisplayName("userName")]
        public string UserName { get; set; }

        [DisplayName("passWord")]
        public string PassWord { get; set; }

        [DisplayName("uniqueNumber")]
        public string UniqueNumber { get; set; }

        [DisplayName("version")]
        public string Version { get; set; }

        [DisplayName("order")]
        public string Order { get; set; }
    }

    public class Order
    {
        public Guid Id { get; set; }

        public string Version { get; set; }
    }

    public class LoginResponse
    {
        public string Key { get; set; }

        public int Status { get; set; }

        public string UserName { get; set; }

        public Guid UserId { get; set; }

        public string Msg { get; set; }

        public List<VersionList> Versions { get; set; }
    }

    public class VersionList
    {
        [DisplayName("url")]
        public string Url { get; set; }

        [DisplayName("version")]
        public int Version { get; set; }

        [DisplayName("id")]
        public Guid Id { get; set; }
    }

}