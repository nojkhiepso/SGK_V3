﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Http;

// ReSharper disable once CheckNamespace
namespace BTS.eBook.Admin.Controllers
{
    public class BaseApiController : ApiController
    {
        protected static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void ValidateRequest()
        {
            if (!Request.Headers.Contains("x-access-site"))
            {
                throw new ArgumentException("Missing x-access-site.");
            }

            if (!Request.Headers.Contains("x-access-token"))
            {
                throw new ArgumentException("Missing x-access-token.");
            }

            var siteRequest = Request.Headers.GetValues("x-access-site").First();
            var tokenRequest = Request.Headers.GetValues("x-access-token").First();

            var site = ConfigurationManager.AppSettings["AccessSite"];
            var token = ConfigurationManager.AppSettings["AccessToken"];

            if (site != siteRequest)
            {
                throw new ArgumentException("Site key invalid.");
            }

            if (token != tokenRequest)
            {
                throw new ArgumentException("Token key invalid.");
            }
        }
    }
}