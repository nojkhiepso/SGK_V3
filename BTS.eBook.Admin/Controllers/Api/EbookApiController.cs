﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using BTS.eBook.Admin.Hubs;
using Microsoft.AspNet.SignalR;
using BTS.eBook.Admin.Controllers.Api.Model;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Cover;
using BTS.eBook.Core.Helper;
using BTS.eBook.Core.Mvc;
using BTS.eBook.Services;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

// ReSharper disable once CheckNamespace
namespace BTS.eBook.Admin.Controllers
{
    public class EbookApiController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IAccessLogService _accessLogService;

        public EbookApiController(IUserService userService, IAccessLogService accessLogService)
        {
            _userService = userService;
            _accessLogService = accessLogService;
        }

        [HttpGet, Route("api/hello-word")]
        public IHttpActionResult HelloWord()
        {
            try
            {
                ValidateRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Json(new LoginResponse()
            {
                UserName = "dapdv"
            });
        }

        [HttpGet, Route("api/camel-case")]
        public HttpResponseMessage CamelCase()
        {
            try
            {
                var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, new LoginResponse(), JsonMediaTypeFormatter.DefaultMediaType);

                var objectContent = httpResponseMessage.Content as ObjectContent;

                if (objectContent != null)
                {
                    var jsonMediaTypeFormatter = new JsonMediaTypeFormatter
                    {
                        SerializerSettings =
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        }
                    };

                    httpResponseMessage.Content = new ObjectContent(objectContent.ObjectType, objectContent.Value, jsonMediaTypeFormatter);
                }

                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        #region Upload 
        [HttpPost, Route("api/upload-success/{uniqueCode}")]
        public IHttpActionResult UploadSuccess(string uniqueCode)
        {
            try
            {
                ValidateRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            Logger.Info("SignalR to client");

            var context = GlobalHost.ConnectionManager.GetHubContext<ActivityHub>();

            context.Clients.All.uploadSuccess(uniqueCode);

            Logger.Info("End signalR to client");

            return Ok("Upload success");
        }
        #endregion

        #region Client

        [Route("eBookServices/eBookServices.asmx/Login")]
        [Route("api/login")]
        [HttpPost]
        public HttpResponseMessage Login(LoginRequest request)
        {
            try
            {
                Logger.Info("Begin Login");

                string tokenKey = Guid.NewGuid().ToString();
                var session = CommonHelper.SetSession("TokenKey", tokenKey);
                if (!session) return Request.CreateResponse(HttpStatusCode.InternalServerError, "Lỗi đăng nhập...");

                var response = AccessLogin(request);

                #region Response
                if (response != null && response.Status == 4)
                {
                    Logger.Info("Token:" + tokenKey);

                    var v = new List<VersionList>();
                    if (response.Versions != null && response.Versions.Any())
                    {
                        v = response.Versions;
                    }

                    var objNew = new
                    {
                        uidHardware = "",
                        tokenKey,
                        versionList = v
                    };

                    _accessLogService.InsertAccessLog(Constant.Login, Constant.Login, objNew, response.UserId);

                    Logger.Info("End Login");

                    var httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK, objNew, JsonMediaTypeFormatter.DefaultMediaType);
                    var objectContent = httpResponseMessage.Content as ObjectContent;

                    if (objectContent != null)
                    {
                        var jsonMediaTypeFormatter = new JsonMediaTypeFormatter
                        {
                            SerializerSettings =
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        }
                        };
                        httpResponseMessage.Content = new ObjectContent(objectContent.ObjectType, objectContent.Value, jsonMediaTypeFormatter);
                    }
                    return httpResponseMessage;
                }
                #endregion

                var obj = new
                {
                    key = request.Key,
                    status = 3,
                    username = string.Empty,
                    msg = response?.Msg
                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Lỗi đăng nhập...", JsonMediaTypeFormatter.DefaultMediaType);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Lỗi đăng nhập...");
            }
        }

        [Route("api/download")]
        [Route("eBookServices/eBookServices.asmx/Download")]
        [HttpPost]
        public IHttpActionResult Donwload()
        {
            try
            {
                return Ok("Download");
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return BadRequest("Lỗi download");
            }
        }

        private LoginResponse AccessLogin(LoginRequest req)
        {
            Logger.Info("Req: " + req.JsonSerialize());

            #region validate
            if (string.IsNullOrEmpty(req.PassWord))
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Vui lòng nhập Mật khẩu!",
                    Status = 3
                };
            }

            if (string.IsNullOrEmpty(req.Version))
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Vui lòng nhập Version: '0' cho phép download toàn bộ!",
                    Status = 3
                };
            }

            var versionNo = 0;
            int.TryParse(req.Version, out versionNo);
            if (versionNo < 0)
            {
                new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Version lớn hơn hoặc bằng '0'!",
                    Status = 3
                };
            }

            var user = _userService.GetUserByName(req.UserName);

            if (user == null)
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Tài khoản không tồn tại!",
                    Status = 3
                };
            }

            if (!CommonHelper.VerifyMd5Hash(req.PassWord))
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Mật khẩu không đúng!",
                    Status = 3
                };
            }

            if (string.IsNullOrEmpty(req.UniqueNumber) && !string.IsNullOrEmpty(user.DeviceCode))
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Mã định danh không hợp lệ!",
                    Status = 3
                };
            }
            else
            {
                _userService.UpdateUniqueNo(req.UniqueNumber, user.UserId);
            }

            #endregion

            var orderRecord = new List<OrderRecord>();

            if (!string.IsNullOrEmpty(req.Order))
            {
                try
                {
                    var decode = JsonConvert.DeserializeObject<List<Order>>(req.Order);
                    if (decode != null && decode.Any())
                    {
                        orderRecord = decode.Select(x => new OrderRecord()
                        {
                            Id = x.Id,
                            Version = x.Version
                        }).ToList();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                    orderRecord = null;
                }
            }
            // null v2
            // !null v2+ v3
            var versionGroup = _userService.GetVersionByUser(user.UserId, orderRecord, versionNo);

            var vs = new List<VersionList>();

            if (versionGroup != null && versionGroup.Any())
            {
                vs = versionGroup.Select(x => new VersionList()
                {
                    Id = x.ParentId,
                    Url = x.PathUrl,
                    Version = x.CoverVersion
                }).ToList();
            }

            return new LoginResponse()
            {
                Key = req.Key,
                Msg = "Đăng nhập thành công",
                Status = 4,
                UserName = user.UserName,
                Versions = vs,
                UserId = user.UserId
            };
        }
        #endregion

    }

}
