﻿using BTS.eBook.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BTS.eBook.Core;
using BTS.eBook.Core.CacheHelper;
using BTS.eBook.Core.Contracts.AccessLog;
using BTS.eBook.Core.Contracts.Category;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.Core.Mvc;

namespace BTS.eBook.Admin.Controllers
{
    [SessionExpire]
    public class HomeController : BaseController
    {
        private readonly IUserService _userService;
        private readonly ICategoryService _categoryService;
        private readonly IAccessLogService _accessLogService;
        private readonly IVersionService _versionService;
        private readonly IGroupService _groupService;
        private readonly IRoleService _roleService;
        private readonly ICoverService _coverService;


        public HomeController(IUserService userService, ICategoryService categoryService, IAccessLogService accessLogService, IVersionService versionService, IGroupService groupService, IRoleService roleService, ICoverService coverService)
        {
            _userService = userService;
            _categoryService = categoryService;
            _accessLogService = accessLogService;
            _versionService = versionService;
            _groupService = groupService;
            _roleService = roleService;
            _coverService = coverService;
        }

        [Route("~/", Name = "Index")]
        public ActionResult Index()
        {
            // user
            ViewBag.Users = _userService.GetAllUser();

            return View();
        }

        [HttpGet, Route("category-dashboard")]
        public ActionResult Category()
        {
            var categories = _categoryService.GetAllCategoryDashBoards(5);
            if (categories != null && categories.Any())
            {
                var cGroup = categories.GroupBy(x => new { x.CategoryId, x.Name, x.Code, x.ImageUrl }).Select(y => new
                {
                    y.Key.CategoryId,
                    y.Key.Name,
                    y.Key.Code,
                    y.Key.ImageUrl,
                    Versions = y.Select(x => new
                    {
                        x.VersionNo
                    }).ToArray()
                });

                return Json(cGroup, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, Route("user-dashboard")]
        public ActionResult Users(int page, int pageSize)
        {
            var request = new SearchUserRequest()
            {
                Header = new RequestHeader
                {
                    CallerName = "Search User"
                },
                Paging = new PagingRequest
                {
                    PageSize = pageSize,
                    PageIndex = page
                }
            };
            var users = _userService.SearchUser(request);
            if (users?.Records != null && users.Records.Any())
            {
                var u = users.Records.Select(x => new
                {
                    x.UserName,
                    x.STT,
                    x.Email,
                    x.FullName,
                    LastModifyTime = x.LastModifyTime?.ToString("dd/MM/yyyy HH:ss:mm tt"),
                    x.AvatarUrl
                }).ToList();

                return Json(new
                {
                    users = u,
                    totalRows = users.Paging.TotalRecords
                });
            }
            return Json(null);
        }


        [HttpPost, Route("access-log")]
        public ActionResult AccessLog(int page, int pageSize)
        {
            var request = new SearchAccessLogRequest()
            {
                Header = new RequestHeader
                {
                    CallerName = "Search Acclog"
                },
                Paging = new PagingRequest
                {
                    PageSize = pageSize,
                    PageIndex = page
                }
            };

            var accessLogs = _accessLogService.SearchPaging(request);
            if (accessLogs?.Records != null && accessLogs.Records.Any())
            {
                var data = accessLogs.Records.Select(x => new
                {
                    x.ActionName,
                    CreatedDate = x.CreatedDate.ToString("dd/MM/yyyy HH:ss:mm tt"),
                    x.FunctionName,
                    x.FullName,
                    x.STT
                }).ToList();

                return Json(new
                {
                    accessLog = data,
                    totalRows = accessLogs.Paging.TotalRecords
                });
            }
            return Json(null);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Route("clear-cache", Name = "h-clear-cache")]
        public ActionResult ClearCache()
        {
            CacheManager.Instance.RemoveAll();
            AddMessage("Xóa cache thành công!");

            return RedirectToRoute("Index");
        }

        [Route("turn-off")]
        [SessionExpire]
        public ActionResult TurnOff(Guid id, string func, string status)
        {
            if (id == Guid.Empty || string.IsNullOrEmpty(func))
                return Json(new { msg = -1 });
            var flag = false;

            var userId = GetCurrentUser().UserId;

            switch (func)
            {
                case Constant.ModuleFunction.Category:
                    flag = _categoryService.TurnOff(id, status, userId);
                    break;
                case Constant.ModuleFunction.Version:
                    flag = _versionService.TurnOff(id, status, userId);
                    break;
                case Constant.ModuleFunction.User:
                    flag = _userService.TurnOff(id, status, userId);
                    break;
                case Constant.ModuleFunction.Group:
                    flag = _groupService.TurnOff(id, status, userId);
                    break;
                case Constant.ModuleFunction.Role:
                    flag = _roleService.TurnOff(id, status, userId);
                    break;
                case Constant.ModuleFunction.Cover:
                    flag = _coverService.TurnOff(id, status, userId);
                    break;
            }
            if (flag)
            {
                return Json(new { msg = 1 });
            }
            return Json(new { msg = 0 });
        }


        [Route("delete-many")]
        [SessionExpire]
        public ActionResult DeleteMany(Guid[] id, string func)
        {
            try
            {
                var flag = false;
                var uId = GetCurrentUser().UserId;

                switch (func)
                {
                    case Constant.ModuleFunction.Category:
                        flag = _categoryService.DeleteBy(id, uId);
                        break;
                    case Constant.ModuleFunction.Version:
                        flag = _versionService.DeleteBy(id, uId);
                        break;
                    case Constant.ModuleFunction.User:
                        flag = _userService.DeleteBy(id, uId);
                        break;
                    case Constant.ModuleFunction.Group:
                        flag = _groupService.DeleteBy(id, uId);
                        break;
                    case Constant.ModuleFunction.Cover:
                        flag = _coverService.DeleteBy(id, uId);
                        break;
                }

                if (!flag)
                {
                    return Json(new
                    {
                        msg = "Xóa không thành công.",
                        status = 0
                    });
                }
                AddMessage("Xóa thành công");
                return Json(new
                {
                    msg = "Xóa thành công.",
                    status = 1
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return RedirectToRoute("u-index");
            }
        }

    }
}