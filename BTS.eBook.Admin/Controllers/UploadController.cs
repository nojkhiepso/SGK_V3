﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BTS.eBook.Core.Mvc;
using BTS.eBook.Services;
using Newtonsoft.Json;

namespace BTS.eBook.Admin.Controllers
{
    [RoutePrefix("uploads")]
    [SessionExpire]
    public class UploadController : BaseController
    {
        private readonly ICoreUploadService _coreUploadService;

        public UploadController(ICoreUploadService coreUploadService)
        {
            _coreUploadService = coreUploadService;
        }

        [Route("remove-file", Name = "up-delete-coreupload")]
        public ActionResult RemoveFileUpload(string uniqueUpload, Guid[] id)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(uniqueUpload))
                {
                    if (id!=null && id.Length > 0 && !string.IsNullOrEmpty(uniqueUpload))
                    {
                        var d = _coreUploadService.DeleteById(id, uniqueUpload);
                        return Json(new { d });
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            return Json(new { d = false });
        }

        [Route("reload-data", Name = "up-reloadData")]
        public ActionResult ReloadData(string uniqueUpload)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(uniqueUpload))
                {
                    var files = _coreUploadService.GetFileBy(uniqueUpload);
                    if (files != null && files.Any())
                    {
                        return Json(new { d = JsonConvert.SerializeObject(files) });
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            return Json(new { d = "[]" });
        }
    }
}