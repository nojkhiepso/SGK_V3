﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BTS.eBook.Admin.Models;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Group;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Contracts.Version;
using BTS.eBook.Core.Helper;
using BTS.eBook.Core.Mvc;
using BTS.eBook.CoreData;
using BTS.eBook.Services;
using MvcPaging;
using Newtonsoft.Json;

namespace BTS.eBook.Admin.Controllers
{
    [RoutePrefix("versions")]
    public class VersionController : BaseController
    {
        private readonly IVersionService _versionService;
        private readonly ICategoryService _categoryService;
        private readonly IContentFilePathService _contentFilePathService;
        private readonly IGroupService _groupService;
        private readonly IVersionGroupService _versionGroupService;

        public VersionController(IVersionService versionService, ICategoryService categoryService, IContentFilePathService contentFilePathService, IGroupService groupService, IVersionGroupService versionGroupService)
        {
            _versionService = versionService;
            _categoryService = categoryService;
            _contentFilePathService = contentFilePathService;
            _groupService = groupService;
            _versionGroupService = versionGroupService;
        }

        #region Core

        [Route("index", Name = "v-index")]
        [SessionExpire]
        public ActionResult Index(Guid? categoryId, int? page)
        {
            var currentPage = page ?? 1;

            Guid? currentU = null;
            var user = GetCurrentUser();
            if (user.UserType != 1)
            {
                currentU = user.UserId;
            }

            var request = new SearchVersionRequest()
            {
                Header = new RequestHeader
                {
                    CallerName = "Search Version"
                },
                CategoryId = categoryId,
                Paging = new PagingRequest
                {
                    PageSize = Constant.DefaultPageSize,
                    PageIndex = currentPage
                },
                CurrentUserId = currentU
            };

            var result = _versionService.SearchPagging(request);
            if (result.Status == null) return View();

            var users = result.Records.ToPagedList(1, Constant.DefaultPageSize, result.Paging.TotalRecords);

            ViewBag.CategoryId = categoryId;
            ViewBag.PageNumber = currentPage;

            // get categor
            ViewBag.Categories = _categoryService.GetAllCategories();

            return View(users);
        }

        [Route("create-new", Name = "v-create")]
        [SessionExpire]
        public ActionResult Create()
        {
            SetUploadKey();
            var u = GetCurrentUser();
            ViewBag.IsAdmin = u.UserType == 1;
            var model = new VersionModel()
            {
                IsActive = true,
                Categories = _categoryService.GetAllCategories(),
            };

            return View(model);
        }

        [Route("create-new", Name = "v-add")]
        [HttpPost]
        public ActionResult Create(VersionModel model, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                string uploadKey = string.Empty;
                if (Session["uploadKeyContentFile"] != null)
                {
                    uploadKey = Convert.ToString(Session["uploadKeyContentFile"]);
                }
                var uId = GetCurrentUser().UserId;

                var record = new VersionRecord()
                {
                    CategoryId = model.CategoryId,
                    IsActive = model.IsActive,
                    PathUrl = model.PathUrl,
                    VersionNo = model.VersionNo,
                    CreateDate = model.CreateDate,
                    Summary = model.Summary,
                    UploadKey = uploadKey,
                    CreatedBy = uId,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    ModifiedBy = uId
                };
                record.Status = model.IsPublish ? 1 : 3; // 1: Publish, 2: Approval, 3: Draft
                var request = new VersionRequest()
                {
                    Header = new RequestHeader
                    {
                        CallerName = "Add new",
                        Action = 1,
                        UserId = uId
                    },
                    Record = record
                };
                var isCreate = _versionService.CreateOrUpdate(request);

                if (isCreate)
                    AddMessage("Thêm nội dung sách thành công.");
                else
                    AddError("Thêm nội dung sách không thành công.");

                return RedirectToRoute("v-index");
            }
            else
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }

            SetUploadKey();

            return View(new VersionModel()
            {
                IsActive = true,
                Categories = _categoryService.GetAllCategories()
            });
        }

        [Route("edit", Name = "v-edit")]
        [SessionExpire]
        public ActionResult Edit(Guid id, Guid? categoryId, int? page)
        {
            var user = _versionService.GetRecordById(id);
            if (user == null) return RedirectToRoute("v-index");
            var userModel = ToModel(user);
            userModel.Categories = _categoryService.GetAllCategories();

            var u = GetCurrentUser();
            ViewBag.IsAdmin = u.UserType == 1;

            SetUploadKey();
            return View(userModel);
        }

        [Route("edit", Name = "v-updated")]
        [HttpPost]
        public ActionResult Edit(VersionModel model)
        {
            if (!ModelState.IsValid)
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            else
            {
                string uploadKey = string.Empty;
                if (Session["uploadKeyContentFile"] != null)
                {
                    uploadKey = Convert.ToString(Session["uploadKeyContentFile"]);
                }
                var uId = GetCurrentUser().UserId;

                var record = new VersionRecord()
                {
                    VersionId = model.VersionId,
                    CategoryId = model.CategoryId,
                    IsActive = model.IsActive,
                    PathUrl = model.PathUrl,
                    VersionNo = model.VersionNo,
                    CreateDate = model.CreateDate,
                    Summary = model.Summary,
                    UploadKey = uploadKey,
                    ModifiedDate = DateTime.Now,
                    ModifiedBy = uId
                };
                record.Status = model.IsPublish ? 1 : 3; // 1: Publish, 2: Approval, 3: Draft
                var request = new VersionRequest()
                {
                    Header = new RequestHeader
                    {
                        Action = 2,
                        CallerName = "Update",
                        UserId = uId
                    },
                    Record = record
                };
                var response = _versionService.CreateOrUpdate(request);
                if (!response)
                {
                    AddError("Cập nhật không thành công.");
                }
                else
                {
                    AddMessage("Cập nhật thành công.");
                }
                return RedirectToRoute("v-index");
            }

            // error
            return View(model);
        }

        [Route("delete", Name = "v-delete")]
        public ActionResult Delete(Guid id, string name, string code, int? page)
        {
            try
            {
                var response = _versionService.DeleteBy(id, GetCurrentUser().UserId);
                if (!response)
                {
                    AddError("Xóa không thành công.");
                    return RedirectToRoute("v-index");
                }

                AddMessage("Xóa thành công.");
                return RedirectToRoute("v-index", new { page = page ?? 1, name, code });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return RedirectToRoute("v-index");
            }
        }

        [Route("version-no", Name = "v-versionNo")]
        public ActionResult GetVersionNo(Guid catId)
        {
            var versionNo = _versionService.GetVersionNo(catId);
            versionNo += 1;

            return Json(new { versionNo });
        }

        [Route("version-all", Name = "v-versions")]
        public ActionResult GetVersions(Guid catId)
        {
            var versions = _versionService.GetVersions(catId);

            var data = versions.Select(x => new
            {
                id = x.VersionId,
                text = "v." + x.VersionNo
            }).ToList();
            return Json(data);
        }


        [Route("version-approval", Name = "v-approval")]
        public ActionResult Approval(Guid id)
        {
            ViewBag.VersionId = id;
            var groups = _groupService.GetAllGroupBy(new[] { 2 });

            return View(groups);
        }

        [Route("version-approval", Name = "v-save-approval")]
        [HttpPost]
        public ActionResult Approval(GroupRecord model)
        {
            var lstV = new List<VersionGroup>();

            if (model.Ids != null && model.Ids.Length > 0)
            {
                foreach (var id in model.Ids)
                {
                    lstV.Add(new VersionGroup()
                    {
                        VersionGroupId = Guid.NewGuid(),
                        VersionId = model.VersionId,
                        GroupId = id
                    });
                }
            }

            var vg = _versionGroupService.AddMany(lstV, GetCurrentUser().UserId);
            if (vg)
            {
                // update 
                var u = _versionService.UpdateBy(2, model.VersionId, GetCurrentUser().UserId);
                if (u)
                {
                    AddMessage("Phê duyệt nhóm thành Công");
                    return new AjaxResult().Reload(true);
                }
            }

            ViewBag.VersionId = model.VersionId;
            var groups = _groupService.GetAllGroupBy(new[] { 2 }); //2: Approval
            return View(groups);
        }

        [Route("version-publish", Name = "v-publish")]
        public ActionResult Publish(Guid id)
        {
            var model = new VersionPublishModel()
            {
                VersionId = id
            };
            return View(model);
        }

        [Route("version-publish", Name = "v-save-publish")]
        [HttpPost]
        public ActionResult Publish(VersionPublishModel model)
        {
            var u = _versionService.UpdateBy(model.Status, model.VersionId, GetCurrentUser().UserId);
            if (u)
            {
                if (model.Status == 1)
                {
                    AddMessage("Xuất bản nội dung thành công.");
                }
                else if (model.Status == 3)
                {
                    AddMessage("Hủy xuất bản.");
                }
                return new AjaxResult().Reload(true);
            }

            AddErrorOnView("Xuất bản nội dung không thành công.");
            var md = new VersionPublishModel()
            {
                VersionId = model.VersionId
            };
            return View(md);
        }

        private static VersionModel ToModel(VersionRecord item)
        {
            var model = new VersionModel()
            {
                VersionId = item.VersionId,
                CategoryId = item.CategoryId,
                IsActive = item.IsActive,
                PathUrl = item.PathUrl,
                VersionNo = item.VersionNo,
                CreateDate = item.CreateDate,
                Summary = item.Summary
            };
            return model;
        }

        private void SetUploadKey()
        {
            if (Session["uploadKeyContentFile"] != null)
            {
                ViewBag.UploadKey = Session["uploadKeyContentFile"];
            }
            else
            {
                string uploadKey = CommonHelper.GetMd5Hash(Guid.NewGuid().ToString());
                Session.Add("uploadKeyContentFile", uploadKey);
                ViewBag.UploadKey = uploadKey;
            }
        }
        #endregion

        #region Upload
        [Route("save-content", Name = "v-save-content")]
        public ActionResult SaveAndClose(Guid id, string uniqueUpload)
        {
            return Json(new { d = SaveContentFile(id, uniqueUpload) });
        }

        private bool SaveContentFile(Guid id, string uniqueUpload)
        {
            try
            {
                if (id != Guid.Empty && !string.IsNullOrEmpty(uniqueUpload))
                {
                    return _contentFilePathService.CreateOrUpdate(id, uniqueUpload, GetCurrentUser().UserId);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            return false;
        }

        [Route("delete-file", Name = "v-delete-file")]
        public ActionResult DeleteUploadResult(Guid id, Guid[] idList)
        {
            try
            {
                return Json(new { d = DeleteContentFile(id, idList) });
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            return Json(new { d = "false" });
        }

        private bool DeleteContentFile(Guid id, Guid[] idList)
        {
            try
            {
                if (id != Guid.Empty)
                {
                    return _contentFilePathService.DeleteBy(id, idList, GetCurrentUser().UserId);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            return false;
        }

        [Route("files", Name = "v-getfilepath")]
        public ActionResult GetContentFile(Guid id)
        {
            try
            {
                if (id != Guid.Empty)
                {
                    var files = _contentFilePathService.GetFilesById(id);
                    if (files != null && files.Any())
                    {
                        return Json(new { d = JsonConvert.SerializeObject(files) });
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            return Json(new { d = "[]" });
        }
        #endregion

        // Tong hop
        [HttpPost, Route("version-category")]
        public ActionResult VersionCategory(int page, int pageSize)
        {
            var request = new SearchVersionRequest()
            {
                Header = new RequestHeader
                {
                    CallerName = "Search Version cat"
                },
                Paging = new PagingRequest
                {
                    PageSize = pageSize,
                    PageIndex = page
                },
                IsActive = true,
                Status = 3 // 3: draft
            };

            var result = _versionService.SearchPagging(request);
            if (result?.Records != null && result.Records.Any())
            {
                var data = result.Records.Select(x => new
                {
                    x.CategoryId,
                    x.STT,
                    x.CategoryName,
                    x.VersionNo,
                    x.VersionId,
                    x.Status
                }).ToList();

                return Json(new
                {
                    data,
                    totalRows = result.Paging.TotalRecords
                });
            }
            return Json(null);
        }
    }
}