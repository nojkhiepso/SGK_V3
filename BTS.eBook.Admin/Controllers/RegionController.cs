﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BTS.eBook.Admin.Models;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Region;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Helper;
using BTS.eBook.Core.Mvc;
using BTS.eBook.Services;
using MvcPaging;

namespace BTS.eBook.Admin.Controllers
{
    [RoutePrefix("regions")]
    
    public class RegionController : BaseController
    {
        private readonly IRegionService _regionService;

        public RegionController(IRegionService regionService)
        {
            _regionService = regionService;
        }


        [Route("index", Name = "un-index")]
        [SessionExpire]
        public ActionResult Index(string name, string code, int? page)
        {
            var currentPage = page ?? 1;
            var request = new SearchRegionRequest()
            {
                Header = new RequestHeader
                {
                    CallerName = "Search Region"
                },
                Name = name,
                Code = code,
                Paging = new PagingRequest
                {
                    PageSize = Constant.DefaultPageSize,
                    PageIndex = currentPage
                }
            };

            var result = _regionService.SearchPaging(request);
            if (result.Status == null) return View();

            var users = result.Records.ToPagedList(1, Constant.DefaultPageSize, result.Paging.TotalRecords);

            ViewBag.Name = name;
            ViewBag.Code = code;
            ViewBag.PageNumber = currentPage;

            return View(users);
        }

        [Route("create-new", Name = "un-create")]
        [SessionExpire]
        public ActionResult Create()
        {
            return View(new RegionModel()
            {
               
            });
        }

        [Route("create-new", Name = "un-add")]
        [HttpPost]
        public ActionResult Create(RegionModel model, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                var uId = GetCurrentUser().UserId;

                var request = new RegionRequest()
                {
                    Header = new RequestHeader
                    {
                        CallerName = "Add new",
                        Action = 1,
                        UserId = uId
                    },
                    Record = new RegionRecord()
                    {
                        Name = model.Name,
                        Code = model.Code,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Body = model.Body,
                        CreatedBy = uId,
                        ModifiedBy = uId
                    }
                };
                var isCreate = _regionService.CreateOrUpdate(request);
                if (isCreate)
                    AddMessage("Thêm Lĩnh vực thành công.");
                else
                    AddError("Thêm Lĩnh vực không thành công.");

                int ctn;
                int.TryParse(fc["hdIsSaveContinue"], out ctn);

                return RedirectToRoute(ctn == 0 ? "un-index" : "un-create");
            }
            else
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }

            return View(model);
        }

        [Route("edit", Name = "un-edit")]
        [SessionExpire]
        public ActionResult Edit(Guid id, string name, string code, int? page)
        {
            var user = _regionService.GetRecordById(id);
            if (user == null) return RedirectToRoute("un-index");
            var userModel = ToModel(user);

            return View(userModel);
        }

        [Route("edit", Name = "un-updated")]
        [HttpPost]
        public ActionResult Edit(RegionModel model)
        {
            if (!ModelState.IsValid)
            {
                var modelStateErrors = ModelState.Values.SelectMany(m => m.Errors);
                foreach (var err in modelStateErrors)
                {
                    AddError(err.ErrorMessage);
                }
            }
            else
            {
                var uId = GetCurrentUser().UserId;
                var request = new RegionRequest()
                {
                    Header = new RequestHeader
                    {
                        Action = 2,
                        CallerName = "Update",
                        UserId = uId
                    },
                    Record = new RegionRecord()
                    {
                        RegionId = model.RegionId,
                        Name = model.Name,
                        Code = model.Code,
                        Body = model.Body,
                        ModifiedBy = uId,
                        ModifiedDate = DateTime.Now
                    }
                };
                var response = _regionService.CreateOrUpdate(request);
                if (!response)
                {
                    AddError("Cập nhật không thành công.");
                }
                else
                {
                    AddMessage("Cập nhật thành công.");
                }
                return RedirectToRoute("un-index");
            }

            // error
            return View(model);
        }

        [Route("delete", Name = "un-delete")]
        public ActionResult Delete(Guid id, string name, string code, int? page)
        {
            try
            {
                var response = _regionService.Delete(id, GetCurrentUser().UserId);
                if (!response)
                {
                    AddError("Xóa không thành công.");
                    return RedirectToRoute("un-index");
                }
                AddMessage("Xóa thành công.");
                return RedirectToRoute("un-index", new { page = page ?? 1, name, code });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return RedirectToRoute("un-index");
            }
        }

        private static RegionModel ToModel(RegionRecord item)
        {
            var model = new RegionModel()
            {
                RegionId = item.RegionId,
                Name = item.Name,
                Code = item.Code,
                Body = item.Body
            };
            return model;
        }
    }
}