﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BTS.eBook.Admin.Startup))]
namespace BTS.eBook.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            app.MapSignalR();
        }
    }
}
