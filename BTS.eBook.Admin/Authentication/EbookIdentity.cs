﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace BTS.eBook.Admin.Authentication
{
    public class EbookIdentity: IIdentity
    {
        #region Properties

        public IIdentity Identity { get; set; }

        public Guid Id { get; set; }

        public string Avatar { get; set; }

        public string FullName { get; set; }

        #endregion

        public string Name => Identity.Name;

        public string AuthenticationType => Identity.AuthenticationType;

        public bool IsAuthenticated => Identity.IsAuthenticated;

        #region Constructor

        public EbookIdentity(IIdentity identity)
        {
            Identity = identity;
            var customMembershipUser = (EbookMembershipUser)Membership.GetUser(identity.Name);
            if (customMembershipUser != null)
            {
                Id = customMembershipUser.Id;
                Avatar = customMembershipUser.Avatar;
                FullName = customMembershipUser.FullName;
            }
        } 
        #endregion
    }
}