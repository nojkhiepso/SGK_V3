﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using System.Web.Caching;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.AccessLog;
using BTS.eBook.Core.Contracts.User;
using BTS.eBook.Core.Helper;
using BTS.eBook.Services;
using Newtonsoft.Json;
using BTS.eBook.Core.CacheHelper;

namespace BTS.eBook.Admin.Authentication
{
    public class EbookMembershipProvider : MembershipProvider
    {
        private int _cacheTimeoutInMinutes = Constant.CacheTimeout;

        public override void Initialize(string name, NameValueCollection config)
        {
            // Set Properties
            int val;
            if (!string.IsNullOrEmpty(config["cacheTimeoutInMinutes"]) && Int32.TryParse(config["cacheTimeoutInMinutes"], out val))
                _cacheTimeoutInMinutes = val;

            // Call base method
            base.Initialize(name, config);
        }

        public override MembershipUser CreateUser(string userName, string password, string email, string passwordQuestion, string passwordAnswer,
            bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string userName, string password, string newPasswordQuestion,
            string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string userName, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string userName, string answer)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string userName, string password)
        {
            var userService = DependencyResolver.Current.GetService(typeof(IUserService)) as IUserService;
            var accessLog = DependencyResolver.Current.GetService(typeof(IAccessLogService)) as IAccessLogService;

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return false;

            //Get Data from Database
            var userResult = new UserRecord();

            var cacheItem = CacheManager.Instance.GetCache<UserRecord>("_Login_" + userName);
            if (cacheItem != null)
            {
                userResult = cacheItem;
            }
            else
            {
                var user = userService.GetUserByName(userName);
                if (user == null) return false;
                userResult = user;

                CacheManager.Instance.SetCache("_Login_" + userName, user);
            }

            if (userResult != null && !userResult.IsLocked)
            {
                if (CommonHelper.VerifyMd5Hash(password, userResult.PasswordHash))
                {
                    accessLog.InsertAccessLog(Constant.Login, Constant.Login, userResult, userResult.UserId);

                    return true;
                }
            }
            return false;
        }

        private static UserRequest UserRequest(string userName)
        {
            return new UserRequest()
            {
                Record = new UserRecord()
                {
                    UserName = userName
                }
            };
        }

        public override MembershipUser GetUser(string userName, bool userIsOnline)
        {
            var _userServices = DependencyResolver.Current.GetService(typeof(IUserService)) as IUserService;

            var cacheKey = string.Format("User_{0}", userName);
            if (HttpRuntime.Cache[cacheKey] != null)
                return (EbookMembershipUser)HttpRuntime.Cache[cacheKey];

            //Get Data from Database
            var userResult = _userServices.GetUserByName(userName);
            if (userResult == null)
            {
                return null;
            }
            var membershipUser = new EbookMembershipUser(userResult);

            //Store in cache,NoSlidingExpiration : timeout
            HttpRuntime.Cache.Insert(cacheKey, membershipUser, null, DateTime.UtcNow.AddMinutes(_cacheTimeoutInMinutes), Cache.NoSlidingExpiration);
            return membershipUser;

        }


        #region Override
        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string userName, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string userNameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordRetrieval { get; }
        public override bool EnablePasswordReset { get; }
        public override bool RequiresQuestionAndAnswer { get; }
        public override string ApplicationName { get; set; }
        public override int MaxInvalidPasswordAttempts { get; }
        public override int PasswordAttemptWindow { get; }
        public override bool RequiresUniqueEmail { get; }
        public override MembershipPasswordFormat PasswordFormat { get; }
        public override int MinRequiredPasswordLength { get; }
        public override int MinRequiredNonAlphanumericCharacters { get; }
        public override string PasswordStrengthRegularExpression { get; }
        #endregion
    }
}