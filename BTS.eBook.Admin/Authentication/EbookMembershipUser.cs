﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using BTS.eBook.Core.Contracts.User;

namespace BTS.eBook.Admin.Authentication
{
    public class EbookMembershipUser : MembershipUser
    {
        #region Properties
        public Guid Id { get; set; }

        public string Avatar { get; set; }

        public string FullName { get; set; }

        #endregion

        public EbookMembershipUser(UserRecord user)
            : base("EbookMembershipProvider", user.UserName, user.FullName, user.Email, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now)
        {
            Id = user.UserId;
            FullName = user.FullName;
            Avatar = user.AvatarUrl;

        }
    }
}