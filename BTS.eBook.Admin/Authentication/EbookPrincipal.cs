﻿using System;
using System.Security.Principal;

namespace BTS.eBook.Admin.Authentication
{
    public class EbookPrincipal : IPrincipal
    {
        public EbookPrincipal(EbookIdentity identity)
        {
            Identity = identity;
        }

        public EbookIdentity ApartmentIdentity
        {
            get { return (EbookIdentity) Identity; }
            set { Identity = value; }
        }

        public bool IsInRole(string role)
        {
            return true;
        }

        public IIdentity Identity { get; set; }
    }
}