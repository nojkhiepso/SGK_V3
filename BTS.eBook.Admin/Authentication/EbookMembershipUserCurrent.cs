﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BTS.eBook.Admin.Models;

namespace BTS.eBook.Admin.Authentication
{
    public class EbookMembershipUserCurrent
    {
        public Guid UserId { get; set; }

        public string Avatar { get; set; }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public bool IsAdmin { get; set; }

        public int UserType { get; set; }

        public Guid[] ListRoles { get; set; }

        public List<ModuleViewModel> ListModules { get; set; }

    }
}