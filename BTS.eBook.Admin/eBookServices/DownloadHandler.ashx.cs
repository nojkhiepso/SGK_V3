﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using BTS.eBook.Core.Helper;
using log4net;

namespace BTS.eBook.Admin.eBookServices
{
    public class DownloadHandler : HttpTaskAsyncHandler, IReadOnlySessionState
    {
        protected static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            try
            {
                Logger.Info("Check authentication");

                var time = 1;
                var nextTime = string.Empty;
                CommonHelper.GetSession("Time_", ref nextTime);

                var currentToken = string.Empty;
                CommonHelper.GetSession("TokenKey", ref currentToken);
                string tokenKeyReq = context.Request.Headers.Get("tokenKey");

                string pathUrl = context.Request.Headers.Get("url");
                string filePathServer = HttpContext.Current.Server.MapPath("~");
                long num = 0L;
                long.TryParse(context.Request.Headers.Get("pos"), out num);

                if (tokenKeyReq != currentToken)
                {
                    context.Response.Write("File not found.");
                }

                if (!string.IsNullOrEmpty(nextTime) && num > 0)
                {
                    time += Convert.ToInt32(nextTime);
                    CommonHelper.SetSession("Time_", time);
                }
                else
                {
                    CommonHelper.SetSession("Time_", time);
                }

                Logger.Info("Time:" + time);

                bool flag = !string.IsNullOrEmpty(pathUrl) && !string.IsNullOrEmpty(filePathServer) && num >= 0L;
                if (flag)
                {
                    Logger.Info("Begin download");

                    string filePath = filePathServer + pathUrl;
                    Logger.Info(filePath);
                    bool fileExist = File.Exists(filePath);
                    if (fileExist)
                    {
                        Logger.Info("File.Exists");
                        Logger.Info("Pos:" + num);
                        Stream stream = null;

                        //This controls how many bytes to read at a time and send to the client
                        // Buffer to read bytes in chunk size specified above
                        byte[] buffer = new byte[100000000];
                        try
                        {
                            string fileName = Path.GetFileName(filePath);
                            stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                            bool flag7 = num > 0L;
                            if (flag7)
                            {
                                stream.Seek(num, SeekOrigin.Begin);
                            }
                            FileInfo fileInfo = new FileInfo(filePath);
                            long num3 = stream.Length;

                            Logger.Info("X-File-Size: " + fileInfo.Length);

                            context.Response.ContentType = "application/octet-stream";
                            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                            context.Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                            context.Response.AddHeader("FileName", fileInfo.Name);
                            context.Response.AddHeader("X-File-Size", fileInfo.Length.ToString());

                            while (num3 > 0L)
                            {
                                // Verify that the client is connected.
                                bool isClientConnected = context.Response.IsClientConnected;
                                if (isClientConnected)
                                {
                                    // Read data into the buffer.
                                    int num4 = stream.Read(buffer, 0, 100000000);
                                    // and write it out to the response's output stream
                                    await context.Response.OutputStream.WriteAsync(buffer, 0, num4);
                                    // Flush the data
                                    context.Response.Flush();
                                    //Clear the buffer
                                    buffer = new byte[100000000];
                                    num3 -= (long)num4;
                                }
                                else
                                {
                                    // cancel the download if client has disconnected
                                    num3 = -1L;
                                }
                            }

                            Logger.Info("Response.Flush");
                        }
                        catch (Exception ex)
                        {
                            context.Response.Write("Error : " + ex.Message);
                        }
                        finally
                        {
                            bool flag8 = stream != null;
                            if (flag8)
                            {
                                //Close the input stream
                                Logger.Info("Stream.Close");
                                stream.Close();
                            }
                            Logger.Info("End download");
                            context.Response.Close();
                        }
                    }
                }
            }
            catch (Exception ex2)
            {
                Logger.Error(ex2.ToString());
            }
        }

        public bool IsReusable => false;
    }
}