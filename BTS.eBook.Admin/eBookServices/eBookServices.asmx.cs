﻿using System;
using BTS.eBook.Admin.Controllers.Api.Model;
using BTS.eBook.Core.Helper;
using BTS.eBook.Services;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Services;
using BTS.eBook.Core;
using BTS.eBook.Core.Contracts.Client;

namespace BTS.eBook.Admin.eBookServices
{
    [ToolboxItem(false), ScriptService, WebService(Namespace = "BTS-Services", Description = "<a href='#' target='_blank'>BTS-eBook</a>", Name = "BTS-eBook"), WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class EBookServices : System.Web.Services.WebService
    {
        protected static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUserService _userService;
        private readonly IAccessLogService _accessLogService;

        public EBookServices()
        {
            _userService = DependencyResolver.Current.GetService(typeof(IUserService)) as IUserService;
            _accessLogService = DependencyResolver.Current.GetService(typeof(IAccessLogService)) as IAccessLogService;
        }

        [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string TestService()
        {
            Logger.Info("Test Log");

            var obj = new
            {
                status = true,
                text = "Ok"
            };
            return obj.JsonSerialize();
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod(EnableSession = true)]
        public string Login(string key, string userName, string passWord, string uniqueNumber, string version)
        {
            var req = new LoginRequest()
            {
                UserName = userName,
                PassWord = passWord,
                Key = key,
                Version = version,
                UniqueNumber = uniqueNumber
            };

            Logger.Info("Begin Login");

            var response = AccessLogin(req);

            if (response != null && response.Status == 4)
            {
                string tokenKey = CommonHelper.GetTokenKey(7);
                CommonHelper.SetSession("TokenKey", tokenKey);

                Logger.Info("Tk:" + tokenKey);

                var v = new List<VersionList>();

                if (response.Versions != null && response.Versions.Any())
                {
                    v = response.Versions;
                }

                var objNew = new
                {
                    uidHardware = "",
                    tokenKey,
                    versionList = v
                };

                var objSer = JsonConvert.SerializeObject(objNew, new JsonSerializerSettings()
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });

                _accessLogService.InsertAccessLog(Constant.Login, Constant.Login, objNew, response.UserId);

                Logger.Info("End Login");

                return objSer;
            }

            var obj = new
            {
                key,
                status = 3,
                username = string.Empty,
                msg = response?.Msg
            };
            return obj.JsonSerialize();
        }

        [ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod(EnableSession = true, MessageName = "Download")]
        public string Download(string url, string pos, string tokenKey)
        {
            return url;
        }

        private LoginResponse AccessLogin(LoginRequest req)
        {
            Logger.Info("Req: " + req.JsonSerialize());
            #region validate
            if (string.IsNullOrEmpty(req.PassWord))
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Vui lòng nhập Mật khẩu!",
                    Status = 3
                };
            }

            if (string.IsNullOrEmpty(req.Version))
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Vui lòng nhập Version: '0' cho phép download toàn bộ!",
                    Status = 3
                };
            }

            var no = 0;
            int.TryParse(req.Version, out no);
            if (no < 0)
            {
                new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Version lớn hơn hoặc bằng '0'!",
                    Status = 3
                };
            }

            var user = _userService.GetUserByName(req.UserName);

            if (user == null)
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Tài khoản không tồn tại!",
                    Status = 3
                };
            }

            if (!CommonHelper.VerifyMd5Hash(req.PassWord))
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Mật khẩu không đúng!",
                    Status = 3
                };
            }

            if (string.IsNullOrEmpty(req.UniqueNumber) && !string.IsNullOrEmpty(user.DeviceCode))
            {
                return new LoginResponse()
                {
                    Key = req.Key,
                    Msg = "Mã định danh không hợp lệ!",
                    Status = 3
                };
            }
            else
            {
                _userService.UpdateUniqueNo(req.UniqueNumber, user.UserId);
            }

            #endregion

            int maxVersion;
            var versionGroup = _userService.GetVersionByUser(user.UserId, null, out maxVersion);
            var vs = new List<VersionList>();

            if (versionGroup != null && versionGroup.Any())
            {
                if (no > maxVersion)
                {
                    return new LoginResponse()
                    {
                        Key = req.Key,
                        Msg = "Version không hợp lệ!",
                        Status = 3
                    };
                }
                var condition = versionGroup.Where(x => x.CoverVersion > no);

                Logger.Info("Filter:" + condition.JsonSerialize());
                if (condition != null && condition.Any())
                {
                    vs = condition.Select(x => new VersionList()
                    {
                        Id = x.ParentId,
                        Url = x.PathUrl,
                        Version = x.CoverVersion
                    }).ToList();
                }
            }

            return new LoginResponse()
            {
                Key = req.Key,
                Msg = "Đăng nhập thành công",
                Status = 4,
                Username = user.UserName,
                Versions = vs,
                UserId = user.UserId
            };
        }
    }
}
