﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BTS.eBook.Core.Contracts.CoreUpload;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Helper;
using BTS.eBook.Services;
using log4net;

namespace BTS.eBook.Admin
{
    public class DocumentInfo
    {
        public string FileName { get; set; }

        public string FileType { get; set; }

        public string FileLength { get; set; }

        public string Path { get; set; }

        public string DeleteFolder { get; set; }

    }
    /// <summary>
    /// Summary description for UploadDocumentHandler
    /// </summary>
    public class UploadDocumentHandler : HttpTaskAsyncHandler
    {
        protected static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ICoreUploadService _coreUploadService;

        public UploadDocumentHandler()
        {
            _coreUploadService = DependencyResolver.Current.GetService(typeof(ICoreUploadService)) as ICoreUploadService;
        }

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            string deleteFolder = string.Empty;
            try
            {
                string tk = context.Request.Headers.Get("X-Tk");
                if (string.IsNullOrEmpty(tk))
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not Found.");

                    Logger.Error("--UploadDocumentHandler Not found--");
                }
                else
                {

                    string endOfFile = context.Request.Headers.Get("X-EOF");
                    if (endOfFile == null)
                    {
                        #region Save path file into folder

                        var iStream = context.Request.InputStream;
                        var bufferTemporary = new byte[iStream.Length];
                        iStream.Read(bufferTemporary, 0, Convert.ToInt32(iStream.Length));

                        //Folder name Uploads
                        string path = HttpContext.Current.Server.MapPath("~/CoreUploads");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        //Folder name X-Unique
                        string uniqueCode = context.Request.Headers.Get("X-Unique");

                        string pathFolderCommon = string.Empty;
                        if (!string.IsNullOrEmpty(uniqueCode))
                        {
                            path += "/" + uniqueCode;
                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }
                            pathFolderCommon = path + "/info.txt";
                        }

                        if (Directory.Exists(path))
                        {
                            //Folder name X-Unique
                            //string fileName = context.Request.Headers.Get("X-File-Name").Replace("%20", "_");
                            string fileName = HttpUtility.UrlDecode(context.Request.Headers.Get("X-File-Name"), Encoding.UTF8);

                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string pathFolder = string.Empty;
                                path += string.Concat("/", CommonHelper.NormalizeTitle(fileName));

                                //Folder name file-name
                                if (!Directory.Exists(path))
                                {
                                    Directory.CreateDirectory(path);
                                    pathFolder = path;
                                }
                                if (Directory.Exists(path))
                                {
                                    string indexOfFile = context.Request.Headers.Get("X-File-Index_Slice_Of_File");

                                    //File name is indexOfFile
                                    if (!string.IsNullOrEmpty(indexOfFile))
                                    {
                                        path += string.Concat("/", indexOfFile, ".txt");

                                        #region Create file Info.txt
                                        if (Convert.ToInt32(indexOfFile) == 1)
                                        {
                                            //string[] arrInfo = fileName.Split('.');
                                            var arrInfo = new string[2];
                                            int lastIndexOf = fileName.LastIndexOf('.');
                                            if (lastIndexOf > 0)
                                            {
                                                arrInfo[0] = fileName.Substring(0, lastIndexOf);
                                                arrInfo[1] = fileName.Substring(lastIndexOf + 1, fileName.Length - lastIndexOf - 1);
                                                //if (arrInfo.Length == 2)
                                                //{
                                                int lengthOfFile = Convert.ToInt32(context.Request.Headers.Get("X-File-Size"));
                                                //  if (lengthOfFile > 0)
                                                // {
                                                string json = ",{'fileName':'" + arrInfo[0] + "','fileType':'" + arrInfo[1] + "','fileLength':'" + lengthOfFile + "','path':'" + pathFolder.Replace(@"\", "/") + "','deleteFolder':'" + path.Replace(@"\", "/") + "'}";
                                                File.AppendAllLines(pathFolderCommon, new[] { json });
                                                //  }
                                                // }
                                            }

                                        }
                                        #endregion

                                        #region Create file contain byte array
                                        if (File.Exists(path))
                                        {
                                            File.Delete(path);
                                        }
                                        using (var file = File.Create(path))
                                        {
                                            file.Write(bufferTemporary, 0, bufferTemporary.Length);
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        string root = HttpContext.Current.Server.MapPath("~/");
                        if (string.IsNullOrWhiteSpace(root))
                        {
                            return;
                        }

                        #region Read all file in Folder
                        string uniqueCode = context.Request.Headers.Get("X-Unique");
                        string currentUser = context.Request.Headers.Get("X-CurrentUser");
                        if (uniqueCode == null || currentUser == null) return;

                        deleteFolder = context.Server.MapPath("~/CoreUploads" + "/" + uniqueCode);
                        string pathFolder = context.Server.MapPath("~/CoreUploads" + "/" + uniqueCode + "/info.txt");

                        if (File.Exists(pathFolder))
                        {
                            string arrFiles = File.ReadAllText(pathFolder);

                            if (!string.IsNullOrEmpty(arrFiles))
                            {
                                arrFiles = string.Concat("[", arrFiles.Trim(','), "]");
                                var serializer = new JavaScriptSerializer();
                                var lstFileInfo = serializer.Deserialize<List<DocumentInfo>>(arrFiles);
                                if (lstFileInfo != null && lstFileInfo.Count > 0)
                                {

                                    foreach (var filesInfo in lstFileInfo)
                                    {
                                        if (!string.IsNullOrEmpty(filesInfo.Path))
                                        {
                                            if (Directory.Exists(filesInfo.Path.Trim()))
                                            {
                                                string[] filesInFolder = Directory.GetFiles(filesInfo.Path.Trim(), "*.txt");
                                                if (filesInFolder.Length > 0)
                                                {
                                                    int index = 0;
                                                    string pathDownload = string.Empty;
                                                    //string dataTimeNow = DateTime.Now.ToBinary().ToString(CultureInfo.InvariantCulture);
                                                    DateTime dataTimeNow = DateTime.Now;
                                                    for (int i = 0; i < filesInFolder.Length; i++)
                                                    {
                                                        string filePath = string.Concat(filesInfo.Path.Trim(), "/", i + 1, ".txt");
                                                        if (File.Exists(filePath))
                                                        {
                                                            string fileName = string.Format("{0}_{1}", CommonHelper.NormalizeTitle(Path.GetFileNameWithoutExtension(filesInfo.FileName)), dataTimeNow.ToString("yyyyMMdd-hhmmss"));
                                                            string pathSavedb = string.Concat(ImageHelper.GetDirectory("/e_ContentFiles/", Convert.ToInt32(currentUser), dataTimeNow, true), fileName, string.Concat(".", filesInfo.FileType));
                                                            pathDownload = string.Concat(root, @"\", ImageHelper.GetDirectory(@"\e_ContentFiles\", Convert.ToInt32(currentUser), dataTimeNow, false));

                                                            if (!Directory.Exists(pathDownload))
                                                            {
                                                                Directory.CreateDirectory(pathDownload);
                                                            }
                                                            pathDownload += string.Concat(@"\", fileName, ".", filesInfo.FileType);

                                                            var arrByte = File.ReadAllBytes(filePath);

                                                            //Note: File auto resize after created
                                                            #region Create File
                                                            if (i == 0)
                                                            {
                                                                //Initialize data and create file on disk 
                                                                using (var file = File.Create(pathDownload))
                                                                {
                                                                    file.Write(arrByte, 0, arrByte.Length);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                using (var fs = new BinaryWriter(new FileStream(pathDownload, FileMode.Open, FileAccess.Write)))
                                                                {
                                                                    //Set possion file stream to write
                                                                    fs.BaseStream.Position = index;

                                                                    //Read byte array from possion 0 and full length data 
                                                                    fs.Write(arrByte, 0, arrByte.Length);

                                                                    //Flush byte array data in to file stream
                                                                    fs.Flush();

                                                                }
                                                            }
                                                            #endregion
                                                            index += arrByte.Length;
                                                            if (index == Convert.ToInt32(filesInfo.FileLength))
                                                            {
                                                                //Finish 
                                                                var request = new CoreUploadRequest()
                                                                {
                                                                    Header = new RequestHeader()
                                                                    {
                                                                        Action = 1,
                                                                        CallerName = "Upload"
                                                                    },
                                                                    Record = new CoreUploadRecord()
                                                                    {
                                                                        Name = filesInfo.FileName,
                                                                        Code = filesInfo.FileType,
                                                                        Path = pathSavedb,
                                                                        UploadKey = uniqueCode
                                                                    }
                                                                };
                                                                Logger.Info("Begin save data");
                                                                if (_coreUploadService.CreateUpload(request))
                                                                {
                                                                    await UploadSucess(uniqueCode);

                                                                }
                                                                Logger.Info("End save data");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        Logger.Error("Begin delete folder core");
                        if (!string.IsNullOrEmpty(deleteFolder))
                        {
                            Directory.Delete(deleteFolder, true);
                        }
                        Logger.Error("End delete folder core");
                        #endregion
                    }
                }

            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                if (!string.IsNullOrEmpty(deleteFolder))
                {
                    Directory.Delete(deleteFolder, true);
                }
            }
        }

        public bool IsReusable => false;

        public async Task UploadSucess(string uniqueCode)
        {
            var handler = new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };

            var httpClient = new HttpClient(handler);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("x-access-site", ConfigurationManager.AppSettings["AccessSite"]);
            httpClient.DefaultRequestHeaders.Add("x-access-token", ConfigurationManager.AppSettings["AccessToken"]);

            var url = ConfigurationManager.AppSettings["ApiUrl"];
            var response = await httpClient.PostAsync(url + "api/upload-success/" + uniqueCode, new StringContent(string.Empty, Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
        }
    }
}