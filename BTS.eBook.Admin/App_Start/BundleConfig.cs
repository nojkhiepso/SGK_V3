﻿using System.Web;
using System.Web.Optimization;

namespace BTS.eBook.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/angular.min.js",
                "~/Scripts/app/app.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/ui-bootstrap-tpls-0.12.0.js",
                "~/Scripts/bootstrap-dialog.min.js",
                "~/Scripts/jquery.fancybox.pack.js",
                "~/Scripts/respond.js"
                , "~/Scripts/select2.js"
                , "~/Scripts/BTS.common.js"
                , "~/Scripts/jquery.bootstrap.custome.js"
                , "~/Scripts/moment-with-locales.min.js"
                , "~/Scripts/daterangepicker.js"
            ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/font-awesome.css",
                "~/Content/select2.css",
                "~/Content/jquery.fancybox.css",
                "~/Content/Site.css",
                "~/Content/paging.css",
                "~/Content/custom.css",
                "~/Content/bootstrap-datetimepicker.css",
                "~/Content/daterangepicker.css",
                "~/Content/Datetime/css/bootstrap-datepicker3.css"
            ));
        }
    }
}
