﻿using System.Web;
using System.Web.Mvc;
using BTS.eBook.Core.Mvc;

namespace BTS.eBook.Admin
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new HandleAndLogErrorAttribute());
        }
    }
}
