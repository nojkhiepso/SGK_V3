﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTS.eBook.Admin.Models
{
    public class ModuleModel
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public bool IsActive { get; set; }

    }
}