﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BTS.eBook.Admin.Models
{
    public class RoleViewModel
    {
        public Guid RoleId { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public int? Type { get; set; }
    }
}