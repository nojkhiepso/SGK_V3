﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTS.eBook.Admin.Models
{
    public class RegionModel
    {
        public System.Guid RegionId { get; set; }

        [Required(ErrorMessage = "Nhập Tên đơn vị")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Nhập mã đơn vị")]
        public string Code { get; set; }
        [AllowHtml]
        public string Body { get; set; }
    }
}