﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BTS.eBook.Core.Contracts.Group;

namespace BTS.eBook.Admin.Models
{
    public class ImportModel
    {
        public List<GroupRecord> GroupRecord { get; set; }

        public Guid[] Groups { get; set; }

        public string Title { get; set; }

        public bool IsDefault { get; set; }
    }

    public class UserImport
    {
        public string STT { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Tel { get; set; }
    }

    public class User
    {
        public string FullName { get; set; }

        public string Email { get; set; }
    }
}