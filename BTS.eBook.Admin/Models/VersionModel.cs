﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BTS.eBook.Core.Contracts.Category;
using BTS.eBook.Core.Contracts.Group;

namespace BTS.eBook.Admin.Models
{
    public class VersionModel
    {
        public System.Guid VersionId { get; set; }

        [Required(ErrorMessage = "Chọn sách.")]
        public System.Guid CategoryId { get; set; }

        public string PathUrl { get; set; }

        [AllowHtml]
        public string Summary { get; set; }

        public int VersionNo { get; set; }

        public DateTime CreateDate { get; set; }

        public bool IsActive { get; set; }

        public bool IsPublish { get; set; }

        public IEnumerable<CategoryRecord> Categories { get; set; } 
    }

    public class VersionPublishModel
    {
        public Guid VersionId { get; set; }

        public int Status { get; set; }

        [AllowHtml]
        public string Body { get; set; }
    }
}