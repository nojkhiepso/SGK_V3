﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTS.eBook.Admin.Models
{
    public class CategoryModel
    {
        public System.Guid CategoryId { get; set; }

        [Required(ErrorMessage = "Nhập Tên sách")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Nhập Mã sách")]
        public string Code { get; set; }

        [AllowHtml]
        public string Summary { get; set; }

        [AllowHtml]
        public string Body { get; set; }

        public string ImageUrl { get; set; }

        public bool IsActive { get; set; }

        public Guid RegionId { get; set; }

        public List<RegionModel> RegionModels { get; set; }
    }
}