﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BTS.eBook.Core.Contracts.Shared;
using BTS.eBook.Core.Contracts.Shared.Records;
using BTS.eBook.Core.Contracts.User;

namespace BTS.eBook.Admin.Models
{
    public class RoleModuleRequest:ICreationRequest<RoleModuleRecord>
    {
        public RequestHeader Header { get; set; }
        public RoleModuleRecord Record { get; set; }
    }
}