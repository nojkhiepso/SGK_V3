﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BTS.eBook.Core.Contracts.Version;

namespace BTS.eBook.Admin.Models
{
    public class GroupModel
    {
        public System.Guid GroupId { get; set; }

        [Required(ErrorMessage = "Nhập Tên nhóm")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Nhập Mã nhóm")]
        public string Code { get; set; }

        public bool IsActive { get; set; }

        public int GroupType { get; set; }

        public List<UserModel> UserModels { get; set; }

        public List<CategoryModel> CategoryModels { get; set; }

        public List<VersionRecord> VersionModels { get; set; }

        public Guid[] Users { get; set; }

        public Guid CategoryId { get; set; }

        public Guid[] Versions { get; set; }
    }
}