﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BTS.eBook.Core.Contracts.Cover;
using BTS.eBook.Core.Contracts.Version;

namespace BTS.eBook.Admin.Models
{
    public class CoverModel
    {
        public Guid CoverId { get; set; }

        public Guid? ParentId { get; set; }

        [Required(ErrorMessage = "Nhập Tên tổng hợp phiên bản")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Nhập Mã tổng hợp phiên bản")]
        public string Code { get; set; }

        public int Version { get; set; }

        public int OldVersion { get; set; }

        public bool IsActive { get; set; }

        public List<GroupModel> GroupModels { get; set; }

        public string CategoryVersion { get; set; }

        [Required(ErrorMessage = "Chọn nhóm")]
        public Guid[] Groups { get; set; }

        public List<VersionCoverModel> CoverVersion { get; set; }
    }
}