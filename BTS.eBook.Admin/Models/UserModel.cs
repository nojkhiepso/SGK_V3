﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BTS.eBook.Core.Contracts.User;

namespace BTS.eBook.Admin.Models
{
    public class UserModel
    {
        public System.Guid UserId { get; set; }

        public int STT { get; set; }

        [Required(ErrorMessage = "Nhập Tên đăng nhập")]
        [RegularExpression(@"^[a-zA-Z0-9._]*$", ErrorMessage = "Tên đăng nhập không đúng định dạng. Ví dụ: Ten123456")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Nhập Mật khẩu")]
        [MinLength(6, ErrorMessage = "Mật khẩu phải lớn hơn 6 ký tự")]
        public string PasswordHash { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime LastModifyTime { get; set; }

        public bool IsLocked { get; set; }

        public string AvatarUrl { get; set; }

        public bool IsActive { get; set; }

        public int UserType { get; set; }

        public string DeviceCode { get; set; }

        public Guid[] Roles { get; set; }

        public List<RoleRecord> RoleModel { get; set; }

    }

    public class LoginModel
    {
        [Required(ErrorMessage = "Nhập Tên đăng nhập")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Nhập mật khẩu")]
        [DataType(DataType.Password)]
        [StringLength(100, MinimumLength = 6)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }

    public class UserFunction
    {
        public Guid UserId { get; set; }

        public string FullName { get; set; }

        public string[] UsersFunction { get; set; }

        public Dictionary<int, string> UserFunctionModel { get; set; }
    }
}