﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BTS.eBook.Core.Contracts.Module;

namespace BTS.eBook.Admin.Models
{
    public class RoleModuleModel
    {
        public Guid RoleModuleId { get; set; }

        public Guid RoleId { get; set; }

        public Guid ModuleId { get; set; }

        public bool Add { get; set; }

        public bool Edit { get; set; }

        public bool Delete { get; set; }

        public bool View { get; set; }

        public bool Publish { get; set; }
    }

    public class GroupViewModel
    {
        public RoleViewModel RoleViewModel { get; set; }

        public List<UserModel> ListAllUserRole { get; set; }

        public Guid[] SelectedAllUserRole { get; set; }

        public List<UserModel> ListUserRole { get; set; }

        public Guid[] SelectedUserRole { get; set; }

        public List<ModuleRecord> ListModule { get; set; }
    }
}