﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace BTS.eBook.Admin.Hubs
{
    public class ActivityHub : Microsoft.AspNet.SignalR.Hub
    {
        public void Send(string name, string message)
        {
            // Call the uploadSuccess method to update clients.
            Clients.All.uploadSuccess(message);
        }
    }
}