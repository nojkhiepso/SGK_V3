﻿var bts = {};
$(function () {
    bts.common.init();
});

bts.common = {
    init: function () {
        $(document).ready(function () {

            $(".select2").select2();

            $("#btnSaveContinue").click(function () {
                $("#hdIsSaveContinue").val("1");
                $("#btnSave").click();
            });

            $(".close").click(function () {
                $("#Message").fadeOut('hide');
            });

            setTimeout(function () {
                $("#Message").fadeOut('hide');
            }, 5000);


            $(".turn-off").click(function () {
                if (confirm('Bạn có muốn thay đổi trạng thái không?')) {

                    var id = $(this).attr('data-id');
                    var func = $(this).attr('data-func');
                    var active = $(this).attr('data-active');

                    $.ajax({
                        type: "POST",
                        data: {
                            id: id,
                            func: func,
                            status: active
                        },
                        url: '/turn-off',
                        dataType: "json",
                        success: function (rs) {
                            if (rs.msg == -1) {
                                alert('Bad request.');
                            } else if (rs.msg == 0) {
                                alert('Thay đổi trạng thái không thành công.');
                            } else if (rs.msg == 1) {
                                //alert('Thay đổi trạng thái thành công.');
                                if (active == "True") {
                                    $(".turnOff_" + id).removeAttr("title").attr('title', "Tắt");
                                    $(".turnOff_" + id).removeAttr("data-active").attr('data-active', "False");
                                    $(".turnOff_" + id).removeClass('btn-danger').addClass('btn-success');
                                } else {
                                    $(".turnOff_" + id).removeAttr("title").attr('title', "Bật");
                                    $(".turnOff_" + id).removeAttr("data-active").attr('data-active', "True");
                                    $(".turnOff_" + id).removeClass('btn-success').addClass('btn-danger');
                                }

                            }
                        }
                    });
                }
            });
            
            $(".v-publish").click(function () {
                if (confirm('Bạn có muốn Xuất bản nội dung sách này không?')) {

                    var id = $(this).attr('data-id');
                    $.ajax({
                        type: "POST",
                        data: {
                            id: id,
                            status: 1
                        },
                        url: '/versions/version-publish',
                        dataType: "json",
                        success: function (rs) {
                            window.location.reload();
                        }
                    });
                }
            });

            //alert(1);
            $(document).on('change', '#CheckAll', function () {
                var checkboxes = $("input[type=checkbox][name=CheckRecord]");
                if ($(this).is(':checked')) {
                    checkboxes.prop('checked', true);
                } else {
                    checkboxes.prop('checked', false);
                }
            });

            $("input[type=checkbox][name=CheckRecord]").change(function () {
                if ($(this).is(':checked')) {
                    //alert($(this).val());
                    var checkboxes = $("input[type=checkbox][name=CheckRecord]:not(:checked)");
                    if (checkboxes.length == 0) {
                        $('#CheckAll').prop('checked', true);
                    } else {
                        $('#CheckAll').prop('checked', false);
                    }
                } else {
                    $('#CheckAll').prop('checked', false);
                }
            });


            $('.tbl-Index tr').click(function (event) {
                if (event.target.type !== 'checkbox') {
                    $(':checkbox', this).trigger('click');
                    var checkAllStatus = true;
                    $('.tbl-Index  input[type=checkbox]').each(function () {
                        checkAllStatus = $(this).attr('checked');
                        if (!checkAllStatus) {
                            return false;
                        }
                    });
                    $('.tbl-Index  input[type=checkbox]').attr('checked', checkAllStatus);
                }
            });

            // fancyBox
            
            $('[data-toggle="fancybox"]').fancybox({
                autoSize: true,
                modal: true,
                scrolling: 'no',
                autoHeight: true,
                title:'',
                iframe: {
                    scrolling: 'auto',
                    preload: false
                },
                beforeLoad: function () {
                    var width = this.element.data('fancybox-width');
                    if (width) {
                        this.width = width;
                    }

                    var height = this.element.data('fancybox-height');
                    if (height) {
                        this.height = height;
                    }

                    if (this.type === "iframe") {
                        this.padding = 0;
                    }

                    var beforeLoad = this.element.data('fancybox-beforeload');
                    if (beforeLoad) {
                        return eval(beforeLoad + "(this.element, this)");
                    }
                    return true;
                },
                afterLoad: function () {
                    if (this.type === "iframe") {
                        this.inner.prepend('<div class="modal-header"><button type="button" class="close" onclick="parent.jQuery.fancybox.close();"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">&nbsp;</h4></div>');
                    }
                }
            });
        }); 
    },
    itemSelected: function () {
        var arrayOfId = [];
        $("input[type=checkbox][name=CheckRecord]").filter("input:checked").each(function () {
            arrayOfId.push($(this).val());
        });
        return arrayOfId;
    },
    confirmDeleteAll: function () {
        if (bts.common.checkSelectAtLeastOne()) {
            return confirm('Bạn có muốn xóa bản ghi này không?');
        } else {
            alert('Vui lòng chọn bản ghi!');
        }
        return false;
    },
    checkSelectAtLeastOne: function () {
        var checkboxes = $("input[type=checkbox][name=CheckRecord]").filter("input:checked");
        if (checkboxes.length > 0) {
            return true;
        } else {
            return false;
        }
    },
    onDelete: function (module) {
        if (bts.common.confirmDeleteAll()) {
            var ids = bts.common.itemSelected();
            console.log(ids);
            $.ajax({
                type: "POST",
                url: '/delete-many',
                dataType: "json",
                data: JSON.stringify({
                    id: ids,
                    func: module
                }),
                contentType: "application/json; charset=utf-8",
                success: function (rs) {
                    if (rs.status == 0) {
                        alert(rs.msg);
                    } else {
                        var checkboxes = $("input[type=checkbox][name=CheckRecord]");
                        checkboxes.prop('checked', false);
                        location.reload();
                    }
                },
                error: function () { console.log("Lỗi xóa"); }
            });
        } 
    }
};
 