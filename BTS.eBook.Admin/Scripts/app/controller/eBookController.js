﻿(function () {
    'use strict';
    angular
        .module('eBook')
        .controller('ebookController', ebookController);

    angular
        .module('eBook')
        .controller('coverController', coverController);

    //angular
    //    .module('eBook')
    //    .controller('ebookController2', ebookController1);

    ebookController.$inject = ['$scope', 'options'];
    coverController.$inject = ['$scope', 'options'];

    function ebookController($scope, options) {
        $scope.categories = [];
        $scope.users = [];
        $scope.accessLog = [];

        $scope.pageSize = 10;
        $scope.currentUserPage = 1;
        $scope.currentAccessPage = 1;

        $scope.getCategories = function () {
            $.ajax({
                type: "GET",
                url: '/category-dashboard',
                success: function (rs) {
                    //console.log(rs)

                    $scope.categories = rs;

                    if (!$scope.$$phase) $scope.$apply();
                }
            });
        };

        $scope.getUsers = function () {
            $.ajax({
                type: "POST",
                data: {
                    page: $scope.currentUserPage,
                    pageSize: 6
                },
                url: '/user-dashboard',
                success: function (rs) {
                    //console.log(rs)

                    $scope.users = rs.users;
                    $scope.totalUserItems = rs.totalRows;

                    if (!$scope.$$phase) $scope.$apply();
                }
            });
        };

        $scope.getAccessLog = function () {
            $.ajax({
                type: "POST",
                data: {
                    page: $scope.currentAccessPage,
                    pageSize: $scope.pageSize
                },
                url: '/access-log',
                dataType: "json",
                success: function (rs) {
                    //console.log(rs)

                    $scope.accessLog = rs.accessLog;
                    $scope.totalAccItems = rs.totalRows;

                    if (!$scope.$$phase) $scope.$apply();
                }
            });
        };

        $scope.pageAccessChanged = function () {
            $scope.getAccessLog();
        };

        $scope.pageUserChanged = function () {
            $scope.getUsers();
        };
        $scope.pageCateChanged = function () {
            $scope.getCategories();
        };

    }

    function coverController($scope, options) {

        $scope.vrc = [];
        $scope.vrcSelected = [];

        if (options.CoverVersion != null && options.CoverVersion.length > 0) { //a dd
            $scope.vrcSelected = options.CoverVersion[0];
        }

        $scope.pageSize = 10;
        $scope.currentPage = 1;
        $scope.isSelected = false;
        $scope.getVrC = function () {
            $.ajax({
                type: "POST",
                data: {
                    page: $scope.currentPage,
                    pageSize: $scope.pageSize
                },
                url: '/versions/version-category',
                dataType: "json",
                success: function (rs) {
                    //console.log(rs)

                    $scope.vrc = rs.data;
                    $scope.totalItems = rs.totalRows;

                    if (!$scope.$$phase) $scope.$apply();
                }
            });
        };

        $scope.pageChanged = function () {
            $scope.getVrC();
        };


        $scope.add = function (cId, vId) {
            //console.log(cId);
            //console.log(vId);

            for (var i = 0; i < $scope.vrc.length; i++) {
                var catId = $scope.vrc[i].CategoryId;
                var verId = $scope.vrc[i].VersionId;

                if (catId == cId && verId == vId) {
                    var flag = false;
                    if ($scope.vrcSelected && $scope.vrcSelected.length > 0) {
                        for (var j = 0; j < $scope.vrcSelected.length; j++) {
                            if ($scope.vrcSelected[j].VersionId == verId) {
                                flag = true;
                                return;
                            }
                        }
                    }
                    if (!flag) {
                        $scope.vrcSelected.push({
                            CategoryName: $scope.vrc[i].CategoryName,
                            VersionNo: $scope.vrc[i].VersionNo,
                            CategoryId: catId,
                            VersionId: verId,
                            Status: $scope.vrc[i].Status
                        });
                    }
                }
            }
        }

        $scope.removeVrc = function (vrc, item) {
            var index = vrc.indexOf(item);
            vrc.splice(index, 1);
        };


    }

})();