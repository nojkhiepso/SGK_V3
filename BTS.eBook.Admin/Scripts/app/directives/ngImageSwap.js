﻿(function () {
    'use strict';

    app.directive("ngImageSwap", [function () {
        return {
            restrict: "A",
            scope: {
                ngImageSwap: '@',
                src: "@",
                imageSwap: "@"
            },
            link: function (scope, element, attr) {
                var $this = $(element);
                var exist = function (url, callback) {
                    var img = new Image();
                    img.onload = function () { callback(true); };
                    img.onerror = function () { callback(false); };
                    img.src = url;
                }
                var url = scope.ngImageSwap;
                var imageSwap = $this.data("image-swap");
                var replaceToAttr = $this.data("replace-to-attr");

                exist(url, function (ex) {
                    if (!ex) {
                        if (replaceToAttr != undefined || replaceToAttr == '') {
                            if (imageSwap) {
                                $this.attr(replaceToAttr, imageSwap);
                            }
                        } else {
                            if (imageSwap) {
                                $this.attr("src", imageSwap);
                            }
                        }
                    } else {
                        $this.attr("src", url);
                        if (replaceToAttr != undefined || replaceToAttr == '') {
                            if (imageSwap) {
                                $this.attr(replaceToAttr, url);
                            }
                        }
                    }
                });
            }
        }
    }]);
})();